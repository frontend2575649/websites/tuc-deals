import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path:'auth', loadChildren: () => import('./auth/authNew/auth.module').then(m => m.AuthModule) },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '', loadChildren: () => import('./panels/access/access.module').then(m => m.TUCAccessModule) },
 
];


@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})

// RouterModule.forRoot(appRoutes, { scrollPositionRestoration: 'enabled' })


export class AppRoutingModule { }
