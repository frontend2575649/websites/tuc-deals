import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HelperService } from './service/helper.service';
import { HttpClientModule } from '@angular/common/http';
import { NgProgressModule } from 'ngx-progressbar';
import { NgProgressHttpModule } from 'ngx-progressbar/http';
import { DataService } from './service/data.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgOtpInputModule } from 'ng-otp-input';
import { Angular4PaystackModule } from 'angular4-paystack';
import {FlutterwaveModule} from 'flutterwave-angular-v3';
import { RatingModule } from 'ng-starrating';
import { SnowFlakeComponent } from './component/snow-flake/snow-flake.component';

@NgModule({
  declarations: [
    AppComponent,
    SnowFlakeComponent
  ],
  imports: [
    BrowserModule,RatingModule,

    AppRoutingModule,
    HttpClientModule,
    NgProgressModule,
    NgProgressHttpModule,
    BrowserAnimationsModule,
    NgOtpInputModule,
    FlutterwaveModule,
    Angular4PaystackModule.forRoot('pk_test_xxxxxxxxxxxxxxxxxxxxxxxx'),
    
  ],
  // RouterModule.forRoot(appRoutes, { scrollPositionRestoration: 'enabled' })
  providers: [
    HelperService,
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
