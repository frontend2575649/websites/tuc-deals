import { Component, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { HelperService } from './service/helper.service';
import { MixpanelService } from './service/mixpanel.service';
import { Title, Meta } from '@angular/platform-browser';
import { SnowFlakeConfig } from './service/interface.service';
import { DataService } from './service/data.service';
declare let document: any; 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  [x: string]: any;
  public snowFlakes: SnowFlakeConfig[];

  constructor(
    private _MixpanelService: MixpanelService, 
    private _HelperService: HelperService,
    private _DataService: DataService,
    private _Router: Router, 
    private titleService: Title,
    private meta: Meta) 
    {
      this._MixpanelService.init();
      let acc = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Acc);
      if(acc){
        this._MixpanelService.identify(acc.MobileNumber);
      }else{
        this._MixpanelService.reset();
      }
      this.meta.addTags([
        { name: 'keywords', content: 'lagos, hotels, lagos hotels, abuja hotels, hotel reservations, massage, spa, dental, dealdey, dealday, deals, discount reservation, best deals, Samsung, lg, travel, tours, trip, visit, accommodation, shopping, shopping, sights, theatre, food, sports, events, nightlife, Thing To Do, Spas, Dentals, Restaurants, Hotels, Tours, Attractions & Getaway clubs, broadway, shows, music, Teeth whitening' },
        { name: 'description', content: 'Dealday.africa offers deals up to 80% on Thing To Do: Spas, Dentals, Restaurants, Hotels, Tours, Attractions & Getaway and More. Sign up now for a new deal every day' },
        ]);

        this.snowFlakes = [];

        for ( var i = 1 ; i <= 150 ; i++ ) {
          this.snowFlakes.push({
            depth: this.randRange( 1, 5 ),
            left: this.randRange( 0, 100 ),
            speed: this.randRange( 1, 5 )
          });
        }
        this.titleService.setTitle("Dealday");
    }
    title = 'Dealday';

  ngOnInit() {
    this._Router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (event instanceof NavigationStart) {
          this._DataService.browserRefresh = !this._Router.navigated;
        }
        let pageUrl = event.url.split('/')[1];
        if(['home', 'category', 'deals', 'buydeal', 'profile', 'dealcode'].includes(pageUrl)){
          this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents[pageUrl],
            {
              page: pageUrl,
              path: `/${pageUrl}`,
              ...(event.url.split('/')[1] == 'category') && {category: event.url.split('/')[2], subcategory: event.url.split('/')[4]}
            })
        } else if (pageUrl == '') {
          this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.LandingPage,
            {
              page: pageUrl,
              path: `/${pageUrl}`
            })
        }
      }    
    });
  }
  overlay(){
    document.getElementById('cust_cat_overlay').style.display = 'none';
    document.getElementById('show-subcat').style.display = 'none';
    document.querySelector('.banner_category-list-sub').style.display = 'none';
    document.querySelector('.banner-block').style.zIndex = '100';
  }

  private randRange( min: number, max: number ) : number {
		var range = ( max - min );
		return( min + Math.round( Math.random() * range ) );
	}

}



