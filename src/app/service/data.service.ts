import { DatePipe } from '@angular/common';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { BehaviorSubject, Observable, throwError, Subscription } from "rxjs";
import { catchError, filter, map } from "rxjs/operators";
import { HelperService } from './helper.service';
import { IBalance, ICategory, ICity, IDeal, IDealCode, IDealDetails, IDealRequest, IMerchants, IProfile, OResponse } from './interface.service';
import "../../assets/js/helper.js";
declare var SystemHelper: any;
import * as moment from 'moment'
import ColorThief from 'colorthief'
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { purchasedDeal } from '../features/account/profile/mock';
import { MixpanelService } from './mixpanel.service';
import { PaginationService } from './pagination.service';
@Injectable()
export class DataService {

    public ActiveCity: string = "";
    public ActiveState: string = "";
    public DealKey: string = "";
    public DealSlug: string = "";
    public DealId: string = "";
    public DealCode: string = "";
    public _Balance: IBalance =
        {
            Balance: 0
        }
    public Profile: IProfile;
    public _OwlOptions: OwlOptions = {
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        dots: true,
        navSpeed: 700,
        navText: ['<i class="icofont-thin-left"></i>', '<i class="icofont-thin-right"></i>'],
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        },
        nav: false
    }
    public _OwlOptionsDeal: OwlOptions = {
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        autoHeight: false,
        dots: false,
        navSpeed: 700,
        autoWidth: false,
        margin: 8,
        navText: ['<i class="icofont-thin-left"></i>', '<i class="icofont-thin-right"></i>'],
        // responsive: {
        //     0: {
        //         items: 1
        //     },
        //     400: {
        //         items: 4
        //     },
        //     740: {
        //         items: 6
        //     },
        //     940: {
        //         items: 8
        //     }
        // },
        nav: false
    }
    public _Deal: IDealDetails = {
        IsBookmarked: false,
        IsDealpurchased: false,
        CountdownConfig:
        {
            leftTime: 0
        },
        EndDate: new Date(),
        Locations: [],
        Gallery: [],
        Quantity: 1,
        Fee: 0,
        AverageRatings: 0
    };

    public Countries: any = [
        {
            ReferenceId: 1,
            ReferenceKey: "nigeria",
            Name: "Nigeria",
            IconUrl: "assets/images/countries/nigeria_icon.png",
            Isd: "234",
            Iso: "ng",
            CurrencyName: "Naira",
            CurrencyNotation: "NGN",
            CurrencySymbol: "₦",
            CurrencyHex: "&#8358;",
            NumberLength: 10,
            SupportEmail: "hello@thankucash.com",
            ContactNumbers: ['+234 908 710 6997', '+234 813 317 8563', '01 888 8177'],
            AboutUsUrl: "https://thankucash.com/about-us.html",
            SellDealUrl: "https://merchant.thankucash.com/account/register",
            PayStackKey: {
                _Live: "pk_live_4acd36db0e852af843e16e83e59e7dc0f89efe12",
                _Test: "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6",
            },
            FlutterwaveKey: {
                _Live: "FLWPUBK-ee5bad9eb83038f3624d909dac59874b-X",
                _Test: "FLWPUBK_TEST-a6e984618172d190fd60d61b15673910-X",
            },
            BusinessUrl: "https://thankucash.com/business.html",
            CaseStudiesUrl : 'https://thankucash.com/casestudies.html',
            FAQUrl: '/faq.html',
            redirectToOtherCountry: "https://deals.thankucash.com/home"
            // redirectToOtherCountry: "http://localhost:4200/home"
        },
        {
            ReferenceId: 87,
            ReferenceKey: "ghana",
            Name: "Ghana",
            IconUrl: "assets/images/countries/ghana_icon.png",
            Isd: "233",
            Iso: "gh",
            CurrencyName: "Cedi",
            CurrencyNotation: "GHS",
            CurrencySymbol: "₵",
            CurrencyHex: "&#8373;",
            NumberLength: 9,
            SupportEmail: "gh_hello@thankucash.com",
            ContactNumbers: ['0502190054'],
            AboutUsUrl: "https://thankucash.com/gh/about-us.html",
            SellDealUrl: "https://merchant.thankucash.com/account/register?country=gh",
            PayStackKey:{
                _Live:"pk_live_e4d4c71f582f3bcbf1843ddce1574ca208792c3a",
                _Test:"pk_test_db0fe4ae38aea0e08b400500e38aaa22834140ac",
            },
            FlutterwaveKey: {
                _Live:"FLWPUBK-ee5bad9eb83038f3624d909dac59874b-X",
                _Test:"FLWPUBK_TEST-a6e984618172d190fd60d61b15673910-X",
            },
            BusinessUrl: "https://thankucash.com/gh/business.html",
            CaseStudiesUrl : 'https://thankucash.com/gh/casestudies.html',
            FAQUrl: 'https://thankucash.com/gh/faq.html',
            redirectToOtherCountry: "https://deals.thankucash.com/home?country=gh"
            // redirectToOtherCountry: "http://localhost:4201/home"
        },
        {
            ReferenceId: 118,
            ReferenceKey: "kenya",
            Name: "Kenya",
            IconUrl: "assets/images/countries/kenya_icon.png",
            Isd: "254",
            Iso: "key",
            CurrencyName: "Shilling",
            CurrencyNotation: "KHS",
            CurrencySymbol: "KSh",
            CurrencyHex: "KSh",
            NumberLength: 9,
            SupportEmail: "hello@thankucash.com",
            ContactNumbers: ['+234 908 710 6997', '+234 813 317 8563', '01 888 8177'],
            AboutUsUrl: "https://thankucash.com/about-us.html",
            SellDealUrl: "https://merchant.thankucash.com/account/register",
            PayStackKey:{
                _Live:"",
                _Test:"",
            },
            FlutterwaveKey: {
                _Live:"",
                _Test:"",
            },
            BusinessUrl: "https://thankucash.com/business.html",
            CaseStudiesUrl : 'https://thankucash.com/casestudies.html',
            FAQUrl: '/faq.html',
            redirectToOtherCountry: `/comingsoon/kenya`
        }
    ];


    public _DealCode: any = {

    };
    public _CategoriesSelected: ICategory[] = [];
    public _CategoriesSorted: ICategory[] = [];
    public _SubCategoriesSorted: ICategory[] = [];
    public _SubCategoriesSelected: ICategory[] = [];
    public _Categories: ICategory[] = [];
    public _MerchantSelected: IMerchants[] = [];
    public _MerchantsSorted: IMerchants[] = [];
    public _Merchants: IMerchants[] = [];
    public _OriginalMerchants: IMerchants[] = [];
    public _Deals: IDeal[] = [];
    public _DealsPromotionNew: IDeal[] = [];
    public _DealsPromotioDoD: IDeal[] = [];
    public _DealsPromotionTop: IDeal[] = [];
    public _DealsProducts: IDeal[] = [];
    public _DealsPromotionFeatured: IDeal[] = [];
    public _DealsPromotionDiscount: IDeal[] = [];
    public _DealsPromotionEndingSoon: IDeal[] = [];
    public _DealsCategeory: IDeal[] = [];
    public _DealsMerchant: IDeal[] = [];
    public _Cities: ICity[] = [];
    public _States: ICity[] = [];
    public _AllStates: ICity[] = [];
    public DealsSortOrder = "Newest";
    public DealsSearchCondition = "";
    public TotalRecords = 0;
    public Offset = 0;
    public activePage = 1;
    public pager: any = {};
    public Limit = 18;
    public MinPrice = 0;
    public MaxPrice = 0;
    public _CountrySelcted: any = null;
    public _SearchedCities: ICity[] = [];
    public CustomerDealHistory: IDeal[] = [];
    public CustomerCart: IDealDetails[] = [];
    public _DealOrderHistory: any[] = [];
    public _DealsPurchasePoint: any[] = [];
    public _PuchasedDeals: any[] = [];
    public _DealCodeLoading: boolean = false;
    public shareReferralUrl:string = '';
    public shareReferralCode:string = '';
    public browserRefresh: boolean = false;
    public TotalBookmarkCount: number = 0;
    public enableCatDropdown: boolean = false;
    constructor(
        private _HelperService: HelperService,
        private router: Router,
        private activateRoute: ActivatedRoute,
        private _MixpanelService: MixpanelService,
        private paged: PaginationService
    ) {
        let country = this.activateRoute.snapshot.queryParams["country"];
        let CacheCountries = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.ActiveCountry);
        if(country && country == 'ken'){
            this.ChangeCountry(this.Countries[2]);
        }
        else if(country && country == 'gh'){
            this.ChangeCountry(this.Countries[1]);
        }
        else if (CacheCountries != null && CacheCountries != undefined) {
            this._CountrySelcted = CacheCountries;
        }
        else {
            this.ChangeCountry(this.Countries[0]);
        }
        var CacheCities = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Cities);
        if (CacheCities != null) {
            this._Cities = CacheCities;
            this._SearchedCities = CacheCities;
            var CityStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.ActiveCity);
            if (CityStorage != null) {
                this.ActiveCity = CityStorage.Name;
            }
        }
        this.RefreshData();
    }
    RefreshData(isCountryChanged = false) {
        this._Deals = [];
        this._GetBalannce();
        this._RefreshCategories();

        this._RefreshMerchants();
        this.GetCities(isCountryChanged);
        this.GetStates();
        this._GetBookmarks();
        this._HelperService.AppConfig.CurrencySymbol = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.ActiveCountry)?.CurrencyHex;
        this.router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if(event.url == '/' || event.url == '/home'){
                    this._GetDeals_Promo_New(isCountryChanged);
                    this._GetDeals_Promo_Discount(isCountryChanged);
                    this._GetDeals_Promo_Ending(isCountryChanged);
                    this._GetDeals_Promo_DealOfTheDay(isCountryChanged);
                    this._GetDeals_Promo_TopDeals(isCountryChanged);
                    this._GetDeals_ProductDeals(isCountryChanged);
                    this._GetDeals_Promo_FeaturedDeals(isCountryChanged);
                    this.enableCatDropdown = false;
                }else{
                    this.enableCatDropdown = true;
                }
            }
        })
        // this._GetDeals_Promo_Top();
    }
    ClearCity() {
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageItem.ActiveCity);
        this.ActiveCity = '';
        this.ActiveState = '';
        this.RefreshData();
        this._GetDealsReset();
        this._GetDeals();
    }
    CityChange(Item, isTrackEnable:boolean = false) {
        if(isTrackEnable){
            this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.LocationSearch, 
                {
                 searchCity: Item.Name
                })
        }
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.ActiveCity, Item);
        this.ActiveCity = Item.Name;
        this.RefreshData();
        this.isCityChangedScource.next(true);
        this._GetDealsReset();
        this._GetDeals();
    }
    ChangeCountry(Item, isReload = false) {
        // debugger;
        // console.log('CountryChange', Item);
            localStorage.clear();
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.ActiveCountry, Item, true);
            this._CountrySelcted = Item;
            this._HelperService.RefreshHelper();
            // if (Item.Name == 'Kenya') {
            //     // this.router.navigate(['comingsoon', Item.Name.toLowerCase()])
            //     window.location.href = Item.redirectToOtherCountry;
            // } else 
            if(isReload){
                this.RefreshData(true);
                // this.router.navigate(['home']);
                window.location.href = '/home';
            }
        // this.RefreshData(true);
        // this._GetDealsReset();
        // this._GetDeals();
    }
    StateChange(Item) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.ActiveState, Item);
        this.ActiveState = Item.Name;
        this._LocationConfig.ShowState = false;
        this._LocationConfig.ShowCity = true;
        this.GetSearchCities(Item);
    }
    GetSearchStates() {
        var Cache = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.SearchStates);
        if (Cache != null && Cache.length > 0) {
            this._States = Cache;
        }
        else {
            var pData = {
                Task: 'getsearchstates',
                Offset: this.Offset,
                Limit: this.Limit,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        if (this._States.length == 0) {
                            this._States = _Response.Result.Data;
                        }
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.SearchStates, _Response.Result.Data);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }

    }
    _LocationConfig =
        {
            ShowState: true,
            ShowCity: false,
        }
    GetSearchCities(Item) {
        var pData = {
            Task: 'getsearchcities',
            ReferenceId: Item.ReferenceId,
            Offset: this.Offset,
            Limit: this.Limit,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (this._Cities.length == 0) {
                        this._Cities = _Response.Result.Data;
                        this._SearchedCities = _Response.Result.Data;
                        this._LocationConfig.ShowCity = true;
                        this._LocationConfig.ShowState = false;
                    }
                    else {
                        this._LocationConfig.ShowCity = false;
                        this._LocationConfig.ShowState = true;
                    }
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Cities, _Response.Result.Data);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
        // var Cache = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Cities);
        // if (Cache != null && Cache.length > 0) {
        //     this._Cities = Cache;
        // }
        // else {

        // }
    }
    GetStates() {
        var CacheStates = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.AllStates);
        if (CacheStates != null && CacheStates.length > 0) {
            this._AllStates = CacheStates;
        }
        // else {
            var pData = {
                Task: 'getstates',
                Offset: 0,
                Limit: 1000,
                ReferenceId: this._CountrySelcted.ReferenceId,
                ReferenceKey: this._CountrySelcted.ReferenceKey,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.State, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        // console.log(_Response.Result)
                        if (this._AllStates.length == 0) {
                            this._AllStates = _Response.Result.Data;
                        }
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.AllStates, _Response.Result.Data);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                    // mock
                    this._AllStates = [
                        {
                            Name: 'Lagos',
                            ReferenceId: '1',
                            ReferenceKey: 'f9bbb5a7c58448f2a5d9c70f2313361e',
                            Deals: 0
                        },
                        {
                            Name: 'Oyo',
                            ReferenceId: '3',
                            ReferenceKey: 'f9bbb5a7c58448f2a5d9c70f2313361e',
                            Deals: 0
                        },
                        {
                            Name: 'River',
                            ReferenceId: '2',
                            ReferenceKey: 'f9bbb5a7c58448f2a5d9c70f2313361e',
                            Deals: 0
                        },
                    ]
                });
        // }
    }
    GetCities(isCountryChanged = false) {
        var CacheCities = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Cities);
        if (CacheCities != null && CacheCities.length > 0 && !isCountryChanged) {
        }
        else {
            var pData = {
                Task: 'getcities',
                Offset: this.Offset,
                Limit: this.Limit,
                CountryId: this._CountrySelcted.ReferenceId,
                CountryKey: this._CountrySelcted.ReferenceKey,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        if (this._Cities.length == 0) {
                            this._Cities = _Response.Result.Data;
                            this._SearchedCities = _Response.Result.Data;
                        }
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Cities, _Response.Result.Data);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }

    }
    _RefreshCategories() {
        var CacheCategories = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Categories);
        if (CacheCategories != null) {
            this._Categories = CacheCategories;
            this._CategoriesSorted = CacheCategories;
        }
        var pData = {
            Task: 'getcategories',
            Host: this._HelperService.AppConfig.HostType,
            Offset: 0,
            Limit: 24,
            Type: this.ActiveCity,
            CountryId: this._CountrySelcted.ReferenceId,
            CountryKey: this._CountrySelcted.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.AllCategories, _Response.Result.Data);
                    var Categories = _Response.Result.Data;
                    var RootCategories = Categories.filter(x => x.ParentCategoryId == undefined);
                    if (RootCategories.length > 0) {
                        RootCategories.forEach(RootCategory => {
                            var SubCategories = Categories.filter(x => x.ParentCategoryId == RootCategory.ReferenceId);
                            if (SubCategories.length > 0) {
                                RootCategory.SubCategories = SubCategories;
                            }
                        });
                    }
                    var tRootCategories: any = [];
                    RootCategories.forEach(element => {
                        var item = tRootCategories.findIndex(x => x.ReferenceId == element.ReferenceId);
                        if (item == -1) {
                            tRootCategories.push(element);
                        }
                    });
                    this._CategoriesSorted.forEach((ele: any, index) => {
                        if (ele.IsChecked) {
                            tRootCategories[index].IsChecked = true;
                        }
                    })
                    this._Categories = tRootCategories;
                    this._CategoriesSorted = tRootCategories;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Categories, tRootCategories);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    _RefreshMerchants() {
        var CacheMerchants = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Merchants);
        if (CacheMerchants != null) {
            this._Merchants = CacheMerchants;
            this._MerchantsSorted = CacheMerchants;
            this._OriginalMerchants = CacheMerchants;
        }
        var pData = {
            Task: 'getmerchants',
            Host: this._HelperService.AppConfig.HostType,
            Offset: 0,
            Limit: 1000,
            CityName: this.ActiveCity,
            Type: this.ActiveCity,
            CountryId: this._CountrySelcted.ReferenceId,
            CountryKey: this._CountrySelcted.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._Merchants = _Response.Result.Data;
                    // if(this._CategoriesSelected.length > 0)
                    // {
                    //     var tItems = [];
                    //     this._MerchantsSorted.forEach(element => {
                    //             var dealCount = 0 ; 
                    //             if(element.CategoryDistribution != undefined && element.CategoryDistribution != null && element.CategoryDistribution.length > 0)
                    //             {
                    //                 element.CategoryDistribution.forEach(element => {
                    //                 });
                    //             }
                    //     });
                    //     // this._MerchantsSorted = _Response.Result.Data;
                    // }
                    // else
                    // {
                    // }
                    this._Merchants.forEach(element => {
                        element.BackgroundColor = "#C0CCDA4D";
                        element.TotalDeals = 0;
                        const colorThief = new ColorThief();
                        const img = new Image();
                        img.addEventListener('load', function () {
                            var _color = colorThief.getColor(img);
                            if (_color != undefined) {
                                const rgbToHex = (r, g, b) => '#' + [r, g, b].map(x => {
                                    const hex = x.toString(16)
                                    return hex.length === 1 ? '0' + hex : hex
                                }).join('');
                                var ColorT = 'rgb(' + _color[0] + ',' + _color[1] + ',' + _color[2] + ',0.3)';
                                element.BackgroundColor = ColorT;
                            }
                        });
                        let imageURL = element.IconUrl;
                        let googleProxyURL = 'https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&refresh=2592000&url=';
                        img.crossOrigin = 'Anonymous';
                        img.src = googleProxyURL + encodeURIComponent(imageURL);
                    });
                    this._MerchantsSorted = this._Merchants;
                    this._OriginalMerchants = this._Merchants;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Merchants, this._Merchants);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    _GetDealsReset() {
        this._Deals = [];
        this._DealsCategeory = [];
        this._DealsPromotionNew = [];
        this._DealsPromotionFeatured = [];
        this._DealsPromotionDiscount = [];
        this._DealsPromotionEndingSoon = [];
        this.Offset = 0;
        this.TotalRecords = 0;
        this.activePage = 1;
        this.pager = {};
        this.Limit = 18;
    }
    _GetDeals() {
        this._HelperService.IsProcessing = true;
        var pData: IDealRequest = {
            Task: 'getdeals',
            Offset: (this.activePage - 1) * this.Limit,
            Limit: this.Limit,
            SearchCondition: this.DealsSearchCondition,
            SortExpression: this.DealsSortOrder,
            Categories: [],
            SubCategories: [],
            Merchants: [],
            City: this.ActiveCity,
            MinPrice: this.MinPrice,
            MaxPrice: this.MaxPrice,
            CountryId: this._CountrySelcted.ReferenceId,
            CountryKey: this._CountrySelcted.ReferenceKey,
        };
        // console.log(pData);
        if (this._CategoriesSelected.length > 0) {
            this._CategoriesSelected.forEach(element => {
                pData.Categories.push(element.ReferenceKey)
            });
        }
        if (this._SubCategoriesSelected.length > 0) {
            this._SubCategoriesSelected.forEach(element => {
                pData.SubCategories?.push(element.ReferenceKey)
            });
        }
        if (this._MerchantSelected.length > 0) {
            this._MerchantSelected.forEach(element => {
                pData.Merchants.push(element.ReferenceKey)
            });
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
            this._HelperService.IsProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    // this.Offset = this.Offset + this.Limit;
                    this.TotalRecords = _Response.Result.TotalRecords;
                    this._Deals = [];
                    this.Offset = this.Offset - 1;
                    this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.Limit);
                    _Response.Result.Data.forEach(element => {
                        element.CountdownConfig = this.TimerCode(element.EndDate)
                        // {
                        //     leftTime: moment(element.EndDate).diff(moment(), 'seconds'),

                        //     // leftTime: Math.round((new Date(element.EndDate).getTime() - new Date().getTime()) / 1000)
                        // }

                        this._Deals.push(element);
                    });
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Deals, this._Deals);
                }
            },
            _Error => {
                this._HelperService.IsProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }
    _GetDealsByCategeory(CKey) {
        var pData: IDealRequest = {
            Task: 'getdeals',
            Offset: this.Offset,
            Limit: this.Limit,
            SearchCondition: this.DealsSearchCondition,
            SortExpression: this.DealsSortOrder,
            Categories: [CKey],
            Merchants: [],
            City: this.ActiveCity,
            CountryId: this._CountrySelcted.ReferenceId,
            CountryKey: this._CountrySelcted.ReferenceKey,
        };
        if (this._CategoriesSelected.length > 0) {
            this._CategoriesSelected.forEach(element => {
                pData.Categories.push(element.ReferenceKey)
            });
        }
        if (this._MerchantSelected.length > 0) {
            this._MerchantSelected.forEach(element => {
                pData.Merchants.push(element.ReferenceKey)
            });
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.Offset = this.Offset + this.Limit;
                    // this._Deals = _Response.Result.Data;
                    _Response.Result.Data.forEach(element => {
                        element.CountdownConfig = this.TimerCode(element.EndDate)
                        this._Deals.push(element);
                    });
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Deals, this._DealsCategeory);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    _GetDealsByMerchant(CKey) {
        // var CacheDeal = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Deals);
        // if (CacheDeal != null) {
        //     this._Deals = CacheDeal;
        // }
        var pData: IDealRequest = {
            Task: 'getdeals',
            Offset: this.Offset,
            Limit: this.Limit,
            SearchCondition: this.DealsSearchCondition,
            SortExpression: this.DealsSortOrder,
            Categories: [],
            Merchants: [CKey],
            City: this.ActiveCity,
            CountryId: this._CountrySelcted.ReferenceId,
            CountryKey: this._CountrySelcted.ReferenceKey,
        };
        if (this._CategoriesSelected.length > 0) {
            this._CategoriesSelected.forEach(element => {
                pData.Categories.push(element.ReferenceKey)
            });
        }
        if (this._MerchantSelected.length > 0) {
            this._MerchantSelected.forEach(element => {
                pData.Merchants.push(element.ReferenceKey)
            });
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.Offset = this.Offset + this.Limit;
                    // this._Deals = _Response.Result.Data;
                    _Response.Result.Data.forEach(element => {
                        element.CountdownConfig = this.TimerCode(element.EndDate)

                        this._DealsMerchant.push(element);
                    });
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Deals, this._DealsCategeory);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    LocationCenter: google.maps.LatLngLiteral = {
        lat: 6.5557907,
        lng: 3.3856198
    };
    DealEndDate: any;
    timeRemaining = "--";
    isTimeLessThanDay: boolean = false;
    _GetDeal(isTrackEnable?:boolean) {
        // var Cache = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Deal + "" + this.DealKey);
        // if (Cache != null) {
        //     this._Deal = Cache;
        // }
        // else {
        this._Deal = {
            IsBookmarked: false,
            IsDealpurchased: false,
            CountdownConfig:
            {
                leftTime: 0
            },
            EndDate: new Date(),
            Locations: [],
            Gallery: [],
            Quantity: 1,
            AverageRatings: 0
        };
        // }
        var pData = {
            Task: 'getdeal',
            Host: this._HelperService.AppConfig.HostType,
            ReferenceKey: this.DealKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._Deal = _Response.Result;
                    this._Deal.Quantity = 1;
                    this.DealEndDate = this._Deal.EndDate;
                    var difference = moment(this.DealEndDate).diff(moment(), 'hours');
                    if (difference < 0) {
                        this.timeRemaining = "Deal No Longer Available";
                        this.isTimeLessThanDay = false;
                    } else if (difference >= 0 && difference <= 24) {
                        this.isTimeLessThanDay = true;
                        this._Deal.CountdownConfig = { leftTime: moment(this.DealEndDate).diff(moment(), 'seconds') };
                    } else if (difference > 24 && difference >= 48) {
                        this.timeRemaining = Math.round(difference / 24) + " Day Remaining";
                        this.isTimeLessThanDay = false;
                    } else {
                        this.timeRemaining = Math.round(difference / 24) + " Days Remaining";
                        this.isTimeLessThanDay = false;
                    }
                    if (this._Deal.Locations != undefined && this._Deal.Locations != null) {
                        if (this._Deal.Locations.length > 0) {
                            this.LocationCenter = {
                                lat: this._Deal.Locations[0].Latitude,
                                lng: this._Deal.Locations[0].Longitude
                            };
                            this._Deal.Locations.forEach(element => {

                                element.Marker =
                                {
                                    position: {
                                        lat: element.Latitude,
                                        lng: element.Longitude,
                                    },
                                    label: {
                                        color: 'red',
                                        text: element.Name,
                                    },
                                    title: element.Name,
                                    options: { animation: google.maps.Animation.DROP },
                                }
                            });
                        }
                    }

                    if (this._Deal.Deals != undefined && this._Deal.Deals != null && this._Deal.Deals.length > 0) {
                        this._Deal.Deals.forEach(element => {
                            element.CountdownConfig = this.TimerCode(element.EndDate)
                        });
                    }
                    this._SetCustomerDealHistory(this._Deal);
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Deal + "" + this.DealKey, this._Deal);
                    // this._Categories = _Response.Result.Data;
                    // this._CategoriesSorted = _Response.Result.Data;
                    // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Categories, this._Categories);
                    if(isTrackEnable){
                        this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.DealViewed, 
                            {
                              DealKey: this.DealKey,
                              DealName: this._Deal.Title,
                              page: 'deal',
                              path: '/deal'
                            })
                    }
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    _SetCustomerDealHistory(Deal) {
        this.CustomerDealHistory = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.CustomerDealHistory);
        if (!this.CustomerDealHistory) {
            this.CustomerDealHistory = [];
        }
        let matchDeal = -1;
        matchDeal = this.CustomerDealHistory?.findIndex(ele => ele.ReferenceKey == Deal.ReferenceKey);
        if (matchDeal == -1) {
            Deal.CountdownConfig = this.TimerCode(Deal.EndDate);
            this.CustomerDealHistory.push(Deal);
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.CustomerDealHistory, this.CustomerDealHistory)
        }
    }

    _GetProductPurchasedHistory() {
        this._HelperService.IsProcessing = true;
        var pData: IDealRequest = {
            Task: 'getproductpurchasehistory',
            Offset: (this.activePage - 1) * this.Limit,
            Limit: this.Limit,
            SearchCondition: "",
            SortExpression: "",
            Categories: [],
            Merchants: [],
            City: this.ActiveCity,
            CountryId: this._CountrySelcted.ReferenceId,
            CountryKey: this._CountrySelcted.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.Offset = this.Offset - 1; //this.Offset + this.Limit;
                    this.TotalRecords = _Response.Result.TotalRecords;
                    this.pager = this.paged.getPager(this.TotalRecords, this.activePage);
                   this._DealOrderHistory = _Response.Result.Data;
                    // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Deals, this._Deals);
                }
            },
            _Error => {
                this._HelperService.IsProcessing = false;
                this._HelperService.HandleException(_Error);
            });
            // this._DealOrderHistory = purchasedDeal;
    }
    _GetPuchasedDeals() {
        this._PuchasedDeals = [];
        var pData = {
            Task: 'getpurchasehistory',
            Offset: (this.activePage - 1) * this.Limit,
            Limit: this.Limit,
            SearchCondition: "",
            SortExpression: "ReferenceId desc",
        };
        this._HelperService.IsProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.Offset = this.Offset - 1; //this.Offset + this.Limit;
                    this.TotalRecords = _Response.Result.TotalRecords;
                    this.pager = this.paged.getPager(this.TotalRecords, this.activePage);
                    this._PuchasedDeals = _Response.Result.Data;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }, () => this._HelperService.IsProcessing = false);
    }

    GetPointPurchaseHistory() {
        var pData = {
          Task: 'getpointpurchasehistory',
          TotalRecords: 0,
          Offset: 0,
          Limit: 10,
          RefreshCount: true,
          SortExpression: 'TransactionDate desc',
          Type: 'success'
        };
        this._HelperService.IsProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._DealsPurchasePoint = _Response.Result.Data;
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.HandleException(_Error);
          });
      }

    setPage(page: number, fn: string) {
        this.activePage = page;
        if(page < 1 || page > this.pager.totalPages){
            return
        }
        this.pager = this.paged.getPager(this.TotalRecords, page)
        this[fn]();
    }

    _AddToCart(Deal) {
        this.CustomerCart = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.CustomerCart);
        if (!this.CustomerCart) {
            this.CustomerCart = [];
        }
        let matchDeal = -1;
        matchDeal = this.CustomerCart?.findIndex(ele => ele.ReferenceKey == Deal.ReferenceKey);
        Deal.CountdownConfig = this.TimerCode(Deal.EndDate);
        if (matchDeal == -1) {
            this.CustomerCart.push(Deal);
        } else {
            this.CustomerCart[matchDeal] = Deal;
        }
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.CustomerCart, this.CustomerCart);
        // console.log(this.CustomerCart)
    }

    _GetPurchaseDeal() {

        // var CacheDeal = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Deals);
        // if (CacheDeal != null) {
        //     this._Deals = CacheDeal;
        // }
        var pData: IDealRequest = {
            Task: 'getdealpurchasehistory',
            Offset: this.Offset,
            Limit: this.Limit,
            SearchCondition: this.DealsSearchCondition,
            SortExpression: this.DealsSortOrder,
            Categories: [],
            Merchants: [],
            City: this.ActiveCity,
            CountryId: this._CountrySelcted.ReferenceId,
            CountryKey: this._CountrySelcted.ReferenceKey,
        };
        if (this._CategoriesSelected.length > 0) {
            this._CategoriesSelected.forEach(element => {
                pData.Categories.push(element.ReferenceKey)
            });
        }
        if (this._MerchantSelected.length > 0) {
            this._MerchantSelected.forEach(element => {
                pData.Merchants.push(element.ReferenceKey)
            });
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.Offset = this.Offset + this.Limit;
                    // this._Deals = _Response.Result.Data;
                    _Response.Result.Data.forEach(element => {
                        element.CountdownConfig = this.TimerCode(element.EndDate)

                        this._Deals.push(element);
                    });
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Deals, this._Deals);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });

    }


    _GetDealCode() {
        this._DealCodeLoading = true;
        var pData = {
            Task: 'getdealcode',
            Host: this._HelperService.AppConfig.HostType,
            DealCodeKey: this.DealCode,
            ReferenceId: this.DealId,
            ReferenceKey: this.DealCode
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._DealCodeLoading = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._DealCode = _Response.Result;
                    // this._DataService._Deal = _Response.Result;
                    // this._GetBalance();
                    // this._DataService._Deal.CountdownConfig =
                    // {
                    //     leftTime: Math.round((new Date(this._DataService._Deal.EndDate).getTime() - new Date().getTime()) / 1000)
                    // }
                    // if (this._DataService._Deal.Deals != undefined && this._DataService._Deal.Deals != null && this._DataService._Deal.Deals.length > 0) {
                    //     this._DataService._Deal.Deals.forEach(element => {
                    //         element.CountdownConfig =
                    //         {
                    //             leftTime: Math.round((new Date(element.EndDate).getTime() - new Date().getTime()) / 1000)
                    //         }
                    //         if (this._DataService._Deal.Locations.length > 0) {
                    //             this._DataService._Deal.Locations.forEach(element => {
                    //                 element.Marker =
                    //                 {
                    //                     position: {
                    //                         lat: element.Latitude,
                    //                         lng: element.Longitude,
                    //                     },
                    //                     label: {
                    //                         color: 'red',
                    //                         text: element.Name,
                    //                     },
                    //                     title: element.Name,
                    //                     options: { animation: google.maps.Animation.BOUNCE },
                    //                 }
                    //             });
                    //         }

                    //     });
                    // }
                    // this._Categories = _Response.Result.Data;
                    // this._CategoriesSorted = _Response.Result.Data;
                    // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Categories, this._Categories);
                }
            },
            _Error => {
                this._DealCodeLoading = false;
                this._HelperService.HandleException(_Error);
            });
    }

    // Account
    _GetBalannce() {
        if (this._HelperService.IsActiveUser) {
            var Bal = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.AccBal);
            if (Bal != null) {
                this._Balance = Bal;
            }
            var pData = {
                Task: 'getaccountbalance',
                Host: this._HelperService.AppConfig.HostType,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        var _Bal = _Response.Result;
                        _Bal.Balance = _Bal.Balance / 100;
                        this._Balance = _Bal;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.AccBal, _Bal);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
    }


    TimerCode(DealEndDate) {
        let date = moment.utc(DealEndDate).format('YYYY-MM-DD HH:mm:ss');
        let stillUtc = moment.utc(date).toDate();
        let local = moment(stillUtc).local().format('YYYY-MM-DD HH:mm:ss');

        let timerlog = { timeRemaining: '--', isTimeLessThanDay: false, leftTime: 0 };
        var difference = moment(local).diff(moment(), 'hours');
        if (difference < 0) {
            timerlog.timeRemaining = "Deal No Longer Available";
            timerlog.isTimeLessThanDay = false;
        } else if (difference >= 0 && difference <= 24) {
            timerlog.isTimeLessThanDay = true;
            // timerlog.leftTime = (difference * 60 * 60);
            timerlog.leftTime = moment(local).diff(moment(), 'seconds');
        } else {
            timerlog.timeRemaining = Math.round(difference / 24) + " Days Remaining";
            timerlog.isTimeLessThanDay = false;
        }
        return timerlog;
    }






    //#region  Home Screen
    _GetDeals_Promo_New(isCountryChanged?) {
        // var Cache = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.DealsNew);
        // if (!isCountryChanged && Cache != null) {
        //     this._DealsPromotionNew = Cache;
        // } else {
        //     this._DealsPromotionNew = [];
        // }
        var pData: IDealRequest = {
            Task: 'getdeals',
            Offset: (this.activePage - 1) * this.Limit,
            Limit: this.Limit,
            SearchCondition: "",
            SortExpression: this.DealsSortOrder,
            Categories: [],
            Merchants: [],
            City: this.ActiveCity,
            CountryId: this._CountrySelcted.ReferenceId,
            CountryKey: this._CountrySelcted.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.TotalRecords = _Response.Result.TotalRecords;
                    if (_Response.Result.Data.length > 0) {
                        this.Offset = this.Offset - 1;
                        this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.Limit);
                        // this.Offset = this.Offset + this.Limit;
                        _Response.Result.Data.forEach(element => {
                            element.CountdownConfig = this.TimerCode(element.EndDate);
                            // {
                            //     leftTime: moment(element.EndDate).diff(moment(), 'seconds'),
                            // }
                        });
                        this._DealsPromotionNew = _Response.Result.Data;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.DealsNew, this._DealsPromotionNew);
                    }
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    // _GetDeals_Promo_Top() {
    //     var Cache = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.DealsPromoTop);
    //     if (Cache != null) {
    //         this._DealsPromotionTop = Cache;
    //     }
    //     var pData: IDealRequest = {
    //         Task: 'getdeals',
    //         Offset: 0,
    //         Limit: 12,
    //         SearchCondition: "",
    //         SortExpression: "Popularity",
    //         Categories: [],
    //         Merchants: [],
    //         City: this.ActiveCity
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             if (_Response.Status == this._HelperService.StatusSuccess) {
    //                 this.Offset = this.Offset + this.Limit;
    //                 _Response.Result.Data.forEach(element => {
    //                     element.CountdownConfig =
    //                     {
    //                         leftTime: moment(element.EndDate).diff(moment(), 'seconds'),
    //                     }
    //                 });
    //                 this._DealsPromotionTop = _Response.Result.Data;
    //                 this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.DealsPromoTop, this._DealsPromotionTop);
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HandleException(_Error);
    //         });
    // }
    _GetDeals_Promo_Discount(isCountryChanged?) {
        // var CacheDeal = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.DealsPromoDiscounts);
        // if (!isCountryChanged && CacheDeal != null) {
        //     this._DealsPromotionDiscount = CacheDeal;
        // }else{
        //     this._DealsPromotionDiscount = [];
        // }
        var pData: IDealRequest = {
            Task: 'getdeals',
            Offset: 0,
            Limit: 12,
            SearchCondition: "",
            SortExpression: "Discount",
            Categories: [],
            Merchants: [],
            City: this.ActiveCity,
            CountryId: this._CountrySelcted.ReferenceId,
            CountryKey: this._CountrySelcted.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data.length > 0) {
                        this.Offset = this.Offset + this.Limit;
                        _Response.Result.Data.forEach(element => {
                            element.CountdownConfig = this.TimerCode(element.EndDate)
                        });
                        this._DealsPromotionDiscount = _Response.Result.Data;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.DealsPromoDiscounts, this._DealsPromotionDiscount);
                    }
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    _GetDeals_Promo_Ending(isCountryChanged?) {
        // var CacheDeal = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.DealsPromoEndingSoon);
        // if (!isCountryChanged && CacheDeal != null) {
        //     this._DealsPromotionEndingSoon = CacheDeal;
        // }else{
        //     this._DealsPromotionEndingSoon = [];
        // }
        var pData: IDealRequest = {
            Task: 'getdeals',
            Offset: (this.activePage - 1) * this.Limit,
            Limit: this.Limit,
            SearchCondition: "",
            SortExpression: "Ending",
            Categories: [],
            Merchants: [],
            City: this.ActiveCity,
            CountryId: this._CountrySelcted.ReferenceId,
            CountryKey: this._CountrySelcted.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.TotalRecords = _Response.Result.TotalRecords;
                    this.Offset = this.Offset - 1;
                    this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.Limit);
                    if (_Response.Result.Data.length > 0) {
                        // this.Offset = this.Offset + this.Limit;
                        _Response.Result.Data.forEach(element => {
                            element.CountdownConfig = this.TimerCode(element.EndDate)
                            // {
                            //     leftTime: moment(element.EndDate).diff(moment(), 'seconds'),
                            // }
                        });
                        this._DealsPromotionEndingSoon = _Response.Result.Data;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.DealsPromoEndingSoon, this._DealsPromotionEndingSoon);
                    }
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    _GetDeals_Promo_DealOfTheDay(isCountryChanged?) {
        // var Cache = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.DealsDealOfTheDay);
        // if (!isCountryChanged && Cache != null) {
        //     this._DealsPromotioDoD = Cache;
        // }else{
        //     this._DealsPromotioDoD = [];
        // }
        var pData: IDealRequest = {
            Task: 'getdealpromotions',
            Offset: (this.activePage - 1)  * this.Limit,
            Limit: this.Limit,
            SearchCondition: "",
            SortExpression: "Newest",
            Categories: [],
            Merchants: [],
            City: this.ActiveCity,
            Location: "Deal Of The Day",
            CountryId: this._CountrySelcted.ReferenceId,
            CountryKey: this._CountrySelcted.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.TotalRecords = _Response.Result.TotalRecords;
                    this.Offset = this.Offset - 1;
                    this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.Limit);
                    if (_Response.Result.Data.length > 0) {
                        _Response.Result.Data.forEach(element => {
                            element.CountdownConfig = this.TimerCode(element.EndDate)
                        });
                        this._DealsPromotioDoD = _Response.Result.Data;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.DealsDealOfTheDay, this._DealsPromotioDoD);
                    }
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    _GetDeals_ProductDeals(isCountryChanged?) {
        // var Cache = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.ProductDeals);
        // if (!isCountryChanged && Cache != null) {
        //     this._DealsProducts = Cache;
        // }else{
        //     this._DealsProducts = [];
        // }
        var pData: IDealRequest = {
            Task: 'getdeals',
            Offset: (this.activePage - 1) * this.Limit,
            Limit: this.Limit,
            SearchCondition: this.DealsSearchCondition,
            SortExpression: this.DealsSortOrder,
            Categories: [],
            Merchants: [],
            City: this.ActiveCity,
            MinPrice: this.MinPrice,
            MaxPrice: this.MaxPrice,
            CountryId: this._CountrySelcted.ReferenceId,
            CountryKey: this._CountrySelcted.ReferenceKey,
            DealTypeCode: this._HelperService.AppConfig.DealTypeCode.ProductDeal
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    // debugger;
                    this.TotalRecords = _Response.Result.TotalRecords;
                    this.Offset = this.Offset - 1;
                    this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.Limit);
                    if (_Response.Result.Data.length > 0) {
                        // this.Offset = this.Offset + this.Limit;
                        _Response.Result.Data.forEach(element => {
                            element.CountdownConfig = this.TimerCode(element.EndDate)
                        });
                        this._DealsProducts = _Response.Result.Data;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.DealsPromoTop, this._DealsProducts);
                    }

                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    _GetDeals_Promo_TopDeals(isCountryChanged?) {
        // var Cache = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.DealsPromoTop);
        // if (!isCountryChanged && Cache != null) {
        //     this._DealsPromotionTop = Cache;
        // }else{
        //     this._DealsPromotionTop = [];
        // }
        var pData: IDealRequest = {
            Task: 'getdealpromotions',
            Offset: (this.activePage - 1) * this.Limit,
            Limit: this.Limit,
            SearchCondition: "",
            SortExpression: "Newest",
            Categories: [],
            Merchants: [],
            City: this.ActiveCity,
            Location: "Top Deals",
            CountryId: this._CountrySelcted.ReferenceId,
            CountryKey: this._CountrySelcted.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    // debugger;
                    this.TotalRecords = _Response.Result.TotalRecords;
                    this.Offset = this.Offset - 1;
                    this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.Limit);
                    if (_Response.Result.Data.length > 0) {
                        // this.Offset = this.Offset + this.Limit;
                        _Response.Result.Data.forEach(element => {
                            element.CountdownConfig = this.TimerCode(element.EndDate)
                        });
                        this._DealsPromotionTop = _Response.Result.Data;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.DealsPromoTop, this._DealsPromotionTop);
                    }

                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    _GetDeals_Promo_FeaturedDeals(isCountryChanged?) {
        // var Cache = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.DealsPromoFeatured);
        // if (!isCountryChanged && Cache != null) {
        //     this._DealsPromotionFeatured = Cache;
        // }else{
        //     this._DealsPromotionFeatured = [];
        // }
        var pData: IDealRequest = {
            Task: 'getdealpromotions',
            Offset: (this.activePage - 1) * this.Limit,
            Limit: this.Limit,
            SearchCondition: "",
            SortExpression: "Newest",
            Categories: [],
            Merchants: [],
            City: this.ActiveCity,
            Location: "Featured Deals",
            CountryId: this._CountrySelcted.ReferenceId,
            CountryKey: this._CountrySelcted.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.TotalRecords = _Response.Result.TotalRecords;
                    this.Offset = this.Offset - 1;
                    this.pager = this.paged.getPager(this.TotalRecords, this.activePage, this.Limit);
                    if (_Response.Result.Data.length > 0) {
                        _Response.Result.Data.forEach(element => {
                            element.CountdownConfig = this.TimerCode(element.EndDate)
                        });
                        this._DealsPromotionFeatured = _Response.Result.Data;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.DealsPromoFeatured, this._DealsPromotionFeatured);
                    }

                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    _GetBookmarks() {
        this._HelperService.IsProcessing = true;
        var pData = {
          Task: 'getbookmarks',
          Offset: 0,
          Limit: 1000,
          SearchCondition: '',
          ReferenceId: 0,
          ReferenceKey: null
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this.TotalBookmarkCount = _Response.Result.TotalRecords;
            }
          },
          _Error => {
            this._HelperService.HandleException(_Error);
          });
      }
    
    WishlistDeal(event, TItem: IDeal) {
        event.stopPropagation();
        var _LoginCheck = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Acc);
        if (!_LoginCheck) {
            this._HelperService.NotifyError(`Please <a href="/auth/login" style="color: #fff; text-decoration: underline;"><u>login</u></a> to wishlist deal`, 5);
        }
        else {
            var pData = {
                Task: 'savebookmark',
                DealKey: TItem.ReferenceKey,
            }
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        TItem.IsBookmarked = _Response.Result.IsBookmarked;
                        this._GetBookmarks();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                }
            );
        }
    }

    showCategorySectionScource = new BehaviorSubject(true);
    $showCategorySectionScource = this.showCategorySectionScource.asObservable();
    isCityChangedScource = new BehaviorSubject(false);
    $isCityChangedScource = this.isCityChangedScource.asObservable();
    //#endregion
}


