import { DatePipe } from '@angular/common';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError, Subscription } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { HostType, IAccount, OResponse } from '../service/interface.service';
import notie from 'notie';
@Injectable()
export class HelperService {
    public IsActiveUser = false;
    public _Account: IAccount =
        {
            Key: "",
            DisplayName: "",
            EmailAddress: "",
            IconUrl: "",
            Name: "",
            LoginTime: new Date(),
            Country:
            {
                Isd: "",
                Iso: "",
                Name: "",
                Symbol: ""
            },
            AccountKey: "",
            AccountId: null
        };
    public IsProcessing = false;
    public StatusSuccess = "Success";
    public StatusError = "Error";
    constructor(
        private _Http: HttpClient
    ) {
        this.RefreshHelper();
        this.CheckAPPVersion();
    }
    public AppConfig =
        {
            APP_VERSION: '0.0.118',
            Host: "",
            Environment: "testing",
            HostType: HostType.Live,
            HostConnect: "",
            CurrencySymbol: "&#8358;",
            ClientHeader: {
                "Content-Type": "application/json; charset=utf-8",
                // "Authorization": "YmZjODdhODZlMmQxNDAzYzk0OGU4NzhhYjBhZGIxMTk=", // LIVE
                "Authorization": "MTZkYWZjYWU5N2NlNDM1ZjlhOWNkZTEwZTUzZWUxYTk=", // TEST
                "ask": ""
            },
            NetworkLocation:
            {
                Acc: {
                    Acc: "api/maddeals/account/",
                    Rating: "api/maddeals/deals/",
                    UserActivity: "api/maddeals/register/"
                },
                MadDeals: {
                    Deals: "api/maddeals/deals/",
                    // Bookmark: "api/maddeals/deals/",
                    // Dealreview: "api/v3/plugins/deals/"
                    Address: "api/v3/plugins/lsaddress/",
                    Shipment: "api/v3/plugins/lsshipment/",
                    Parcel: "/api/v3/plugins/lsparcels/",
                    State: "api/maddeals/sys/",
                    Register: "api/maddeals/register/",
                    Search: "api/maddeals/search/"
                },
                Impersonate:{
                    Impersonate:"api/v3/cust/account/"
                }
            },
            StorageItem:
            {
                ActiveReference: 'acref',
                AllCategories: 'tdealallcat',
                Categories: 'tdealcat',
                Merchants: 'tddealmerchant',
                Deals: 'tdeal',
                Deal: 'tdealitem_',
                DealsNew: 'tdealpromonew',
                DealsDealOfTheDay: 'tdealpromodod',
                DealsPromoDiscounts: 'tdealpromodiscount',
                DealsPromoEndingSoon: 'tdealpromoending',
                DealsPromoTop: 'tdealpromotop',
                ProductDeals: 'tproductdeal',
                DealsPromoFeatured: 'tdealpromofeatured',
                DealCode: 'tdealcode',
                Acc: 'acc',
                AccBal: 'accbal',
                HomeSl1: 'homesl1',
                Cities: 'cities',
                SearchStates: 'searchstates',
                AllStates: 'allstates',
                ActiveCity: 'activecity',
                ActiveState: 'activestate',
                ActiveCountry: 'activecountry',
                Profile: 'profile',
                bookmarkedDeals: 'bookmarkeddeals',
                CustomerDealHistory: 'customerdealhistory',
                PurchasedDealHistory: 'purchaseddealhistory',
                CustomerCart: 'customerdealcart',
                GuestUser: 'guestuser',
            },
            DealTypeCode:
            {
                ProductDeal: 'dealtype.product',
                ServiceDeal: 'dealtype.service',
            },
            DealCodeValidityTypeCode: {
                DealEndDate: 'dealenddate',
                DaysAfterPurchase: 'daysafterpurchase',
                Hour: 'hour',
                Date: 'date'
            },
            DeliveryTypeCode:
            {
                InStorePickUp: 'deliverytype.instore',
                Delivery: 'deliverytype.delivery',
                InStoreAndDelivery: 'deliverytype.instoreanddelivery',
            },
            EmailPattern: '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$',
            PayStackKey: "pk_live_4acd36db0e852af843e16e83e59e7dc0f89efe12",
            FlutterwaveKey: "FLWPUBK_TEST-a6e984618172d190fd60d61b15673910-X",
            MixPanelToken: "07732bd9b246df7d5df3cc31f63a8583",
            MerchantPanelUrl: "https://merchant-dealday.thankucash.dev",
            Partners_DocumentApi:"https://docs.dealday.africa",
            isMixpanelDisable: true,
            MinPanelEvents: {
                LandingPage: "WEB_LANDING_PAGE",
                home: "WEB_HOME_PAGE",
                category: "WEB_DEALS_CATEGORY_PAGE",
                deals: "WEB_DEALS_LIST_PAGE",
                buydeal: "WEB_BUY_DEAL_PAGE",
                profile: "WEB_PROFILE_PAGE",
                dealcode: "WEB_DEALCODE_PAGE",
                DealViewed: "WEB_DEALS_VIEW_PAGE",
                Checkout: "WEB_CHECKOUT",
                BuyNow: "WEB_BUY_NOW_PAGE",
                Payment: "WEB_PAYMENT",
                Registration: "WEB_REGISTRATION",
                DealSearch: "WEB_USER_SEARCH",
                Wishlist: "WEB_WISHLIST",
                LocationSearch: "WEB_LOCATION_SEARCH",
                Page404: "WEB_404_PAGE",
                LoginInitiated: "WEB_LOGIN_INITIATED",
                LoginFailed: "WEB_LOGIN_FAILED",
                LoginSuccessful: "WEB_LOGIN_SUCCESSFUL",
                RegistrationInitiated: "WEB_REGISTRATION_INITIATED",
                RegistrationFailed: "WEB_REGISTRATION_FAILED",
                RegisteredSuccessfully: "WEB_REGISTERED_SUCCESSFULLY",
                RegistrationUserAlreadyExist: "WEB_REGISTRATION_USER_ALREADY_EXIST",
                PinVerficationInitiated: "WEB_PIN_VERFICATION_INITIATED",
                PinVerficationFailed: "WEB_PIN_VERFICATION_FAILED",
                PinVerficationPassed: "WEB_PIN_VERFICATION_PASSED",
                WebErrorUnableToBuy : 'WEB_ERROR_UNABLE_TO_BUY'
              }
        };

    Truncate(value: string, limit = 20, completeWords = false, ellipsis = '...') {
        if (value != undefined && value != null && value != "") {
            if (completeWords) {
                limit = value.substr(0, limit).lastIndexOf(' ');
            }
            return value.length > limit ? value.substr(0, limit) + ellipsis : value;
        }
        else {
            return value;
        }
    }
    GetPercentage(BigAmount, SmallAmount) {
        let percentage = 0;
        if (BigAmount && SmallAmount) {
            percentage = Math.round((((BigAmount - SmallAmount) * 100) / BigAmount));
        }
        return percentage;
    }

    RefreshHelper() {
        let activeCountry = this.GetStorage(this.AppConfig.StorageItem.ActiveCountry);
        this.AppConfig.Host = window.location.host;
        if(!activeCountry?.PayStackKey || !activeCountry?.PayStackKey._Live){
            localStorage.clear();
        }
        if (this.AppConfig.Host == "deals.thankucash.com" || this.AppConfig.Host == "thankucash.com" || this.AppConfig.Host == "dealday.africa" || this.AppConfig.Host == "www.dealday.africa") {
            this.AppConfig.Environment = 'live';
            this.AppConfig.HostConnect = "https://dealsconnect.thankucash.com/";
            this.AppConfig.PayStackKey = activeCountry?.PayStackKey._Live; //"pk_live_4acd36db0e852af843e16e83e59e7dc0f89efe12";
            this.AppConfig.FlutterwaveKey = activeCountry?.FlutterwaveKey._Live;
            this.AppConfig.ClientHeader.Authorization = "YmZjODdhODZlMmQxNDAzYzk0OGU4NzhhYjBhZGIxMTk=";
            this.AppConfig.MixPanelToken = "bbc782192ac7b369bb1a1daf1d84c71c";
            this.AppConfig.isMixpanelDisable = false;
            this.AppConfig.MerchantPanelUrl = "https://merchant.dealday.africa/";
        }

        else if (this.AppConfig.Host == "beta-deals.thankucash.com")
        {
             this.AppConfig.Environment = 'live';
             this.AppConfig.HostConnect = "https://beta-connect.thankucash.com/";
             this.AppConfig.PayStackKey = activeCountry?.PayStackKey._Live; //"pk_live_4acd36db0e852af843e16e83e59e7dc0f89efe12";
             this.AppConfig.FlutterwaveKey = activeCountry?.FlutterwaveKey._Live;
             this.AppConfig.ClientHeader.Authorization = "YmZjODdhODZlMmQxNDAzYzk0OGU4NzhhYjBhZGIxMTk=";
             this.AppConfig.MixPanelToken = "bbc782192ac7b369bb1a1daf1d84c71c";
             this.AppConfig.isMixpanelDisable = false;
             this.AppConfig.MerchantPanelUrl = "http://beta-merchant-dealday.thankucash.com/";
         }

        else if (this.AppConfig.Host == "deals.thankucash.com" || this.AppConfig.Host == "thankucash.com") {
            this.AppConfig.Environment = 'live';
            this.AppConfig.HostConnect = "https://dealsconnect.thankucash.com";
            this.AppConfig.PayStackKey = activeCountry?.PayStackKey._Live; //"pk_live_4acd36db0e852af843e16e83e59e7dc0f89efe12";
            this.AppConfig.FlutterwaveKey = activeCountry?.FlutterwaveKey._Live;
            this.AppConfig.ClientHeader.Authorization = "YmZjODdhODZlMmQxNDAzYzk0OGU4NzhhYjBhZGIxMTk=";
            this.AppConfig.MixPanelToken = "bbc782192ac7b369bb1a1daf1d84c71c";
            this.AppConfig.isMixpanelDisable = false;
            this.AppConfig.MerchantPanelUrl = "https://merchant.dealday.africa/";
        }
        else if (this.AppConfig.Host == "deals.thankucash.co") {
            this.AppConfig.HostConnect = "https://connect.thankucash.co/";
            this.AppConfig.PayStackKey = activeCountry?.PayStackKey._Test; //"pk_test_4cca266d686312285a54570e20b46a808ae0d0f6";
            this.AppConfig.FlutterwaveKey = activeCountry?.FlutterwaveKey._Test;
            this.AppConfig.ClientHeader.Authorization = "MThkYTY0OGE1OTAyNGYyYmE5M2UyZTA3MzE5YjE3NjQ=";
            this.AppConfig.MixPanelToken = "0a56f1471081c550b472dd32de7bd6ef";
            this.AppConfig.isMixpanelDisable = false;
            this.AppConfig.MerchantPanelUrl = "https://merchant-dealday.thankucash.tech/";
        }
        else if (this.AppConfig.Host == "testdeals.thankucash.com") {
            this.AppConfig.HostConnect = "https://testconnect.thankucash.com/";
            this.AppConfig.PayStackKey = activeCountry?.PayStackKey._Test; //"pk_test_4cca266d686312285a54570e20b46a808ae0d0f6";
            this.AppConfig.FlutterwaveKey = activeCountry?.FlutterwaveKey._Test;
            this.AppConfig.ClientHeader.Authorization = "MThkYTY0OGE1OTAyNGYyYmE5M2UyZTA3MzE5YjE3NjQ=";
            this.AppConfig.isMixpanelDisable = true;
            this.AppConfig.MerchantPanelUrl = "https://merchant.dealday.africa/";
        }
        else if (this.AppConfig.Host == "deals.thankucash.dev") {
            this.AppConfig.HostConnect = "https://dealsconnect.thankucash.dev/";
            this.AppConfig.PayStackKey = "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6";
            this.AppConfig.ClientHeader.Authorization = "MThkYTY0OGE1OTAyNGYyYmE5M2UyZTA3MzE5YjE3NjQ=";
            this.AppConfig.PayStackKey = activeCountry?.PayStackKey._Test; //"pk_test_4cca266d686312285a54570e20b46a808ae0d0f6";
            this.AppConfig.FlutterwaveKey = activeCountry?.FlutterwaveKey._Test;
            this.AppConfig.MixPanelToken = "2e09823db2f7153f946deaa139a4d3d3";
            this.AppConfig.isMixpanelDisable = false;
            this.AppConfig.MerchantPanelUrl = "https://merchant-dealday.thankucash.dev/";
        }
        else if (this.AppConfig.Host == "deals.thankucash.tech") {
            this.AppConfig.HostConnect = "https://dealsconnect.thankucash.tech/";
            this.AppConfig.PayStackKey = "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6";
            this.AppConfig.ClientHeader.Authorization = "MThkYTY0OGE1OTAyNGYyYmE5M2UyZTA3MzE5YjE3NjQ=";
            this.AppConfig.PayStackKey = activeCountry?.PayStackKey._Test; //"pk_test_4cca266d686312285a54570e20b46a808ae0d0f6";
            this.AppConfig.FlutterwaveKey = activeCountry?.FlutterwaveKey._Test;
            this.AppConfig.MixPanelToken = "73c6798faf7265d7aeaecbc0a8803139";
            this.AppConfig.isMixpanelDisable = false;
            this.AppConfig.MerchantPanelUrl = "https://merchant-dealday.thankucash.tech/";
        }
        else {
            this.AppConfig.HostConnect = "https://localhost:5002/";
            this.AppConfig.PayStackKey = activeCountry?.PayStackKey._Test; 
            this.AppConfig.FlutterwaveKey = activeCountry?.FlutterwaveKey._Test;
            this.AppConfig.ClientHeader.Authorization = "MThkYTY0OGE1OTAyNGYyYmE5M2UyZTA3MzE5YjE3NjQ=";
            this.AppConfig.isMixpanelDisable = true;
            this.AppConfig.MerchantPanelUrl = "https://merchant-dealday.thankucash.dev/";
        }
        var Account = this.GetStorage(this.AppConfig.StorageItem.Acc);
        if (Account != null) {
            this._Account = Account;
            this.AppConfig.ClientHeader.ask = this._Account.Key;
            this.IsActiveUser = true;
        }
        else {
            this.IsActiveUser = false;
        }
    }
    CheckAPPVersion(){
        if(typeof localStorage.getItem('APP_VERSION') == 'undefined' || localStorage.getItem('APP_VERSION') == null || localStorage.getItem('APP_VERSION') == ''){
            localStorage.setItem('APP_VERSION', this.AppConfig.APP_VERSION);
        }
        if(localStorage.getItem('APP_VERSION') != this.AppConfig.APP_VERSION){
            localStorage.clear();
            localStorage.setItem('APP_VERSION', this.AppConfig.APP_VERSION);
            window.location.href = '/home';
        }
    }
    SaveStorage(StorageName, StorageValue, encode?) {
        try {
            var StringV = encode ? btoa(unescape(encodeURIComponent(JSON.stringify(StorageValue)))) : btoa(JSON.stringify(StorageValue));
            localStorage.setItem(StorageName, StringV);
            return true;
        } catch (e) {
            return false;
        }
    }
    SaveStorageValue(StorageName, StorageValue) {
        try {
            localStorage.setItem(StorageName, btoa(StorageValue));
            return true;
        } catch (e) {
            return false;
        }
    }
    GetStorage(StorageName) {
        var StorageValue = localStorage.getItem(StorageName);
        if (StorageValue != undefined) {
            if (StorageValue != null) {
                if (StorageValue != "") {
                    return JSON.parse(atob(StorageValue));
                }
                else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    GetStorageValue(StorageName) {
        var StorageValue = localStorage.getItem(StorageName);
        if (StorageValue != undefined) {
            if (StorageValue != null) {
                if (StorageValue != "") {
                    return atob(StorageValue);
                }
                else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    NotifySuccess(Message, time: number = 3) {
        notie.alert({
            type: "1",
            text: Message,
            time
        });
    }
    NotifyWarning(Message) {
        notie.alert({
            type: "2",
            text: Message,
        });
    }
    NotifyError(Message, time: Number = 3) {
        notie.alert({
            type: "3",
            text: Message,
            time
        });
    }
    NotifyInfo(Message) {
        notie.alert({
            type: "4",
            text: Message,
        });
    }
    DeleteStorage(StorageName) {
        localStorage.removeItem(StorageName);
        return true;
    }
    GetAmount(Value) {
        if (Value != undefined || Value != null) {
            return parseFloat(Value).toFixed(2);
        }
        else {
            return Value;
        }
    }
    newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0,
                v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
    HandleException(Exception: HttpErrorResponse) {
        this.IsProcessing = false;
        if (
            Exception.status != undefined &&
            Exception.error != null &&
            Exception.status == 401
        ) {
            var ResponseData = JSON.parse(atob(Exception.error.zx)) as OResponse;
            //   this.NotifyError(ResponseData.Message);
            debugger;
            setTimeout(() => {
                window.location.href = "/home";
            }, 1500);
        } else {
            if (Exception.error instanceof Error) {
                // this.NotifyError(
                //   "Sorry, error occured while connecting server:" +
                //   Exception.error.message
                // );
            } else {
                // this.NotifyError(
                //   "Sorry, error occured while connecting server : " +
                //   Exception.message +
                //   " Response Code -" +
                //   Exception.status
                // );
            }
        }
    }
    PostData(PostLocation, Data): Observable<OResponse> {
        var CountryDetails = this.GetStorage(this.AppConfig.StorageItem.ActiveCountry);
        if (CountryDetails != null) {
            Data.CountryId = CountryDetails.ReferenceId;
            Data.CountryKey = CountryDetails.ReferenceKey;
        }
        var NetworkLocation = this.AppConfig.HostConnect + PostLocation + Data.Task;
        let _Headers = new HttpHeaders(this.AppConfig.ClientHeader);
        var RequestData = JSON.stringify(Data);
        return this._Http.post<OResponse>(NetworkLocation, RequestData, {
                headers: _Headers,
            })
            .pipe(map((_Response) => _Response as OResponse))
            .pipe(catchError((error: HttpErrorResponse) => throwError(error)));
    }
    getTimeDifference(){
        let now = new Date();
        let endOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 23,59,59)
        return Math.round(  ( endOfDay.getTime() - now.getTime() ) /1000)
    }

    replaceImageSrc(data:string, categoryName:any){
        if(!categoryName){
            return  'assets/images/banners/default-img.jpg'
        }
        switch(categoryName){
             case 'Groceries':
                 return 'assets/images/banners/food.jpg'
             break;
             case 'Carry-Go':
                 return 'assets/images/banners/carrygo.jpg'
             break;
             case 'Health & fitness':
                 return 'assets/images/banners/health&fitness.jpg'
             break;
             case 'Hotels and Travels':
                 return 'assets/images/banners/hotel&travels.jpg'
             break;
             case 'Fun and Events':
                 return 'assets/images/banners/fun&games.jpg'
             break;
             case 'Games':
                 return 'assets/images/banners/fun&games.jpg'
             break;
             case 'Beauty & Spa':
                 return 'assets/images/banners/beauty&spa.jpg'
             case 'Professional Services':
                 return 'assets/images/banners/professional.jpg'
             case 'Training':
                 return 'assets/images/banners/training.jpg'
            case 'Fashion':
                 return 'assets/images/banners/fashion.jpg'
            case 'Electronics':
                 return 'assets/images/banners/electronics.jpg'
             default:
                 return 'assets/images/banners/default-img.jpg'
         }
     }
}
