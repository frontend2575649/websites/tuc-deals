import { Injectable } from "@angular/core";
import * as mixpanel from 'mixpanel-browser';
import { environment } from "src/environments/environment";
import { HelperService } from "./helper.service";
@Injectable({ providedIn: 'root' })
export class MixpanelService {
    constructor(private _HelperService: HelperService) {

    }

    /**
   * Initialize mixpanel.
   * @memberof MixpanelService
   */
    init(): void {
        mixpanel.init(this._HelperService.AppConfig.MixPanelToken, {
            debug: !environment.production, ignore_dnt: true,
            loaded: function (mixpanel) { 
             }
        });
        if(this._HelperService.AppConfig.isMixpanelDisable){
            mixpanel.disable();
        }
    }

    /**
     * Identify user with userId 
     * @param {string} userId user mobile number 
     */
    identify(userId: string): void {
        mixpanel.identify(userId);
    }

    /**
     * Clears super properties and generates a new random distinct_id for this instance. Useful for clearing data when a user logs out. 
     */
    reset(): void {
        mixpanel.reset();
    }

    /**
     * alias user with userId 
     * @param {string} userId user mobile number 
     */
    alias(userId: string): void {
        mixpanel.alias(userId);
    }

    /**
     * set user Profile 
     * @param {string} userProperties user mobile number 
     */
    setProfile(userProperties: any): void {
        mixpanel.people.set(userProperties);
    }


    /**
     * Push new action to mixpanel.
     *
     * @param {string} id Name of the action to track.
     * @param {*} [action={}] Actions object with custom properties.
     * @memberof MixpanelService
     */
    track(id: string, action: any = {}): void {
        mixpanel.track(id, action);
    }
}