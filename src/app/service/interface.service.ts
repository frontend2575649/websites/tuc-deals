export interface OResponse {
    Status: string;
    EndDate: any;
    Message: string;
    ResponseCode: string;
    Result: any;
}

export enum HostType {
    Live,
    Test,
    Local
}

export interface IMerchants {
    ReferenceKey: string;
    Name: string;
    IconUrl: string;
    BackgroundColor?: any;
    Deals: number;
    TotalDeals?: number;
    IsChecked?: boolean;
    CategoryDistribution: IMerchantCategoryDistribution[];
}
export interface IMerchantCategoryDistribution {
    CategoryKey: string;
    Count: number;
    CategoryName:string;
}
export interface ICategory {
    ReferenceKey: string;
    Name: string;
    IconUrl: string;
    Deals: number;
    IsChecked?: boolean;
    SubCategories?: ISubCategory[];
}
export interface ISubCategory {
    ReferenceKey: string;
    Name: string;
    IconUrl: string;
    Deals: number;
    IsChecked?: boolean;
}
export interface IDeal {
    ReferenceId: number;
    ReferenceKey: string;
    Title: string;
    MerchantKey: string;
    MerchantName: string;
    MerchantIconUrl: string;
    CategoryKey: string;
    CategoryName: string;
    EndDate: Date;
    ImageUrl: string;
    ActualPrice: string;
    SellingPrice: string;
    IsBookmarked: boolean;
    IsDealpurchased: boolean;
    Location: string;
    Gallery: string[];
    Address: string;
    CityName: string;
    DealTypeCode?: string;
    DeliveryTypeCode?: string;
    Slug?:string;
    CountdownConfig:
    {
        leftTime: number,
        timeRemaining?: string,
        isTimeLessThanDay?: boolean
    };
    Views: number;
    IsSoldout?: boolean;
    CityAreaName?:string;
    DiscountPercentage:number;
    IsFlashDeal:boolean;
    ItemCount:number
}

export interface IDealReview {
    Offset: number,
    Limit: number,
    TotalRecords: number,
    List: IDealReviewItem[];
}
export interface IDealReviewItem {
    ReferenceId: number,
    ReferenceKey: string,
    AccountId: string,
    AccountKey: string,
    AccountDisplayName: string,
    AccountMobileNumber: string,
    AccountIconUrl: string,
    Rating: string,
    IsWorking: string,
    Review: string,
    Comment: string,
    StatusId: string,
    CreateDate: Date,
    RatingContent: string
}


export interface IDealRequest {
    Task: string;
    ReferenceKey?: string;
    SearchCondition?: string;
    SortExpression?: string;
    Offset: number;
    Limit: number;
    MinPrice?: number;
    MaxPrice?: number;
    Categories: string[];
    SubCategories?: string[];
    Merchants: string[];
    City: string;
    Location?: string;
    CountryId: string,
    CountryKey: string,
    DealTypeCode?:string;
}

export interface IDealDetails {
    MerchantId?: number;
    ReferenceId?: number;
    ReferenceKey?: string;
    StatusCode?: string;
    Title?: string;
    MerchantKey?: string;
    MerchantName?: string;
    MerchantIconUrl?: string;
    CategoryKey?: string;
    CategoryName?: string;
    Description?: string;
    Terms?: string;
    EndDate: Date;
    ImageUrl?: string;
    ActualPrice?: number;
    SellingPrice?: number;
    DiscountAmount?: number;
    DiscountPercentage?: number;
    IsBookmarked: boolean;
    IsDealpurchased: boolean;
    Location?: string;
    Address?: string;
    Gallery?: string[];
    Quantity: number;
    Fee?: number;
    CountdownConfig:
    {
        leftTime: number,
    };
    SoldCount?: number;
    Deals?: IDeal[],
    Locations: IDealLocation[],
    Views?: number;

    DealTypeCode?: string;
    DeliveryTypeCode?: string;
    MaximumUnitSalePerPerson?: number;
    TotalReviews?: number;
    AverageRatings: number;
    AvailableQuantityPerPerson?: number;
    AvailableQuantity?: number;
    DealCodeValidityDays?: number;
    DealCodeValidityTypeCode?: string;
    DealCodeValidityTypeName?: string;
    DealCodeValidityEndDate?: string;
}



export interface IDealLocation {
    Name: string;
    Address: string;
    Latitude: number;
    Longitude: number;
    Marker: any;
}
export interface ICountry {
    Name: string;
    Isd: string;
    Iso: string;
    Symbol: string;
}

export interface IAccount {
    Key: string;
    LoginTime: Date;
    DisplayName: string;
    EmailAddress: string;
    Name: string;
    IconUrl: string;
    Country: ICountry;
    AccountKey: string;
    AccountId: number | null;
}
export interface IBalance {
    Balance: number;
}


export interface IDealCode {
    DealId?: string;
    DealCodeId?: string;
    ItemCode?: string;
    StartDate?: Date;
    EndDate?: Date;
    StatusCode?: string;
    StatusName?: string;
    TotalAmount?: number;
    TTotalAmount?: number;
    Amount?: number;
    Title?: string;
    Description?: string;
    ImageUrl?: string;
    Terms?: string;
    MerchantId?: number;
    MerchantDisplayName?: string;
    MerchantIconUrl?: string;
    MerchantContactNumber?: string;
    MerchantEmailAddress?: string;
    RedeemInstruction?: string;
    StatusId?: number;
    CreateDate?: Date;
    DealCodeLocation?: IDealCodeLocation[];
}
export interface IDealCodeLocation {
    DisplayName: string;
    Address: string;
    Latitude: number;
    Longitude: number;
    Marker: any;
}

export interface ICity {
    ReferenceId: string;
    ReferenceKey: string;
    Name: string;
    Deals: number;
}

export interface IProfile {
    DateOfBirth: Date;
    DisplayName: string;
    EmailAddress: string;
    FirstName: string;
    MiddleName: string;
    LastName: string;
    Gender: string;
    IconUrl: any;
    Name: string;
    Address: String;
    MobileNumber: String;
    ReferralCode?: string;
}

export interface SnowFlakeConfig {
	depth: number ;
	left: number ;
	speed: number ;
}

