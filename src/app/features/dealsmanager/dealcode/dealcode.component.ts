import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { IBalance, OResponse } from 'src/app/service/interface.service';
declare var $: any;

@Component({
    selector: 'tuc-dealcode',
    templateUrl: './dealcode.component.html',
})
export class TUCDealCodeComponent implements OnInit {
    zoom = 12;
    center: google.maps.LatLngLiteral = {
        lat: 6.5557907,
        lng: 3.3856198
    };
    options: google.maps.MapOptions = {
        mapTypeId: 'roadmap',
        zoomControl: true,
        scrollwheel: false,
        disableDoubleClickZoom: true,
        maxZoom: 19,
        minZoom: 1,
    };
    constructor(private _ActivatedRoute: ActivatedRoute, public _DataService: DataService, public _HelperService: HelperService) {
    }

    _PaymentDetails: any =
        {
            Amount: 0,
            Fees: 0,
            WalletBalance: 0,
            TotalAmount: 0
        }

    ImageSelected(Image) {
        this._DataService._Deal.ImageUrl = Image;
    }
    ngOnInit() {
        this._DataService.DealId = this._ActivatedRoute.snapshot.params['referenceid'];
        this._DataService.DealCode = this._ActivatedRoute.snapshot.params['referencekey'];
        this._DataService._GetDealCode();
        navigator.geolocation.getCurrentPosition((position) => {
            this.center = {
                lat: position.coords.latitude,
                lng: position.coords.longitude,
            }
        })
    }
    cutString(data:string){
        return (data && data.length > 28) ? `${data.substring(0,28)}...` : data;
    }
}