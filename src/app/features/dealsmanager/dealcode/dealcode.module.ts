import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TUCDealCodeComponent } from './dealcode.component';
import { CountdownModule } from 'ngx-countdown';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { GoogleMapsModule } from '@angular/google-maps'
import { NgOtpInputModule } from 'ng-otp-input';
import { Angular4PaystackModule } from 'angular4-paystack';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
const routes: Routes = [
    { path: '', component: TUCDealCodeComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUDealCodeRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TUDealCodeRoutingModule,
        CountdownModule,
        CarouselModule,
        GoogleMapsModule,
        NgOtpInputModule,
        Angular4PaystackModule.forRoot('pk_test_xxxxxxxxxxxxxxxxxxxxxxxx'),
        NgxSkeletonLoaderModule.forRoot({theme: {'margin-bottom': '0'}}),
    ],
    declarations: [TUCDealCodeComponent]
})
export class TUCDealCodeModule {
}
