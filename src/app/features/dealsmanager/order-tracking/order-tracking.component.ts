import { CommonModule } from "@angular/common";
import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ActivatedRoute, RouterModule } from "@angular/router";
import { GoogleMapsModule } from '@angular/google-maps'
import { DomSanitizer } from '@angular/platform-browser';
import { DataService } from "src/app/service/data.service";
import { HelperService } from "src/app/service/helper.service";
import { Observable } from "rxjs";
import { OResponse } from "src/app/service/interface.service";

@Component({
    standalone: true,
    selector: 'tuc-order-tracking',
    templateUrl: './order-tracking.component.html',
    styleUrls: ['./order-tracking.component.scss'],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        GoogleMapsModule
    ],
    encapsulation: ViewEncapsulation.ShadowDom
})

export class TUCOrderTrackingComponent implements OnInit {
    zoom = 12;
    center: google.maps.LatLngLiteral = {
        lat: 6.441703,
        lng: 3.5039371
    };
    options: google.maps.MapOptions = {
        mapTypeId: 'roadmap',
        zoomControl: true,
        scrollwheel: false,
        disableDoubleClickZoom: true,
        maxZoom: 19,
        minZoom: 1,
        fullscreenControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        panControl: true
    };
    TrackingSteps: number = 1;
    TrackingNumber: string;
    TrackingDetails: any = {};
    CurrentState: number = 0;
    OrderStatus = {
        'New': 'orderstatus.new',
        'PendingConfirmation': 'orderstatus.pendingconfirmation',
        'Confirmed': 'orderstatus.confirmed',
        'Preparing': 'orderstatus.preparing',
        'Ready': 'orderstatus.ready',
        'ReadyToPickup': 'orderstatus.readytopickup',
        'OutForDelivery': 'orderstatus.outfordelivery',
        'DeliveryFailed': 'orderstatus.deliveryfailed',
        'CancelledByUser': 'orderstatus.cancelledbyuser',
        'CancelledBySeller': 'orderstatus.cancelledbyseller',
        'CancelledBySystem': 'orderstatus.cancelledbysystem',
        'Delivered': 'orderstatus.delivered',
        'PickUped': 'orderstatus.pickuped',
        'ProcessedAtFacility': 'orderstatus.procvessedatfacility',
        'DepartedToFacility': 'orderstatus.departedtofacility',
        'ArrivedAtFacility': 'orderstatus.arrivedatfacility',
        'DepartedFromFacility': 'orderstatus.departedfromfacility',
    }
    constructor(
        public _HelperService: HelperService,
        public _DataService: DataService,
        public sanitizer: DomSanitizer,
        private _activatedRoute: ActivatedRoute
    ) { }
    ngOnInit(): void {
        this._activatedRoute.queryParams.subscribe((params: any) => {
            this.TrackingNumber = params.trackingnumber;
            this.changeTrackingNumber({ target: { value: params.trackingnumber } })
        })
    }
    changeTrackingNumber(event) {
        console.log(event.target.value);
        if (event.target.value && event.target.value.length >= 10) {
            // call trackorder api
            var pData = {
                Task: 'trackorder',
                TrackingNumber: this.TrackingNumber,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this.TrackingDetails = _Response.Result;
                        this.TrackingSteps = this.TrackingDetails.IsDelivery ? 3 : 4;
                        this.SetTrackingUI();
                    } else {
                        this.TrackingSteps = 2;
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    SetTrackingUI() {
        let status = this.TrackingDetails.OrderDetails.OrderStatus;
        if (status == this.OrderStatus.Preparing || status == this.OrderStatus.Confirmed) {
            this.CurrentState = 1;
            return;
        }
        else if (status == this.OrderStatus.ReadyToPickup || status == this.OrderStatus.Ready) {
            this.CurrentState = 2;
            return;
        }
        else if ([this.OrderStatus.OutForDelivery, this.OrderStatus.PickUped, this.OrderStatus.ProcessedAtFacility, this.OrderStatus.DepartedFromFacility, this.OrderStatus.DepartedToFacility, this.OrderStatus.ArrivedAtFacility].includes(status)) {
            this.CurrentState = 3;
            return;
        }
        else if (status == this.OrderStatus.Delivered) {
            this.CurrentState = 4;
            return;
        }
        else if ([this.OrderStatus.CancelledBySeller, this.OrderStatus.CancelledBySystem, this.OrderStatus.CancelledByUser].includes(status)) {
            this.CurrentState = 5;
            return;
        }
    }
}