import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TUCDealPaymentComponent } from './dealpayment.component';
import { CountdownModule } from 'ngx-countdown';
import { NgxStarRatingModule } from 'ngx-star-rating';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { GoogleMapsModule } from '@angular/google-maps'
import { NgOtpInputModule } from 'ng-otp-input';
import { Angular4PaystackModule } from 'angular4-paystack';
import {FlutterwaveModule} from 'flutterwave-angular-v3';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';
import { TUCDealPaymentSuccessfulComponent } from './deal-payment-successful.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareButtonModule } from 'ngx-sharebuttons/button';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';

const routes: Routes = [
    { path: '', component: TUCDealPaymentComponent },
    { path: ':referenceId/paymentsuccessful', component: TUCDealPaymentSuccessfulComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUDealPaymentRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TUDealPaymentRoutingModule,
        CountdownModule,
        CarouselModule,
        GoogleMapsModule,
        NgOtpInputModule,
        SharedModule,
        NgbModule,
        NgxStarRatingModule,
        InfiniteScrollModule,
        FlutterwaveModule,
        Angular4PaystackModule.forRoot('pk_test_xxxxxxxxxxxxxxxxxxxxxxxx'),
        NgxSkeletonLoaderModule.forRoot({theme: {'margin-bottom': '0'}}),
        ShareButtonsModule,
        ShareButtonModule,
        ShareIconsModule,
    ],
    providers: [DatePipe],
    declarations: [TUCDealPaymentComponent, TUCDealPaymentSuccessfulComponent]
})
export class TUCDealPaymentModule {
}
