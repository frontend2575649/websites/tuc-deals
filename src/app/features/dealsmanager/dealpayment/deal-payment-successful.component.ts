import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';

@Component({
  selector: 'app-deal-payment-successful',
  templateUrl: './deal-payment-successful.component.html',
  styleUrls: ['./deal-payment-successful.component.css']
})
export class TUCDealPaymentSuccessfulComponent implements OnInit {
  zoom = 12;
  center: google.maps.LatLngLiteral = {
    lat: 6.5557907,
    lng: 3.3856198
  };
  options: google.maps.MapOptions = {
    mapTypeId: 'roadmap',
    zoomControl: true,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    maxZoom: 19,
    minZoom: 1,
  };
  paymentsuccess: any = {};
  constructor(
    public _DataService: DataService,
    public _HelperService: HelperService,
    private _ActivatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.paymentsuccess = this._HelperService.GetStorage('paymentsuccess');
    if (this.paymentsuccess == undefined || this.paymentsuccess == null) {
      this.router.navigate(['/home']);
    }else if (this.paymentsuccess.paymentsuccess) {    
        this._DataService._GetDealCode();
        navigator.geolocation.getCurrentPosition((position) => {
          this.center = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          }
        })
      }

  }
  navigateToDealCode(): void {
    this._HelperService.DeleteStorage('paymentsuccess');
    this.router.navigate([`/dealcode/${this._DataService._DealCode.ReferenceId}/${this._DataService._DealCode.ReferenceKey}`]);
  }

  isDeliveryDeal() {
    return (this._DataService?._DealCode?.DealTypeCode == 'dealtype.product' && this._DataService._DealCode?.DeliveryTypeCode == 'deliverytype.delivery');
  }

}
