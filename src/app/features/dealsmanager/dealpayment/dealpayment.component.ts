import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Observable, tap } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { IBalance, IDealReview, OResponse } from 'src/app/service/interface.service';
import { NgbCarousel, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpEventType } from '@angular/common/http';
import { MixpanelService } from 'src/app/service/mixpanel.service';
import * as moment from 'moment'
declare var $: any;
import { Flutterwave, InlinePaymentOptions, PaymentSuccessResponse } from "flutterwave-angular-v3"
import { DatePipe } from '@angular/common';

@Component({
    selector: 'tuc-dealpayment',
    templateUrl: './dealpayment.component.html',
    styleUrls: ['./dealpayment.component.css']
})
export class TUCDealPaymentComponent implements OnInit {
    @ViewChild('guestRegistrationModal') guestRegistrationModal: any;
    @ViewChild('tucpayment') tucpayment: any;
    @ViewChild('ngOtpInput') ngOtpInputRef: any;
    @ViewChild('btnpst') btn: any;
    zoom = 12;
    DealQuantity: number = 1;
    center: google.maps.LatLngLiteral = {
        lat: 6.5557907,
        lng: 3.3856198
    };
    options: google.maps.MapOptions = {
        mapTypeId: 'roadmap',
        zoomControl: true,
        scrollwheel: false,
        disableDoubleClickZoom: true,
        maxZoom: 19,
        minZoom: 1,
    };
    public _Balance: IBalance =
        {
            Balance: 0
        }
    DeliveryType: string = '';
    SelectedDeliveryAddress: string = '';
    shippingAddresses: any[] = [];
    isSubmittedForm: boolean = false;
    shippingAddressForm: FormGroup;
    shippingAddressChangeMode: boolean = false;
    deliveryPartnerMode: boolean = false;
    selectedAddress: any = null;
    selectedDeliveryPartner: any = null;
    editForm: string;
    progress: number = 0;
    deliveryPartner: any[] = [];
    deliveryPartnerInfo: any = {};
    finalCheckout: boolean = false;
    fetchAddressIsProcessing: boolean = false;
    public activeTab = "details";
    _DealProcessing: boolean = false;
    tabChange(activeTab) {
        this.activeTab = activeTab;
    }
    @ViewChild('dealCarousel', { static: false }) dealCarousel: NgbCarousel;
    public _OwlDeal: OwlOptions = {
        loop: false,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        autoHeight: false,
        dots: false,
        navSpeed: 700,
        autoWidth: true,
        margin: 8,
        navText: ['<i class="icofont-thin-left"></i>', '<i class="icofont-thin-right"></i>'],
        responsive: {
            0: {
                items: 6
            },
            400: {
                items: 6
            },
            740: {
                items: 6
            },
            940: {
                items: 6
            }
        },
        nav: false
    }
    throttle = 300;
    scrollDistance = 1;
    scrollUpDistance = 2;
    _DealRatingNumber: number | null = 0;
    _Cdata = { Comment: "" };
    isDragging: boolean = false;
    ValidDateAfterPurchase: any;
    constructor(public _NgbModal: NgbModal, public flutterwave: Flutterwave,
                private _ActivatedRoute: ActivatedRoute, public _DataService: DataService,
                public _HelperService: HelperService, private fb: FormBuilder,
                private _MixPanelService: MixpanelService, private _router: Router,
                private _datePipe: DatePipe) {
        this._DealRatingNumber = 0;
        this._router.routeReuseStrategy.shouldReuseRoute = () => false;
        this._DataService._GetDeals();
    }




    public _SLGen: OwlOptions = {
        loop: false,
        mouseDrag: true,
        touchDrag: true,
        autoWidth: true,
        autoHeight: false,
        pullDrag: true,
        dots: false,
        dotsEach: false,
        navSpeed: 700,
        navText: ['<i class="icofont-thin-left"></i>', '<i class="icofont-thin-right"></i>'],
        responsive: {
            0: {
                items: 1,
                autoWidth: false
            },
            400: {
                items: 1
            },
            740: {
                items: 3
            },
            940: {
                items: 4
            }
        },
        nav: true
    }




    ImageSelected(Image, index) {
        this._DataService._Deal.ImageUrl = Image;
        this.dealCarousel.select(`carouselSlide${index}`);
    }
    ngOnInit() {
        this.Stage = 0;
        this._DataService.DealSlug = this._ActivatedRoute.snapshot.params['slug'];
        this._GetDeal();
        this._GetDealReviews();

        navigator.geolocation.getCurrentPosition((position) => {
            this.center = {
                lat: position.coords.latitude,
                lng: position.coords.longitude,
            }
        })
    }
    timeRemaining = "--";
    isTimeLessThanDay: boolean = false;
    TimerCode(DealEndDate) {
        let date = moment.utc(DealEndDate).format('YYYY-MM-DD HH:mm:ss');
        let stillUtc = moment.utc(date).toDate();
        let local = moment(stillUtc).local().format('YYYY-MM-DD HH:mm:ss');
        let difference = moment(local).diff(moment(), 'hours');
        if (difference < 0) {
            this.timeRemaining = "Deal No Longer Available";
            this.isTimeLessThanDay = false;
        } else if (difference >= 0 && difference <= 24) {
            this.isTimeLessThanDay = true;
            this._DataService._Deal.CountdownConfig = { leftTime: moment(local).diff(moment(), 'seconds') };;
        } else if (difference > 24 && difference >= 48) {
            this.timeRemaining = Math.round(difference / 24) + " Day Remaining";
            this.isTimeLessThanDay = false;
        } else {
            this.timeRemaining = Math.round(difference / 24) + " Days Remaining";
            this.isTimeLessThanDay = false;
        }
    }
    _GetDeal() {
        this._HelperService.IsProcessing = true;
        this._DealProcessing = true;
        var pData = {
            Task: 'getdeal',
            Host: this._HelperService.AppConfig.HostType,
            Slug: this._DataService.DealSlug,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsProcessing = false;
                this._DealProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._DataService._Deal = _Response.Result;
                    this._DataService._Deal.AverageRatings = _Response.Result.AverageRatings ? _Response.Result.AverageRatings : 0
                    this._DataService._Deal.DiscountAmount = this._HelperService.GetAmount(this._DataService._Deal.ActualPrice) - this._HelperService.GetAmount(this._DataService._Deal.SellingPrice);
                    // this._GetBalance();
                    // this._DataService._Deal.CountdownConfig =
                    // {
                    //     leftTime: Math.round((new Date(this._DataService._Deal.EndDate).getTime() - new Date().getTime()) / 1000)
                    // }
                    this.TimerCode(_Response.Result.EndDate)
                    // DealTypeCode checking....
                    let { DealTypeCode, DeliveryTypeCode } = this._DataService._Deal;
                    if (DealTypeCode !== this._HelperService.AppConfig.DealTypeCode.ProductDeal) {
                        this.DeliveryType = 'Pickup';
                        this.finalCheckout = true;
                    } else {
                        if (DeliveryTypeCode == this._HelperService.AppConfig.DeliveryTypeCode.InStorePickUp) {
                            this.DeliveryType = 'Pickup';
                            this.finalCheckout = true;
                        }
                        else if (DeliveryTypeCode == this._HelperService.AppConfig.DeliveryTypeCode.Delivery) {
                            this.DeliveryType = 'Delivery';
                            this.finalCheckout = false;
                            this.fetchAddress(true);
                        }
                    }
                    if (this._DataService._Deal.Deals != undefined && this._DataService._Deal.Deals != null && this._DataService._Deal.Deals.length > 0) {
                        this._DataService._Deal.Deals.forEach(element => {
                            element.CountdownConfig =
                            {
                                leftTime: Math.round((new Date(element.EndDate).getTime() - new Date().getTime()) / 1000)
                            }
                            if (this._DataService._Deal.Locations.length > 0) {
                                this._DataService.LocationCenter = {
                                    lat: this._DataService._Deal.Locations[0].Latitude,
                                    lng: this._DataService._Deal.Locations[0].Longitude
                                };
                                this._DataService._Deal.Locations.forEach(element => {
                                    element.Marker =
                                    {
                                        position: {
                                            lat: element.Latitude,
                                            lng: element.Longitude,
                                        },
                                        label: {
                                            color: 'red',
                                            text: element.Name,
                                        },
                                        title: element.Name,
                                        options: { animation: google.maps.Animation.DROP },
                                    }
                                });
                            }

                        });
                    }

                    this._MixPanelService.track(this._HelperService.AppConfig.MinPanelEvents.DealViewed,
                        {
                            DealKey: this._DataService._Deal.ReferenceKey,
                            DealName: this._DataService._Deal.Title,
                            page: 'deal',
                            path: '/deal'
                        })
                               
                    // ** Valid for text
                    switch (this._DataService._Deal.DealCodeValidityTypeCode) {
                        case this._HelperService.AppConfig.DealCodeValidityTypeCode.DaysAfterPurchase: {
                            if (this._DataService._Deal.DealCodeValidityDays) {
                                let days = Math.ceil(this._DataService._Deal.DealCodeValidityDays / 24);
                                this.ValidDateAfterPurchase = (days >= 90) ? '3 months after purchase' :
                                    (days >= 60 && days < 90) ? '2 months after purchase' :
                                        (days >= 30 && days < 60) ? '1 months after purchase' :
                                            `${this._DataService._Deal.DealCodeValidityDays} days after purchase`;
                            }
                            break;
                        }
                        case this._HelperService.AppConfig.DealCodeValidityTypeCode.DealEndDate:
                        case this._HelperService.AppConfig.DealCodeValidityTypeCode.Date: {
                            if (this._DataService._Deal.DealCodeValidityEndDate) {
                                this.ValidDateAfterPurchase = `${this._datePipe.transform(this._DataService._Deal.DealCodeValidityEndDate, 'dd MMM, yyyy')}`
                            }
                            break;
                        }
                        case this._HelperService.AppConfig.DealCodeValidityTypeCode.Hour: {
                            if (this._DataService._Deal.DealCodeValidityDays) {
                                this.ValidDateAfterPurchase = `${this._DataService._Deal.DealCodeValidityDays} hours`
                            }
                            break;
                        }
                    }

                    // this.PaymentDetails.PaymentAmount = this._HelperService.GetAmount(this._DataService._Deal.SellingPrice);
                    // this.PaymentDetails.WalletBalance = 0;
                    // this.PaymentDetails.RemainingAmount = this._HelperService.GetAmount(this.PaymentDetails.PaymentAmount);
                    // this.PaymentDetails.WalletUsageAmount = 0;
                    // this._Categories = _Response.Result.Data;
                    // this._CategoriesSorted = _Response.Result.Data;
                    // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Categories, this._Categories);
                }
                else {
                    this._HelperService.NotifyError(_Response.Message)
                    setTimeout(() => {
                        this._router.navigate(['/'])
                    }, 1500);
                }
            },
            _Error => {
                this._DealProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }
    //rating code--
    public _DealsReviews: IDealReview =
        {
            Offset: 0,
            Limit: 10,
            TotalRecords: 0,
            List: []
        }
    RatingClick(content) {
        const modal = this._NgbModal.open(content, { size: 'sm', centered: true });
        modal.result.then(() => { console.log('When user closes'); }, (err) => {
            this._DealRatingNumber = null;
        })
    }
    SaveRating(Rate) {
        this._HelperService.IsProcessing = true;
        var pData = {
            Task: 'savedealreview',
            Host: this._HelperService.AppConfig.HostType,
            DealId: Rate.ReferenceId,
            DealKey: Rate.ReferenceKey,
            Rating: this._DealRatingNumber,
            Review: this._Cdata.Comment,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Rating, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                    this._Cdata.Comment = "";
                    this._DealRatingNumber = 0;
                    this._NgbModal.dismissAll();
                    this._DealsReviews = {
                        Offset: 0,
                        Limit: 10,
                        TotalRecords: 0,
                        List: []
                    }
                    this._GetDealReviews();
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    _GetDealReviews() {
        var pData = {
            Task: 'getdealreviews',
            Host: this._HelperService.AppConfig.HostType,
            ReferenceId: this._DataService._Deal.ReferenceId,
            ReferenceKey: this._DataService._Deal.ReferenceId,
            Offset: this._DealsReviews.Offset,
            Limit: this._DealsReviews.Limit
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._DealsReviews.Offset = this._DealsReviews.Offset + this._DealsReviews.Limit;
                    _Response.Result.Data.forEach(element => {
                        // element.CreateDate  = this._HelperService
                        element.RatingContent = '';
                        for (let index = 0; index < element.Rating; index++) {
                            element.RatingContent += '<i class="icofont-ui-rating active"></i>';
                        }
                        let activecountry =this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.ActiveCountry);
                        if(activecountry.Name === 'Nigeria'){
                            let date = moment.utc(element.CreateDate)
                            const newDate = date.add(1, 'hour');
                            const newDateString = newDate.format('YYYY-MM-DD HH:mm:ss');
                            element.CreateDate =newDateString;
                            this._DealsReviews.List.push(element);
                        }else{
                            this._DealsReviews.List.push(element)
                        }
                    });
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    _GetDealReviews_onScrollDown() {
        this._GetDealReviews();
    }
    navigateToRoute(route, TItem) {
        if (!this.isDragging && !TItem.IsSoldout) { this._router.navigate(route); }
    }
    on_carouselDrag(dragging: boolean) {
        setTimeout(() => {
            this.isDragging = dragging;
        }, 100)
    }
    checkDeliveryCheckout() {
        let { DealTypeCode, DeliveryTypeCode } = this._DataService._Deal;
        return (DealTypeCode == this._HelperService.AppConfig.DealTypeCode.ProductDeal && (DeliveryTypeCode == this._HelperService.AppConfig.DeliveryTypeCode.Delivery || DeliveryTypeCode == this._HelperService.AppConfig.DeliveryTypeCode.InStoreAndDelivery));
    }
    checkInStoreCheckout() {
        let { DealTypeCode, DeliveryTypeCode } = this._DataService._Deal;
        // console.log(DealTypeCode, DeliveryTypeCode)
        return (DealTypeCode == this._HelperService.AppConfig.DealTypeCode.ServiceDeal || DeliveryTypeCode == this._HelperService.AppConfig.DeliveryTypeCode.InStorePickUp)
    }
    IsBalanceLoaded = false;
    // _GetBalance() {
    //     this.IsBalanceLoaded = false;
    //     if (this._HelperService.IsActiveUser)  {
    //         var pData = {
    //             Task: 'getaccountbalance',
    //             Host: this._HelperService.AppConfig.HostType,
    //         };
    //         let _OResponse: Observable<OResponse>;
    //         _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
    //         _OResponse.subscribe(
    //             _Response => {
    //                 if (_Response.Status == this._HelperService.StatusSuccess) {
    //                     this.IsBalanceLoaded = true;
    //                     this._TransactionReference = this._HelperService.newGuid();
    //                     var _Bal = _Response.Result;
    //                     _Bal.Balance = this._HelperService.GetAmount(_Bal.Balance / 100);
    //                     this._Balance = _Bal;
    //                     this._PaymentDetails.Amount = this._HelperService.GetAmount(this._DataService._Deal.SellingPrice);
    //                     this._PaymentDetails.Fees = 0;
    //                     if (this._Balance.Balance > 0) {
    //                         if (parseFloat(_Bal.Balance) > parseFloat(this._PaymentDetails.Amount)) {
    //                             this._PaymentDetails.WalletBalance = this._HelperService.GetAmount(this._PaymentDetails.Amount);
    //                             this._PaymentDetails.TotalAmount = 0;
    //                         }
    //                         else {
    //                             this._PaymentDetails.WalletBalance = this._HelperService.GetAmount(_Bal.Balance);
    //                             this._PaymentDetails.TotalAmount = this._HelperService.GetAmount((this._HelperService.GetAmount(this._PaymentDetails.Amount) - this._HelperService.GetAmount(_Bal.Balance)));
    //                             this._PaymentDetails.PayableAmount = Math.round(this._HelperService.GetAmount(this._PaymentDetails.TotalAmount) * 100);
    //                         }
    //                     }
    //                     else {
    //                         this._PaymentDetails.WalletBalance = 0;
    //                         this._PaymentDetails.TotalAmount = this._HelperService.GetAmount(this._PaymentDetails.Amount);
    //                         this._PaymentDetails.PayableAmount = Math.round(this._HelperService.GetAmount(this._PaymentDetails.TotalAmount) * 100);
    //                     }
    //                     this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.AccBal, _Bal);
    //                 }
    //             },
    //             _Error => {
    //                 this._HelperService.HandleException(_Error);
    //             });
    //     }
    // }

    customerPin: string = "";
    _TransactionReference = this._HelperService.newGuid();
    IsPinValidated = false;
    onPinChange(otp) {
        if (otp.length == 4) {
            this.customerPin = otp;
        }
    }

    ValidatePin() {
        if (this.IsWalletBalanceUse == false && !this.paymentGateway) {
           return this._HelperService.NotifyError("Please select payment method");
        }
        else if (this.IsWalletBalanceUse == true && this.IsPinValidated == false) {
            if (this.customerPin == undefined || this.customerPin == null || this.customerPin == "") {
                this._HelperService.NotifyError("Enter your account pin ");
            }
            else if (this.customerPin.length != 4) {
                this._HelperService.NotifyError("Enter your valid account pin ");
            }
            else if (this._HelperService._Account.EmailAddress == undefined || this._HelperService._Account.EmailAddress == null || this._HelperService._Account.EmailAddress == "") {
                this._HelperService.NotifyError("Your profile is not updated. Please update your profile to process payment");
            }
            else {
                this._HelperService.IsProcessing = true;
                var pData = {
                    Task: 'validatepin',
                    Host: this._HelperService.AppConfig.HostType,
                    Pin: this.customerPin,
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this.IsPinValidated = true;
                            this.ProcessPayment();
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                            this.IsPinValidated = false;
                            this.ngOtpInputRef.setValue("");
                        }
                    },
                    _Error => {
                        this.IsPinValidated = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        }
        else if(this.IsWalletBalanceUse == true && this.IsPinValidated == true && this.PaymentDetails.PaystackAmount > 0 && !this.paymentGateway){
          return this._HelperService.NotifyError("Please select payment method");
        }
        else {
            this.ProcessPayment();
        }
    }

    paymentCancel() {

    }
    paymentDone(Item) {
        this.BuyDeal();
    }

    BuyDeal() {
        this._HelperService.IsProcessing = true;
        var pData = {
            Task: 'buydeal',
            Host: this._HelperService.AppConfig.HostType,
            Pin: this.customerPin,
            DealKey: this._DataService._Deal.ReferenceKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.DealCode, _Response.Result);
                    this._DataService._GetBalannce();
                    this._HelperService.NotifySuccess(_Response.Message);
                    window.location.href = `/dealcode/${_Response.Result.ReferenceId}/${_Response.Result.ReferenceKey}`;
                }
                else {
                    this.ngOtpInputRef.setValue("");
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    //#region  NEW CODE - START
    public Stage = 0;
    public PurchaseDetails =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Amount: 0,
            Fee: 0,
            DeliveryCharge: 0,
            TotalAmount: 0,
            Balance: 0,
            PaymentSource: 'online'
        };
    public PaymentDetails =
        {
            PaystackAmount: 0,
            WalletAmount: 0,
            // PaymentAmount: 0,
            // RemainingAmount: 0,
            // WalletUsageAmount: 0,
        }
    public PaymentConfiguration =
        {
            AccountId: null,
            AccountKey: null,
            Amount: 0,
            Charge: 0,
            DeliveryCharge: 0,
            EmailAddress: '',
            TotalAmount: 0,
            PaymentMode: 'paystack',
            PaymentReference: '',
            ReferenceId: null,
            TransactionDate: null,
            StatusName: null,
            StatusCode: null,
            UserCards: [],
            Banks: [],
            Balance:
            {
                Credit: 0,
                Debit: 0,
                Balance: 0
            },
            MobileNumber: null,
            DisplayName: null,
            MerchantDisplayName: null,
            PaymentDescription: null,
            IconUrl: null,
        }
    public IsWalletBalanceUse = false;
    PaymentMode = 'paystack';
    Checkout() {
        if (this.finalCheckout) {
            if (!this._HelperService.IsActiveUser) {
                if (this.DeliveryType == 'Pickup') {
                    this._NgbModal.open(this.guestRegistrationModal, { size: 'md', modalDialogClass: 'tuc-guestRegistration-modal' });
                } else {
                    this.InitializeDeal(true);
                }
            } else {
                this.InitializeDeal(false);
            }
        } else if (this.DeliveryType == 'Delivery' && !this.shippingAddressChangeMode) {
            return this._HelperService.NotifyError('Please select delivery address')
        } else if (this.DeliveryType == 'Delivery' && this.deliveryPartnerMode) {
            return this._HelperService.NotifyError('Please select delivery partner')
        } else {
            this._HelperService.NotifyError('Please select delivery option')
            this._MixPanelService.track(
                this._HelperService.AppConfig.MinPanelEvents.WebErrorUnableToBuy, {
                Dealkey: this._DataService._Deal.ReferenceKey,
                DealName: this._DataService._Deal.Title,
                path: window.location.pathname
            });
        }
    }
    registeredUser(event) {
        
        let { user } = event;
        if (event.Key) {
            this._HelperService.AppConfig.ClientHeader.ask = event.Key;
            if (this.DeliveryType == 'Pickup') {
                this.InitializeDeal(true);
            } else {
                this.shippingAddressChangeMode = true;

                this.changeDeliveryAddress(true,
                    {
                        "CityId": user.CityId,
                        "EmailAddress": user.EmailAddress,
                        "FirstName": user.FirstName,
                        "LastName": user.LastName, "AddressLine1": user.AddressLine1, "AddressLine2": user.AddressLine2,
                        "Name": user.Name, "ContactNumber": user.MobileNumber, "StateId": user.StateId, "StateKey": user.StateKey, "ZipCode": null,
                        "IsPrimary": false, "LocationTypeId": user.LocationTypeId, "LocationTypeCode": user.LocationTypeCode, "DisplayName": user.DisplayName,
                        "Landmark": null, "CityAreaId": 0, "Latitude": 0, "Longitude": 0, "MapAddress": null, "Instructions": null,
                        "ReferenceId": event.AddressId
                    }
                )
            }
        }
    }
    InitializeDeal(isGuest: boolean = true) {
        if (!isGuest) {
            if (this._HelperService._Account.EmailAddress == undefined || this._HelperService._Account.EmailAddress == null || this._HelperService._Account.EmailAddress == "") {
                this._HelperService.NotifyError("Your profile is not updated. Please update your profile to process payment");
            } else {
                this.buydeal_initizalize()
            }
        } else {
            this.buydeal_initizalize()
        }

    }

    buydeal_initizalize() {
        this._HelperService.IsProcessing = true;
        var pData = {
            Task: "buydeal_initizalize",
            ReferenceId: this._DataService._Deal.ReferenceId,
            ReferenceKey: this._DataService._Deal.ReferenceKey,
            PaymentSource: 'online',
            DeliveryCharge: this.PaymentConfiguration.DeliveryCharge, //this.PurchaseDetails.DeliveryCharge,
            DeliveryPartnerKey: null,
            TrackingId: this.deliveryPartnerInfo.TrackingId,
            TrackingKey: this.deliveryPartnerInfo.TrackingKey,
            FromAddressId: 0,
            FromAddressKey: null,
            ToAddressId: 0,
            ToAddressKey: null,
            CarrierId: null,
            DeliveryEta: 0,
            ItemCount: this.DealQuantity
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.PurchaseDetails = _Response.Result;
                    this._MixPanelService.track(this._HelperService.AppConfig.MinPanelEvents.Checkout, { Dealkey: this._DataService._Deal.ReferenceKey, DealName: this._DataService._Deal.Title });
                    this.PaymentDetails.PaystackAmount = this.PurchaseDetails.Amount + (this.selectedDeliveryPartner?.DeliveryCharge ? this.selectedDeliveryPartner.DeliveryCharge : 0);
                    this.PaymentDetails.WalletAmount = 0;
                    this.PaymentConfiguration.Amount = this.PurchaseDetails.Amount + (this.selectedDeliveryPartner?.DeliveryCharge ? this.selectedDeliveryPartner.DeliveryCharge : 0);
                    this.IsBalanceLoaded = true;
                    this._NgbModal.open(this.tucpayment, { size: 'lg', modalDialogClass: 'checkout', centered: true, backdrop: 'static', keyboard: false });
                }
                else {
                    this._HelperService.NotifyError(_Response.Message, 8);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    UseTucWalletClick() {
        if (this.IsWalletBalanceUse == true) {
            this.IsWalletBalanceUse = false;
            this.PaymentMode = 'paystack';
            this.PaymentDetails.WalletAmount = 0;
            if (this.selectedDeliveryPartner != undefined && this.selectedDeliveryPartner != null && this.selectedDeliveryPartner.DeliveryCharge != undefined && this.selectedDeliveryPartner.DeliveryCharge != null) {
                this.PaymentDetails.PaystackAmount = this.PurchaseDetails.Amount + this.selectedDeliveryPartner.DeliveryCharge;
                this.PaymentConfiguration.Amount = this.PurchaseDetails.Amount + this.selectedDeliveryPartner.DeliveryCharge;
            }
            else {
                this.PaymentDetails.PaystackAmount = this.PurchaseDetails.TotalAmount;
                this.PaymentConfiguration.Amount = this.PurchaseDetails.TotalAmount;
            }
        }
        else {
            this.IsWalletBalanceUse = true;
            if (this.PurchaseDetails.Balance >= this.PurchaseDetails.TotalAmount) {
                this.PaymentDetails.PaystackAmount = 0;
                if (this.selectedDeliveryPartner?.DeliveryCharge != undefined && this.selectedDeliveryPartner?.DeliveryCharge != null) {
                    this.PaymentDetails.WalletAmount = this.PurchaseDetails.Amount + this.selectedDeliveryPartner.DeliveryCharge;
                }
                else {
                    this.PaymentDetails.WalletAmount = this.PurchaseDetails.TotalAmount;
                }
                this.PaymentConfiguration.Amount = 0;
                this.PaymentMode = 'wallet';
            }
            else {
                this.PaymentDetails.PaystackAmount = Math.round(((this.PurchaseDetails.TotalAmount - this.PurchaseDetails.Balance) + Number.EPSILON) * 100) / 100;
                this.PaymentDetails.WalletAmount = Math.round(((this.PurchaseDetails.TotalAmount - this.PaymentDetails.PaystackAmount) + Number.EPSILON) * 100) / 100;
                if (this.selectedDeliveryPartner?.DeliveryCharge != undefined && this.selectedDeliveryPartner?.DeliveryCharge != null) {
                    this.PaymentConfiguration.Amount = this.PaymentDetails.PaystackAmount + this.selectedDeliveryPartner.DeliveryCharge;
                }
                else {
                    this.PaymentConfiguration.Amount = this.PaymentDetails.PaystackAmount
                }
            }
        }
    }

    ProcessPayment() {
        if (this.PaymentDetails.PaystackAmount == 0) {
            this.BuyDeal_Confirm(null, 0);
        }
        else {
            this.InitializeTransaction();
        }
    }
    async InitializeTransaction() {
        if (this.PaymentConfiguration.Amount == undefined || this.PaymentConfiguration.Amount == null || isNaN(this.PaymentConfiguration.Amount) == true) {
            this._HelperService.NotifyError("Please enter valid amount for purchase");
        }
        else if (this.PaymentConfiguration.Amount < 0) {
            this._HelperService.NotifyError("Amount must be greater than  0");
        }
        else {
            this._HelperService.IsProcessing = true;
            var pData = {
                Task: "buypointinitialize",
                Amount: Math.ceil(this.PaymentConfiguration.Amount),
                PaymentMode: this.paymentGateway,
                MerchantId: this._DataService._Deal.MerchantId,
                DealId: this._DataService._Deal.ReferenceId,
                DealKey: this._DataService._Deal.ReferenceKey
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this.PaymentConfiguration.Amount = _Response.Result.Amount;
                        this.PaymentConfiguration.Charge = _Response.Result.Charge;
                        this.PaymentConfiguration.TotalAmount = _Response.Result.TotalAmount;
                        this.PaymentConfiguration.PaymentMode = _Response.Result.PaymentMode;
                        this.PaymentConfiguration.EmailAddress = _Response.Result.EmailAddress;
                        this.PaymentConfiguration.PaymentReference = _Response.Result.PaymentReference;
                        this.PaymentConfiguration.Banks = _Response.Result.Banks;
                        this.PaymentConfiguration.UserCards = _Response.Result.Cards;
                        this.PaymentConfiguration.MobileNumber = _Response.Result.MobileNumber;
                        this.PaymentConfiguration.DisplayName = _Response.Result.DisplayName;
                        this.PaymentConfiguration.MerchantDisplayName = _Response.Result.MerchantDisplayName;
                        this.PaymentConfiguration.PaymentDescription = _Response.Result.PaymentDescription;
                        this.PaymentConfiguration.IconUrl = _Response.Result.IconUrl;
                        this._MixPanelService.track(this._HelperService.AppConfig.MinPanelEvents.BuyNow, { Dealkey: this._DataService._Deal.ReferenceKey, DealName: this._DataService._Deal.Title });
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.ActiveReference, _Response);
                        if (this.paymentGateway == "flutterwave") {
                            setTimeout(() => {
                                this._HelperService.IsProcessing = false;
                                this.makePayment(this.PaymentConfiguration);
                            }, 200);
                        }
                        else {
                            setTimeout(() => {
                                this._HelperService.IsProcessing = false;
                                document.getElementById('paymentbutton')?.click();
                            }, 200);
                        }
                    }
                    else {
                        this._HelperService.IsProcessing = false;
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    ProcessOnline_Cancel() {

    }
    CancelPayment() {
        // window.location.reload();
        this._NgbModal.dismissAll();
        this.IsWalletBalanceUse = false;
        this.paymentGateway = undefined;
        // this._PaymentStatus =
        // {
        //     Status: 'error',
        //     Balance: 0,
        // }
        // this.ModalDismiss(this._PaymentStatus);
    }
    ProcessOnline_Confirm(Item) {
        if(this.paymentGateway=='flutterwave'){
            this.flutterwave.closePaymentModal();
        }

        let PaymentApi = function (this,Item,pData) {
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        if (_Response.Result.StatusCode == 'transaction.success') {
                            this.BuyDeal_Confirm(this.PurchaseDetails.ReferenceKey, Item.transaction)
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }.bind(this);

        let pData: any = undefined;
        if (this.paymentGateway == 'flutterwave') {
            if (Item.status == "successful") {
                this._HelperService.IsProcessing = true;
                pData = {
                    Task: "buypointverify",
                    Amount: this.PaymentConfiguration.Amount,
                    PaymentReference: this.PaymentConfiguration.PaymentReference,
                    RefTransactionId: Item.transaction_id,
                    RefStatus: Item.status,
                    RefMessage: Item.message,
                    PaymentMode: this.paymentGateway,
                };
                PaymentApi(Item,pData);
            }
            else {
                this._HelperService.NotifyError('Payment process could not be completed. Please process transactiona again');
            }
        }
        else {
            if (Item.status == "success") {
                this._HelperService.IsProcessing = true;
                pData = {
                    Task: "buypointverify",
                    Amount: this.PaymentConfiguration.Amount,
                    PaymentReference: this.PaymentConfiguration.PaymentReference,
                    RefTransactionId: Item.transaction,
                    RefStatus: Item.status,
                    RefMessage: Item.message,
                    PaymentMode: this.paymentGateway,
                };
                PaymentApi(Item,pData);
            }
            else {
                this._HelperService.NotifyError('Payment process could not be completed. Please process transactiona again');
            }
        }

    }


    BuyDeal_Confirm(PaymentReference, TransactionId) {
        this._HelperService.IsProcessing = true;
        var pData = {
            Task: "buydeal_confirm",
            ReferenceId: this.PurchaseDetails.ReferenceId,
            ReferenceKey: this.PurchaseDetails.ReferenceKey,
            DealId: this._DataService._Deal.ReferenceId,
            DealKey: this._DataService._Deal.ReferenceKey,
            PaymentReference: PaymentReference,
            TransactionId: TransactionId,
            PaymentSource: 'wallet',
            DeliveryPartnerKey: this.deliveryPartnerInfo?.DeliveryPartnerKey,
            DeliveryEta: this.deliveryPartnerInfo.DeliveryEta,
            TrackingId: this.deliveryPartnerInfo.TrackingId,
            TrackingKey: this.deliveryPartnerInfo.TrackingKey,
            FromAddressId: 0,
            FromAddressKey: null,
            ToAddressId: 0,
            ToAddressKey: null,
            Amount: this.PurchaseDetails.Amount,
            Charge: this.PurchaseDetails.Fee,
            DeliveryCharge: this.PaymentConfiguration.DeliveryCharge,
            TotalAmount: this.PurchaseDetails.TotalAmount,
            CarrierId: this.deliveryPartnerInfo.CarrierId,
            ItemCount: this.DealQuantity,
            RateId: this.selectedDeliveryPartner?.RateId,
            RedisKey: this.selectedDeliveryPartner?.RedisKey,
            KwikKey: this.selectedDeliveryPartner?.KwikKey,
        };
        if (this.selectedDeliveryPartner) {
            pData.DeliveryPartnerKey = this.selectedDeliveryPartner.DeliveryPartnerKey;
            pData.RateId = this.selectedDeliveryPartner.RateId;
            pData.CarrierId = this.selectedDeliveryPartner.CarrierId;
            pData.DeliveryEta = this.selectedDeliveryPartner.DeliveryEta;
            pData.FromAddressId = this.deliveryPartnerInfo.FromAddressId;
            pData.FromAddressKey = this.deliveryPartnerInfo.FromAddressKey;
            pData.ToAddressId = this.deliveryPartnerInfo.ToAddressId;
            pData.ToAddressKey = this.deliveryPartnerInfo.ToAddressKey;
            pData.RedisKey = this.selectedDeliveryPartner?.RedisKey;
            pData.KwikKey = this.selectedDeliveryPartner?.KwikKey;
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsProcessing = true;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.DealCode, _Response.Result);
                    this._MixPanelService.track(this._HelperService.AppConfig.MinPanelEvents.Payment, { Dealkey: this._DataService._Deal.ReferenceKey, DealName: this._DataService._Deal.Title });
                    this._HelperService.NotifySuccess(_Response.Message);
                    // this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageItem.GuestUser);
                    this._HelperService.SaveStorage('paymentsuccess', { "paymentsuccess": true, address: this._DataService._Deal.Address });
                    // window.location.href = `/dealcode/${_Response.Result.ReferenceId}/${_Response.Result.ReferenceKey}`;
                    this._NgbModal.dismissAll();
                    // this._router.navigateByUrl(`/dealcode/${ _Response.Result.ReferenceId}/${_Response.Result.ReferenceKey}`);
                    this._DataService.DealId = _Response.Result.ReferenceId;
                    this._DataService.DealCode = _Response.Result.ReferenceKey;
                    this._DataService._GetBalannce();
                    this._router.navigate([`/buydeal/${_Response.Result.ReferenceKey}/${_Response.Result.ReferenceId}/paymentsuccessful`]);
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    //#endregion NEW CODE - END

    changeDeliveryAddress(isGuest?, event?) {
        if (isGuest) {
            this.selectedAddress = event;
        } else {
            this.selectedAddress = this.shippingAddresses.find(el => el.ReferenceKey == this.SelectedDeliveryAddress);
        }
        this.shippingAddressChangeMode = true;
        this.progress = 0;
        let complete: boolean = false;
        let progressInterval = setInterval(function () { ProgressBar() }, 2000);

        let self = this;
        function ProgressBar() {
            self.progress = self.progress + Math.round(Math.random() * Math.round(Math.random() * 20));
            if (complete) {
                self.progress = 100;
                setTimeout(() => {
                    self.deliveryPartnerMode = true;
                }, 1000);
                clearInterval(progressInterval);
            }
            else if (self.progress >= 60) {
                self.progress = 92;
            }
        }

        var pData = {
            Task: "getdeliverypricing",
            MetaData: {},
            AddressId: this.selectedAddress.ReferenceId,
            DealId: this._DataService._Deal.ReferenceId,
            DealKey: this._DataService._Deal.ReferenceKey,
            Data: "",
            ItemCount: this.DealQuantity
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                complete = true;
                this._HelperService.IsProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    
                    this.deliveryPartner = _Response.Result.DeliveryPartners;
                    this.deliveryPartnerInfo = _Response.Result;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message, 5);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
                if (_Error.status == 401) {
                    complete = true;
                    // mocking
                    // this.deliveryPartner = [
                    //     {
                    //         "DeliveryCharge": 14104.76,
                    //         "breakdown": [],
                    //         "carrier_logo": "https://ucarecdn.com/dcdd8109-af8c-4057-8104-192be821dd6e/download4.png",
                    //         "Name": "DHL Express",
                    //         "carrier_rate_description": "EXPRESS DOMESTIC",
                    //         "carrier_reference": "CA-81957188177",
                    //         "carrier_slug": "dhl-ng",
                    //         "currency": "NGN",
                    //         "delivery_address": "AD-94320920622",
                    //         "delivery_eta": 1812,
                    //         "delivery_date": "2022-07-29T23:59:00Z",
                    //         "delivery_time": "within 2 days",
                    //         "id": "rate_fBryTnjao6GYLyQM",
                    //         "insurance_coverage": 0,
                    //         "insurance_fee": 0,
                    //         "includes_insurance": false,
                    //         "metadata": {
                    //             "localProductCode": "N",
                    //             "productCode": "N"
                    //         },
                    //         "parcel": "PC-53016039424",
                    //         "pickup_address": "AD-38960253080",
                    //         "pickup_eta": 1440,
                    //         "DeliveryTime": "Within 24 hours",
                    //         "rate_id": "RT-33038674442",
                    //         "used": false,
                    //         "user": "USER-25066556633",
                    //         "ReferenceId":"1",
                    //         "ReferenceKey":"b01e4a97cd814da1ab448966b5718144",
                    //         "IconUrl":"https://shipmondo.com/assets/logos/carrier/round/dhl-053538c61b7829a4536a288f204e09430431c65093eea0388abe7d92e15cbf25.png"
                    //     },
                    //     {
                    //         "DeliveryCharge": 17604.76,
                    //         "breakdown": [],
                    //         "carrier_logo": "https://ucarecdn.com/dcdd8109-af8c-4057-8104-192be821dd6e/download4.png",
                    //         "Name": "DHL Express + (Parcel Insurance)",
                    //         "carrier_rate_description": "EXPRESS DOMESTIC",
                    //         "carrier_reference": "CA-81957188177",
                    //         "carrier_slug": "dhl-ng",
                    //         "currency": "NGN",
                    //         "delivery_address": "AD-94320920622",
                    //         "delivery_eta": 1812,
                    //         "delivery_date": "2022-07-29T23:59:00Z",
                    //         "delivery_time": "Within 2 days",
                    //         "id": "rate_VezBKSAXlphax927",
                    //         "insurance_coverage": 0,
                    //         "insurance_fee": 3500,
                    //         "includes_insurance": true,
                    //         "metadata": {
                    //             "localProductCode": "N",
                    //             "productCode": "N"
                    //         },
                    //         "parcel": "PC-53016039424",
                    //         "pickup_address": "AD-38960253080",
                    //         "pickup_eta": 1440,
                    //         "DeliveryTime": "Within 24 hours",
                    //         "rate_id": "RT-63373378463",
                    //         "used": false,
                    //         "user": "USER-25066556633",
                    //         "ReferenceId":"2",
                    //         "ReferenceKey":"b01e4a97cd814da1ab448966b5718145",
                    //         "IconUrl":"https://shipmondo.com/assets/logos/carrier/round/dhl-053538c61b7829a4536a288f204e09430431c65093eea0388abe7d92e15cbf25.png"
                    //     }
                    // ]
                }
            });
    }
    openShippingAddressModal(content, action?) {
        this._NgbModal.open(content, { size: 'md', modalDialogClass: 'tuc-shipping-address-modal' });
        this.editForm = action;
    }

    changeDeliveryType(event) {
        this.DeliveryType = event;
        if (event == 'Delivery') {
            this.finalCheckout = false;
            this._HelperService.IsActiveUser && this.fetchAddress(true);
        } else {
            this.resetDeliveryMode();
            this.finalCheckout = true;
        }
    }

    fetchAddress(event) {
        if (event == true) {
            this.shippingAddresses = [];
            this.fetchAddressIsProcessing = true;
            this._HelperService.IsProcessing = true;
            var pData = {
                Task: "getaddresslist",
                "TotalRecords": 0,
                "Offset": 0,
                "Limit": 100,
                "RefreshCount": true,
                "SearchCondition": "",
                "SortExpression": "CreateDate desc"
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
            _OResponse.subscribe(
                _Response => {
                    this.fetchAddressIsProcessing = false;
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this.shippingAddresses = _Response.Result.Data;
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                    if (_Error.status == 401) {
                        this.shippingAddresses = [];
                    }
                });
        }
    }
    resetDeliveryMode() {
        this.shippingAddressChangeMode = false;
        this.deliveryPartnerMode = false;
        this.deliveryPartnerInfo = {};
        this.finalCheckout = false;
        this.SelectedDeliveryAddress = "";
        this.selectedDeliveryPartner = null;
        this.PaymentConfiguration.DeliveryCharge = 0;
    }
    changeDeliveryPartner(partner) {
        this.selectedDeliveryPartner = partner;
        // call rate api with partner.rate_id
        this._DataService._Deal.Fee = partner.DeliveryCharge;
        // finally checkout button enable;
        if (partner.DeliveryCharge != undefined && partner.DeliveryCharge != null) {
            this.PaymentConfiguration.DeliveryCharge = parseFloat(parseFloat(partner.DeliveryCharge).toFixed(2));
            this.PaymentConfiguration.TotalAmount = parseFloat(this.PaymentConfiguration.TotalAmount + parseFloat(partner.DeliveryCharge).toFixed(2));
        }
        this.finalCheckout = true;
    }

    WishlistDeal() {
        var _LoginCheck = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Acc);
        if (!_LoginCheck) {
            this._HelperService.NotifyError(`Please <a href="/auth/login" style="color: #fff; text-decoration: underline;"><u>login</u></a> to wishlist deal`, 5);
        }
        else {
            var pData = {
                Task: 'savebookmark',
                DealKey: this._DataService._Deal.ReferenceKey,
            }
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
            _OResponse.subscribe(

                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._DataService._Deal.IsBookmarked = _Response.Result.IsBookmarked;
                        // this._HelperService.NotifySuccess(_Response.Message);
                        // if(_Response.Result.)
                        // this._ManagerDetails = _Response.Result;
                        this._MixPanelService.track(this._HelperService.AppConfig.MinPanelEvents.Wishlist,
                            { DealKey: this._DataService._Deal.ReferenceKey, DealName: this._DataService._Deal.Title })
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                }
            );
        }
    }

    makePayment(PaymentConfiguration: any) {
        let paymentData = {
            public_key: this._HelperService.AppConfig.FlutterwaveKey,
            tx_ref: PaymentConfiguration.PaymentReference,
            amount: PaymentConfiguration.Amount,
            currency: this._DataService._CountrySelcted.CurrencyNotation,
            payment_options: "card, banktransfer, ussd",
            redirect_url: "",
            callback: this.ProcessOnline_Confirm,
            onclose: this.closedPaymentModal,
            callbackContext: this,
            customer: {
                email: PaymentConfiguration.EmailAddress,
                phone_number: PaymentConfiguration.MobileNumber,
                name: PaymentConfiguration.DisplayName,
            },
            customizations: {
                title: PaymentConfiguration.MerchantDisplayName,
                description: PaymentConfiguration.PaymentDescription,
                logo: PaymentConfiguration.IconUrl,
            },
        }
        this.flutterwave.inlinePay(paymentData);
    }


    // for flutterwave call back method by default
    makePaymentCallback(response: PaymentSuccessResponse): void {
        console.log("paymentcallback", response);
        this.flutterwave.closePaymentModal(5)
    }
    paymentmod: any;
    closedPaymentModal(): void {
        console.log(this.paymentmod, 'payment is closed');
    }

    paymentGateway: any;
    toggle(event: any) {
        this.paymentGateway = event.target.value;
        console.log(this.paymentGateway, "paymentGateway of eval")
    }

}


export interface IPaymentDetails {
    Amount: any;
    Fees: number;
    WalletBalance: number;
    TotalAmount: number;
    PayableAmount: number;
}