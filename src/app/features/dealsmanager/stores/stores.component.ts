import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { ICategory } from 'src/app/service/interface.service';
@Component({
    selector: 'tuc-stores',
    templateUrl: './stores.component.html',
})
export class TUCstoresComponent implements OnInit {
    sum = 100;
    throttle = 300;
    scrollDistance = 1;
    scrollUpDistance = 2;
    direction = "";
    constructor(
        public _HelperService: HelperService,
        public _DataService: DataService,
        public _activatedRoute: ActivatedRoute
    ) {
    }

    categeoryName: any;
    ngOnInit() {
        // this._activatedRoute.paramMap.subscribe((x) => {
        //     let id = x.get("ReferenceId");
        //      this._CategorySelected(id);
        //   });

        this._activatedRoute.params.subscribe((params: Params) => {
            var CategeoryKey = params['referencekey'];

            this._DataService._GetDealsByMerchant(params['referencekey']);
            // var UserName = params['username'];
            // if (AccountKey != undefined && UserName != undefined) {
            //     this.ProcessAccount(UserName, AccountKey);
            // }
        });
    }

    onScrollDown() {
        this._DataService._GetDeals();
    }
    onUp() {

    }

    _SearchDeal() {
        this._DataService._GetDealsReset();
        this._DataService._GetDeals();
    }

    public CategorySearchContent = "";
    _SearchCategory() {
        if (this.CategorySearchContent != undefined && this.CategorySearchContent != null && this.CategorySearchContent != "") {
            this._DataService._CategoriesSorted = this._DataService._Categories.filter(x => x.Name.toLowerCase() == this.CategorySearchContent || x.Name.toLowerCase().startsWith(this.CategorySearchContent));
        }
        else {
            this._DataService._CategoriesSorted = this._DataService._Categories;
        }

    }

    _CategorySelected(Item) {
        var SCat = this._DataService._CategoriesSorted.find(x => x.ReferenceKey == Item.ReferenceKey);
        if (SCat != undefined) {
            if (SCat.IsChecked) {
                SCat.IsChecked = false;
            }
            else {
                SCat.IsChecked = true;
            }
        }


        var Items = this._DataService._CategoriesSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
        if (Items.length > 0) {
            var ItemIndex = this._DataService._CategoriesSelected.indexOf(Items[0]);
            this._DataService._CategoriesSelected.splice(ItemIndex, 1);
        }
        else {
            this._DataService._CategoriesSelected.push(Item);
        }
        this._DataService._GetDealsReset();
        this._DataService._DealsCategeory = []
        this._DataService._GetDealsByCategeory(Item.ReferenceKey);
    }


    public MerchantSearchContent = "";
    _SearchMerchant() {
        if (this.MerchantSearchContent != undefined && this.MerchantSearchContent != null && this.MerchantSearchContent != "") {
            this._DataService._MerchantsSorted = this._DataService._Merchants.filter(x => x.Name.toLowerCase() == this.MerchantSearchContent || x.Name.toLowerCase().startsWith(this.MerchantSearchContent));
        }
        else {
            this._DataService._MerchantsSorted = this._DataService._Merchants;
        }
    }

    _MerchantSelected(Item) {
        var SCat = this._DataService._MerchantsSorted.find(x => x.ReferenceKey == Item.ReferenceKey);
        if (SCat != undefined) {
            if (SCat.IsChecked) {
                SCat.IsChecked = false;
            }
            else {
                SCat.IsChecked = true;
            }
        }

        var Items = this._DataService._MerchantSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
        if (Items.length > 0) {
            var ItemIndex = this._DataService._MerchantSelected.indexOf(Items[0]);
            this._DataService._MerchantSelected.splice(ItemIndex, 1);
        }
        else {
            this._DataService._MerchantSelected.push(Item);
        }
        this._DataService._GetDealsReset();
        this._DataService._GetDeals();
    }

    _SortBy(Item) {
        this._DataService.DealsSortOrder = Item;
        this._DataService._GetDealsReset();
        this._DataService._GetDeals();
    }
}