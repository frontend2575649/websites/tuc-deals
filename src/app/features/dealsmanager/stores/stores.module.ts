import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TUCstoresComponent } from './stores.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CountdownModule } from 'ngx-countdown';

const routes: Routes = [
    { path: '', component: TUCstoresComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUCstoresRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TUCstoresRoutingModule,
        InfiniteScrollModule,
        CountdownModule
    ],
    declarations: [TUCstoresComponent]
})
export class TUCstoresModule {
}
