import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import * as moment from 'moment'
import { Observable } from 'rxjs';
import { IDealReview, OResponse } from 'src/app/service/interface.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StarRatingComponent } from 'ng-starrating';
import { MixpanelService } from 'src/app/service/mixpanel.service';

@Component({
    selector: 'tuc-deal',
    templateUrl: './deal.component.html',
    styleUrls:['./deal.component.css']
})
export class TUCDealComponent implements OnInit {
    sum = 100;
    throttle = 300;
    scrollDistance = 1;
    scrollUpDistance = 2;
    QuantityCount:number = 1;
    @ViewChild('tuclogin') loginModal: any;
    public activeTab = "details";
    tabChange(activeTab) {
        this.activeTab = activeTab;
    }
    _Cdata =
        {
            Comment: "",
        }

    zoom = 12;
    center: google.maps.LatLngLiteral = {
        lat: 6.5557907,
        lng: 3.3856198
    };
    options: google.maps.MapOptions = {
        mapTypeId: 'roadmap',
        zoomControl: true,
        scrollwheel: false,
        disableDoubleClickZoom: true,
        maxZoom: 19,
        minZoom: 1,
    };
    @ViewChild('dealCarousel', { static: true }) dealCarousel: NgbCarousel;
    public _OwlDeal: OwlOptions = {
        loop: false,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        autoHeight: false,
        dots: false,
        navSpeed: 700,
        autoWidth: true,
        margin: 8,
        navText: ['<i class="icofont-thin-left"></i>', '<i class="icofont-thin-right"></i>'],
        responsive: {
            0: {
                items: 6
            },
            400: {
                items: 6
            },
            740: {
                items: 6
            },
            940: {
                items: 6
            }
        },
        nav: false
    }

    _DealRatingNumber: number | null = 0;
    isDragging: boolean = false;
    public form: FormGroup;
    constructor(private _ActivatedRoute: ActivatedRoute, private fb: FormBuilder, public _DataService: DataService, public _HelperService: HelperService, private _NgbModal: NgbModal, private router: Router, private _MixpanelService: MixpanelService) {
        this._DealRatingNumber = 0;
        this.form = this.fb.group({
            // rating1: ['', Validators.required],
            //  rating2: [4]
        });
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this._DataService._GetDeals();
    }
    ImageSelected(Image, index) {
        this._DataService._Deal.ImageUrl = Image;
        this.dealCarousel.select(`carouselSlide${index}`);
    }
    OpenLogin() {
        document.getElementById("btntuclogin")?.click();
    }
    ngOnInit() {
        this._DataService.DealKey = this._ActivatedRoute.snapshot.params['referencekey'];
        this._GetDealReviews();
        this._DataService._GetDeal(true);
        navigator.geolocation.getCurrentPosition((position) => {
            this.center = {
                lat: position.coords.latitude,
                lng: position.coords.longitude,
            }
        })
        this.TimerCode();
    }
    on_carouselDrag(dragging: boolean) {
        setTimeout(() => {
            this.isDragging = dragging;
        }, 100)
    }
    navigateToRoute(route, TItem) {
        if (!this.isDragging && !TItem.IsSoldout) { this.router.navigate(route); }
    }

    timeRemaining = "--";
    isTimeLessThanDay: boolean = false;
    TimerCode() {
        var difference = moment(this._DataService.DealEndDate).diff(moment(), 'hours');
        if (difference < 0) {
            this.timeRemaining = "Deal No Longer Available";
            this.isTimeLessThanDay = false;
        } else if (difference >= 0 && difference <= 24) {
            this.isTimeLessThanDay = true;
            this._DataService._Deal.CountdownConfig = { leftTime: moment(this._DataService.DealEndDate).diff(moment(), 'seconds') };;
        } else if (difference > 24 && difference >= 48) {
            this.timeRemaining = Math.round(difference / 24) + " Day Remaining";
            this.isTimeLessThanDay = false;
        } else {
            this.timeRemaining = Math.round(difference / 24) + " Days Remaining";
            this.isTimeLessThanDay = false;
        }
    }


    WishlistDeal() {
        var _LoginCheck = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Acc);
        if (_LoginCheck == null) {
            this._HelperService.NotifyError("Please login to wishlist deal");
        }
        else {
            var pData = {
                Task: 'savebookmark',
                DealKey: this._DataService.DealKey,
            }
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
            _OResponse.subscribe(

                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._DataService._Deal.IsBookmarked = _Response.Result.IsBookmarked;
                        // this._HelperService.NotifySuccess(_Response.Message);
                        // if(_Response.Result.)
                        // this._ManagerDetails = _Response.Result;
                        this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.Wishlist, 
                            { DealKey: this._DataService._Deal.ReferenceKey, DealName: this._DataService._Deal.Title })
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                }
            );
        }

    }


    // OpenWishlist(content) {
    //     this._NgbModal.open(content, { size: 'lg', modalDialogClass: 'tuc-pp-wishlist' });
    // }

    _LoadDeal(Item) {
        this._NgbModal.dismissAll();
        window.location.href = "deal/" + Item.ReferenceKey;
    }

    // Rate_Review(content) {
    //     this._NgbModal.open(content, { size: 'sm', centered: true });

    // }

    // ViewRate_Review(content) {
    //     this._NgbModal.open(content, { size: 'lg', modalDialogClass: 'tuc-pp-viewrateandreviwe' });
    // }

    //rating code--
    RatingClick(content) {
        const modal =this._NgbModal.open(content, { size: 'sm', centered: true });
        modal.result.then(() => { console.log('When user closes'); }, (err) => { 
            this._DealRatingNumber = null;
            // console.log('Backdrop click', this._DealRatingNumber, err);
        })
    }
    SaveRateing(Rate) {
        this._HelperService.IsProcessing = true;
        var pData = {
            Task: 'savedealreview',
            Host: this._HelperService.AppConfig.HostType,
            DealId: Rate.ReferenceId,
            DealKey: Rate.ReferenceKey,
            Rating: this._DealRatingNumber,
            Review: this._Cdata.Comment,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Rating, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                    this._Cdata.Comment = "";
                    this._DealRatingNumber = 0;
                    this._NgbModal.dismissAll();
                    this._DealsReviews = { Offset: 0,
                        Limit: 10,
                        TotalRecords: 0,
                        List: []
                    }
                    this._GetDealReviews();
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });



    }


    // onRate($event: { oldValue: number, newValue: number, starRating: StarRatingComponent }) {
    //     alert(`Old Value:${$event.oldValue}, 
    //       New Value: ${$event.newValue}, 
    //       Checked Color: ${$event.starRating.checkedcolor}, 
    //       Unchecked Color: ${$event.starRating.uncheckedcolor}`);
    // }

    public _DealsReviews: IDealReview =
        {
            Offset: 0,
            Limit: 10,
            TotalRecords: 0,
            List: []
        }
    // TotalRating: any;
    // Totalreview: any;
    _GetDealReviews() {
        var pData = {
            Task: 'getdealreviews',
            Host: this._HelperService.AppConfig.HostType,
            ReferenceId: this._DataService._Deal.ReferenceId,
            ReferenceKey: this._DataService._Deal.ReferenceId,
            Offset: this._DealsReviews.Offset,
            Limit: this._DealsReviews.Limit
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._DealsReviews.Offset = this._DealsReviews.Offset + this._DealsReviews.Limit;
                    _Response.Result.Data.forEach(element => {
                        // element.CreateDate  = this._HelperService
                        element.RatingContent = '';
                        for (let index = 0; index < element.Rating; index++) {
                            element.RatingContent += '<i class="icofont-ui-rating active"></i>';
                        }
                        this._DealsReviews.List.push(element);
                    });
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    _GetDealReviews_onScrollDown() {
        this._GetDealReviews();
    }
    decQuantityCount(){
        if(this._DataService._Deal.Quantity > 1){
            this._DataService._Deal.Quantity = this._DataService._Deal.Quantity - 1;
        }
    }
    incQuantityCount(){
        this._DataService._Deal.Quantity = this._DataService._Deal.Quantity + 1;
    }
}