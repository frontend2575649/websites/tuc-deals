import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TUCcategeoryComponent } from './categeory.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CountdownModule } from 'ngx-countdown';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxSliderModule } from '@angular-slider/ngx-slider';

const routes: Routes = [
    { path: '', component: TUCcategeoryComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUCcategeoryRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TUCcategeoryRoutingModule,
        InfiniteScrollModule,
        CountdownModule,
        SharedModule,
        NgxSkeletonLoaderModule.forRoot({theme: {'margin-bottom': '0'}}),
        NgxSliderModule
    ],
    declarations: [TUCcategeoryComponent]
})
export class TUCcategeoryModule {
}
