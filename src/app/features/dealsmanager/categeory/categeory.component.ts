import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { ICategory } from 'src/app/service/interface.service';
@Component({
    selector: 'tuc-categeory',
    templateUrl: './categeory.component.html',
    styleUrls:['./category.component.css']
})
export class TUCcategeoryComponent implements OnInit, OnDestroy {
    sum = 100;
    throttle = 300;
    scrollDistance = 1;
    scrollUpDistance = 2;
    direction = "";
    noCategoryFound: boolean = false;
    firstLoad: boolean = false;
    minValue: number = 0;
    maxValue: number = 10000000;
    options: any = {
      floor: 0,
      ceil: 10000000,
      hideLimitLabels:true
    };
    no_result_found:boolean=false;
    flashCountDown:number = 0;
    constructor(
        public _HelperService: HelperService,
        public _DataService: DataService,
        public _activatedRoute: ActivatedRoute,
        public _Router: Router,
    ) {
        this.flashCountDown =  this._HelperService.getTimeDifference()
    }
    CategoryKey = "";
    SubCategoryKey = "";
    categeoryName: any;
    ngOnInit() {
        this.resetCategoryChecked();
        this._activatedRoute.params.subscribe((params: Params) => {
            this._DataService._GetDealsReset();
            this.CategoryKey = params['name'];
            this.SubCategoryKey = params['subcategory'];
            let isNumeric = Number(this.CategoryKey);
            setTimeout(() => {     
                if (isNaN(isNumeric)) {
                    // search by Name
                    this._CategorySelected(this._DataService._CategoriesSorted.find(x => x.Name?.toLowerCase().split("-").join("").split(" ").join("").indexOf(this.CategoryKey?.toLowerCase()) > -1), { target: { checked: true } });
                } else {
                    // search by ReferenceKey
                    this._CategorySelected(this._DataService._CategoriesSorted.find(x => x.ReferenceKey == this.CategoryKey), { target: { checked: true } });
                }
            }, 800);
        });
        this.firstLoad = true;
    }

    ngOnDestroy(){
        this.firstLoad = false;
    }
    navigateToRoute(route, TItem, event) {
        event.stopPropagation();
        if (!TItem.IsSoldout) { this._Router.navigate(route); }
    }

    onScrollDown() {
        if(this.firstLoad == true){
            this._DataService._GetDeals();
        }
    }
    onUp() {

    }

    _SearchDeal() {
        this._DataService._GetDealsReset();
        this._DataService._GetDeals();
    }

    public CategorySearchContent = "";
    _SearchCategory() {
        if (this.CategorySearchContent != undefined && this.CategorySearchContent != null && this.CategorySearchContent != "") {
            this._DataService._CategoriesSorted = this._DataService._Categories.filter(x => x.Name.toLowerCase() == this.CategorySearchContent || x.Name.toLowerCase().startsWith(this.CategorySearchContent));
        }
        else {
            this._DataService._CategoriesSorted = this._DataService._Categories;
        }

    }

    _CategorySelected(Item, event:any = null) {
        this.resetCategoryChecked();
        if (!Item) {
            this.noCategoryFound = true;
        } else {
            this.noCategoryFound = false;
            var SCat = this._DataService._CategoriesSorted.find(x => x.ReferenceKey == Item.ReferenceKey);
            if (SCat != undefined) {
                if (!event.target.checked) {
                    SCat.IsChecked = false;
                    this.SubCategoryKey = '';
                    var Items :any = this._DataService._CategoriesSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                    if (Items.length > 0) {
                        Item?.SubCategories?.forEach(ele => {
                            this._SubCategorySelected(ele, true);    
                        });
                        var ItemIndex = this._DataService._CategoriesSelected.indexOf(Items[0]);
                        this._DataService._CategoriesSelected.splice(ItemIndex, 1);
                    }
                }
                else {
                    var Items :any= this._DataService._CategoriesSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                    if (Items.length == 0) {
                        this._DataService._CategoriesSelected.push(Item);
                    }
                    SCat.IsChecked = true;
                }
            }
        }
        this._ManageMerchantList('_CategoriesSelected', 'CategoryDistribution');
        // subcat manipulation
        this._DataService._SubCategoriesSorted = [];
        this._DataService._CategoriesSelected.forEach(element => {
            if (element.SubCategories?.length) {
                element.SubCategories.forEach(subEl => subEl.IsChecked = subEl.IsChecked ? true : false);
                this._DataService._SubCategoriesSorted.push(...element.SubCategories)
            }
        });
        if(this.SubCategoryKey){
            this._SubCategorySelected(this._DataService._SubCategoriesSorted.find(x => x.Name?.toLowerCase().split("-").join("").split(" ").join("").indexOf(this.SubCategoryKey?.toLowerCase()) > -1));
        }else{
            this._DataService._GetDeals();
            if(this._DataService._Deals.length === 0){
                this.no_result_found = true;
            }else{
                this.no_result_found = false;
            }
            this._DataService._GetDealsReset();
        }
    }

    _SubCategorySelected(Item, makeFalse?: boolean) {
        this._DataService._MerchantsSorted = [];
        this._DataService._MerchantSelected = [];
        var SubCat = this._DataService._SubCategoriesSorted.find(x => x.ReferenceKey == Item.ReferenceKey);
        if (SubCat != undefined) {
            if (!makeFalse) {
                if (SubCat.IsChecked) {
                    SubCat.IsChecked = false;
                    var Items = this._DataService._SubCategoriesSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                    if (Items.length > 0) {
                        var ItemIndex = this._DataService._SubCategoriesSelected.indexOf(Items[0]);
                        this._DataService._SubCategoriesSelected.splice(ItemIndex, 1);
                    }
                }
                else {
                    var Items = this._DataService._SubCategoriesSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                    if (Items.length == 0) {
                        this._DataService._SubCategoriesSelected.push(Item);
                    }
                    SubCat.IsChecked = true;
                }
                this._DataService._GetDealsReset();
                this._DataService._GetDeals();
            } else {
                SubCat.IsChecked = false;
                var Items = this._DataService._SubCategoriesSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                if (Items.length > 0) {
                    var ItemIndex = this._DataService._SubCategoriesSelected.indexOf(Items[0]);
                    this._DataService._SubCategoriesSelected.splice(ItemIndex, 1);
                }
            }
            this._ManageMerchantList('_SubCategoriesSelected', 'SubCategoryDistribution');
        }
    }

    _ManageMerchantList(control:string, distribution:string){
        this._DataService._MerchantsSorted = [];
        this._DataService._MerchantSelected = [];
        let selectedCategoryIds = this._DataService[control].map((ele: any) => ele.ReferenceKey);
        if(selectedCategoryIds.length > 0){
            this._DataService._OriginalMerchants.forEach((element:any) => {
                element.TotalDeals = 0;
                element[distribution].forEach(cat => {
                    selectedCategoryIds.forEach(ids => {
                        if(ids == cat.CategoryKey){
                            element.TotalDeals = element?.TotalDeals + cat.Count;
                            element.IsChecked = false;
                            if(this._DataService._MerchantsSorted.indexOf(element) == -1){
                                this._DataService._MerchantsSorted.push(element);
                            }
                        }
                        this._DataService._Merchants = this._DataService._MerchantsSorted;
                    })
                })
            })
        }else{
            this._DataService._MerchantsSorted = this._DataService._OriginalMerchants;
            this._DataService._Merchants = this._DataService._OriginalMerchants;
        }
    }

    public MerchantSearchContent = "";
    _SearchMerchant() {
        if (this.MerchantSearchContent != undefined && this.MerchantSearchContent != null && this.MerchantSearchContent != "") {
            this._DataService._MerchantsSorted = this._DataService._Merchants.filter(x => x.Name.toLowerCase() == this.MerchantSearchContent || x.Name.toLowerCase().startsWith(this.MerchantSearchContent));
        }
        else {
            this._DataService._MerchantsSorted = this._DataService._Merchants;
        }
    }

    _MerchantSelected(Item) {
        var SCat = this._DataService._MerchantsSorted.find(x => x.ReferenceKey == Item.ReferenceKey);
        if (SCat != undefined) {
            if (SCat.IsChecked) {
                SCat.IsChecked = false;

                var Items = this._DataService._MerchantSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                if (Items.length > 0) {
                    var ItemIndex = this._DataService._MerchantSelected.indexOf(Items[0]);
                    this._DataService._MerchantSelected.splice(ItemIndex, 1);
                }
            }
            else {
                var Items = this._DataService._MerchantSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                if (Items.length == 0) {
                    this._DataService._MerchantSelected.push(Item);
                }
                SCat.IsChecked = true;
            }
        }
        this._DataService._GetDealsReset();
        this._DataService._GetDeals();
    }

    _SortBy(Item) {
        this._DataService.DealsSortOrder = Item;
        this._DataService._GetDealsReset();
        this._DataService._GetDeals();
    }
    resetCategoryChecked() {
        this._DataService._GetDealsReset();
        this._DataService._CategoriesSelected = [];
        this._DataService._SubCategoriesSelected = [];
        this._DataService?._CategoriesSorted.forEach(ele => delete ele.IsChecked);
        this._DataService?._SubCategoriesSorted.forEach(ele => delete ele.IsChecked);
        this._DataService._MerchantsSorted.forEach(ele => delete ele.IsChecked);
        this._DataService._MerchantSelected = [];
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Categories, this._DataService._CategoriesSorted);
    }
    _Pricing =
    {
        MinPrice: 0,
        MaxPrice: 0,
    };
    _Pricings = [
        {
            Title: '0 - 1000',
            MinPrice: 0,
            MaxPrice: 1000
        },
        {
            Title: '1000 - 5000',
            MinPrice: 1000,
            MaxPrice: 5000
        },
        {
            Title: '5000 - 10000',
            MinPrice: 5000,
            MaxPrice: 10000
        },
        {
            Title: '10000 - 20000',
            MinPrice: 10000,
            MaxPrice: 20000
        },
        {
            Title: '20000 - 50000',
            MinPrice: 20000,
            MaxPrice: 50000
        },
        {
            Title: '50000 + ',
            MinPrice: 50000,
            MaxPrice: 0
        }

    ]

    _SearchByPriceClear() {
        this._DataService._GetDealsReset();
        this._Pricing =
        {
            MinPrice: 0,
            MaxPrice: 0,
        };
        this._DataService.MinPrice = 0;
        this._DataService.MaxPrice = 0;
        this._DataService._GetDeals();
    }
    ResetFilters()
    {
        this._DataService._GetDealsReset();
        this._SearchByPriceClear();
    }
    userChange(data)
 {
   this.minValue = data.value
   this.maxValue = data.highValue
 }

 _SearchByPrice() {
    this._DataService._GetDealsReset();
    this._DataService.MinPrice = this.minValue;
    this._DataService.MaxPrice = this.maxValue;
    this._DataService._GetDeals();
}
 _PricingClick(Item) {
    this._DataService._GetDealsReset();
    this._Pricing.MinPrice = Item.MinPrice;
    this._Pricing.MaxPrice = Item.MaxPrice;
    this._DataService.MinPrice = Item.MinPrice;
    this._DataService.MaxPrice = Item.MaxPrice;
    this._DataService._GetDeals();
}

}