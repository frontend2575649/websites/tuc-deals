import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { ICategory } from 'src/app/service/interface.service';
import { Options, LabelType } from "@angular-slider/ngx-slider";
@Component({
    selector: 'tuc-deals',
    templateUrl: './deals.component.html',
    styleUrls:['./deals.component.css']
})
export class TUCDealsComponent implements OnInit {
    sum = 100;
    throttle = 300;
    scrollDistance = 1;
    scrollUpDistance = 2;
    direction = "";
    searchByCategory = "";
    searchByQuery = "";
    _DealsSearch: any = [];
    minValue: number = 0;
    maxValue: number = 10000000;
    no_result_found = false;
    options: Options = {
        floor: 0,
        ceil: 10000000,
        hideLimitLabels:true
    };
    flashCountDown:number = 0;
    _SortByFilterVisible: boolean = true;
    _fn: string;
    constructor(
        public _HelperService: HelperService,
        public _DataService: DataService,
        public _Router: Router,
        private _activatedRoute: ActivatedRoute
    ) {
       this.flashCountDown =  this._HelperService.getTimeDifference()
    }
    ngOnInit() {
        this._HelperService.DeleteStorage('paymentsuccess');
        this.resetCategoryChecked();
        // this._DataService._GetDealsReset();
        // this._DataService._GetDeals();
        this._activatedRoute.queryParams.subscribe((params:any) => {
            this._DataService._GetDealsReset();
            this.searchByCategory = params.category;
            this.searchByQuery = params.search;
            if(params.store){
                var SCat = this._DataService._MerchantsSorted.find(x => x.Name == params.store);
                this._MerchantSelected(SCat);
            }else{
                this.getDealsByQueryParam(); // instead of calling _GetDeals api, now calling dynamic api based on params
            }
            this._DataService.$isCityChangedScource.subscribe(data=> {if(data) this.getDealsByQueryParam();});
        })
    }
    getDealsByQueryParam(){
        if(this.searchByCategory == 'top-deals'){
            this._DataService._GetDeals_Promo_TopDeals();
            this._fn = '_GetDeals_Promo_TopDeals';
            this._SortByFilterVisible = false;
        }else if(this.searchByCategory == 'flash-deals'){
            this._DataService._GetDeals_Promo_DealOfTheDay();
            this._fn = '_GetDeals_Promo_DealOfTheDay';
            this._SortByFilterVisible = false;
        }else if(this.searchByCategory == 'featured-deals'){
            this._DataService._GetDeals_Promo_FeaturedDeals();
            this._fn = '_GetDeals_Promo_FeaturedDeals';
            this._SortByFilterVisible = false;
        }else if(this.searchByCategory == 'product-deals'){
            this._DataService._GetDeals_ProductDeals();
            this._fn = '_GetDeals_ProductDeals';
        } else if(this.searchByCategory == 'recently-vieweddeals'){
            this._DataService.CustomerDealHistory;
            this._DataService.TotalRecords = this._DataService.CustomerDealHistory?.length;
        } else if(this.searchByCategory == 'fresh-deals'){
            this._DataService._GetDeals_Promo_New()
            this._fn = '_GetDeals_Promo_New';
        }else if(this.searchByCategory == 'ending-soon'){
            this._DataService._GetDeals_Promo_Ending();
            this._fn = '_GetDeals_Promo_Ending';
            this._SortByFilterVisible = false;
        }else if(this.searchByQuery !=undefined){
            this._DataService.DealsSearchCondition = this.searchByQuery;
            this._DataService.DealsSortOrder = '';
            this._DataService._GetDeals();
            this._fn = '_GetDeals';
            if (this._DataService._Deals.length === 0) {
                this.no_result_found = true;
            } else {
                this.no_result_found = false;
            }

        }
        else{
            this._DataService._GetDeals();
            this._fn = '_GetDeals';
        }
    }
    onScrollDown() {
        // this._DataService._GetDeals();
        this.getDealsByQueryParam();
    }
    onUp() {
    }
    _SearchDeal() {
        this._DataService._GetDealsReset();
        this._DataService._GetDeals();
    }
    navigateToRoute(route, TItem, event) {
        event.stopPropagation();
        if (!TItem.IsSoldout) { this._Router.navigate(route); }
    }

    public CategorySearchContent = "";
    _SearchCategory() {
        if (this.CategorySearchContent != undefined && this.CategorySearchContent != null && this.CategorySearchContent != "") {
            this._DataService._CategoriesSorted = this._DataService._Categories.filter(x => x.Name.toLowerCase() == this.CategorySearchContent || x.Name.toLowerCase().startsWith(this.CategorySearchContent));
        }
        else {
            this._DataService._CategoriesSorted = this._DataService._Categories;
        }
    }

    _SubCategorySelected(Item, makeFalse?: boolean) {
        this._DataService._MerchantsSorted = [];
        this._DataService._MerchantSelected = [];
        var SubCat = this._DataService._SubCategoriesSorted.find(x => x.ReferenceKey == Item.ReferenceKey);
        if (SubCat != undefined) {
            if (!makeFalse) {
                if (SubCat.IsChecked) {
                    SubCat.IsChecked = false;
                    var Items = this._DataService._SubCategoriesSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                    if (Items.length > 0) {
                        var ItemIndex = this._DataService._SubCategoriesSelected.indexOf(Items[0]);
                        this._DataService._SubCategoriesSelected.splice(ItemIndex, 1);
                    }
                }
                else {
                    var Items = this._DataService._SubCategoriesSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                    if (Items.length == 0) {
                        this._DataService._SubCategoriesSelected.push(Item);
                    }
                    SubCat.IsChecked = true;
                }
                this._DataService._GetDealsReset();
                this.getDealsByQueryParam();
            } else {
                SubCat.IsChecked = false;
                var Items = this._DataService._SubCategoriesSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                if (Items.length > 0) {
                    var ItemIndex = this._DataService._SubCategoriesSelected.indexOf(Items[0]);
                    this._DataService._SubCategoriesSelected.splice(ItemIndex, 1);
                }
            }
            this._ManageMerchantList('_SubCategoriesSelected', 'SubCategoryDistribution');
        }
    }

    _CategorySelected(Item, event:any= null) {
        if(Item.Deals <= 0){
            this.no_result_found=true;
        }else{
            this.no_result_found=false;
        }
        this.resetCategoryChecked();
        var SCat = this._DataService._CategoriesSorted.find(x => x.ReferenceKey == Item.ReferenceKey);
        if (SCat != undefined) {
            if (!event.target.checked) {
                SCat.IsChecked = false;
                var Items: any = this._DataService._CategoriesSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                if (Items.length > 0) {
                    Item?.SubCategories?.forEach(ele => {
                        this._SubCategorySelected(ele, true);
                    });
                    var ItemIndex = this._DataService._CategoriesSelected.indexOf(Items[0]);
                    this._DataService._CategoriesSelected.splice(ItemIndex, 1);
                }
            }
            else {
                var Items: any = this._DataService._CategoriesSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                if (Items.length == 0) {
                    this._DataService._CategoriesSelected.push(Item);
                }
                SCat.IsChecked = true;
            }
        }
        this._ManageMerchantList('_CategoriesSelected', 'CategoryDistribution');

        // subcat manipulation
        this._DataService._SubCategoriesSorted = [];
        this._DataService._CategoriesSelected.forEach(element => {
            if (element.SubCategories?.length) {
                element.SubCategories.forEach(subEl => subEl.IsChecked = subEl.IsChecked ? true : false);
                this._DataService._SubCategoriesSorted.push(...element.SubCategories)
            }
        });

        this._DataService._GetDealsReset();
        // this._DataService._GetDeals();
        this.getDealsByQueryParam();
    }
    _ManageMerchantList(control:string, distribution:string) {
        this._DataService._MerchantsSorted = [];
        this._DataService._MerchantSelected = [];
        let selectedCategoryIds = this._DataService[control].map((ele: any) => ele.ReferenceKey);
        if(selectedCategoryIds.length > 0){
            this._DataService._OriginalMerchants.forEach((element:any) => {
                element.TotalDeals = 0;
                element[distribution].forEach(cat => {
                    selectedCategoryIds.forEach(ids => {
                        if(ids == cat.CategoryKey){
                            element.TotalDeals = element?.TotalDeals + cat.Count;
                            element.IsChecked = false;
                            if(this._DataService._MerchantsSorted.indexOf(element) == -1){
                                this._DataService._MerchantsSorted.push(element);
                            }
                        }
                        this._DataService._Merchants = this._DataService._MerchantsSorted;
                    })
                })
            })
        }else{
            this._DataService._MerchantsSorted = this._DataService._OriginalMerchants;
            this._DataService._Merchants = this._DataService._OriginalMerchants;
        }
    }

    ResetFilters()
    {
        this.resetCategoryChecked();
        this._SearchByPriceClear();
    }


    public MerchantSearchContent = "";
    _SearchMerchant() {
        if (this.MerchantSearchContent != undefined && this.MerchantSearchContent != null && this.MerchantSearchContent != "") {
            this._DataService._MerchantsSorted = this._DataService._Merchants.filter(x => x.Name.toLowerCase() == this.MerchantSearchContent || x.Name.toLowerCase().startsWith(this.MerchantSearchContent));
        }
        else {
            this._DataService._MerchantsSorted = this._DataService._Merchants;
        }
    }
    _MerchantSelected(Item) {
        var SCat = this._DataService._MerchantsSorted.find(x => x.ReferenceKey == Item.ReferenceKey);
        if (SCat != undefined) {
            if (SCat.IsChecked) {
                SCat.IsChecked = false;

                var Items = this._DataService._MerchantSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                if (Items.length > 0) {
                    var ItemIndex = this._DataService._MerchantSelected.indexOf(Items[0]);
                    this._DataService._MerchantSelected.splice(ItemIndex, 1);
                }
            }
            else {
                var Items = this._DataService._MerchantSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                if (Items.length == 0) {
                    this._DataService._MerchantSelected.push(Item);
                }
                SCat.IsChecked = true;
            }
        }
        this._DataService._GetDealsReset();
        this.getDealsByQueryParam();
    }

    _SortBy(Item) {
        this._DataService.DealsSortOrder = Item;
        this._DataService._GetDealsReset();
        this.getDealsByQueryParam();
    }

    _Pricing =
        {
            MinPrice: 0,
            MaxPrice: 0,
        };

    _SearchByPrice() {
        this._DataService._GetDealsReset();
        this._DataService.MinPrice = this.minValue;
        this._DataService.MaxPrice = this.maxValue;
        this.getDealsByQueryParam();
    }
    _SearchByPriceClear() {
        this._DataService._GetDealsReset();
        this._Pricing =
        {
            MinPrice: 0,
            MaxPrice: 0,
        };
        this._DataService.MinPrice = 0;
        this._DataService.MaxPrice = 0;
        this.getDealsByQueryParam();
    }
    _PricingClick(Item) {
        this._DataService._GetDealsReset();
        this._Pricing.MinPrice = Item.MinPrice;
        this._Pricing.MaxPrice = Item.MaxPrice;
        this._DataService.MinPrice = Item.MinPrice;
        this._DataService.MaxPrice = Item.MaxPrice;
        this.getDealsByQueryParam();
    }
    _Pricings = [
        {
            Title: '0 - 1000',
            MinPrice: 0,
            MaxPrice: 1000
        },
        {
            Title: '1000 - 5000',
            MinPrice: 1000,
            MaxPrice: 5000
        },
        {
            Title: '5000 - 10000',
            MinPrice: 5000,
            MaxPrice: 10000
        },
        {
            Title: '10000 - 20000',
            MinPrice: 10000,
            MaxPrice: 20000
        },
        {
            Title: '20000 - 50000',
            MinPrice: 20000,
            MaxPrice: 50000
        },
        {
            Title: '50000 + ',
            MinPrice: 50000,
            MaxPrice: 0
        }

    ]
    resetCategoryChecked() {
        this._DataService._GetDealsReset();
        this._DataService._CategoriesSelected = [];
        this._DataService._SubCategoriesSelected = [];
        this._DataService._MerchantsSorted = this._DataService._OriginalMerchants;
        this._DataService._Merchants = this._DataService._OriginalMerchants;
        this._DataService?._CategoriesSorted.forEach(ele => delete ele.IsChecked);
        this._DataService?._SubCategoriesSorted.forEach(ele => delete ele.IsChecked);
        this._DataService._MerchantsSorted.forEach(ele => delete ele.IsChecked);
        this._DataService._MerchantSelected = [];
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Categories, this._DataService._CategoriesSorted);
    }

    filterBatch(data: string) {
        switch (data) {
            case 'top-deals':
                return this._DataService._DealsPromotionTop;
                break;

            case 'flash-deals':
                return this._DataService._DealsPromotioDoD;
                break;

            case 'featured-deals':
                return this._DataService._DealsPromotionFeatured
                break;

            case 'product-deals':
                return this._DataService._DealsProducts
                break;

            case 'fresh-deals':
                return this._DataService._DealsPromotionNew
                break;

            case 'ending-soon':
                return this._DataService._DealsPromotionEndingSoon
                break

            default:
                return this._DataService._Deals
                break
        }
    }
    userChange(data) {
        this.minValue = data.value
        this.maxValue = data.highValue
    }
}