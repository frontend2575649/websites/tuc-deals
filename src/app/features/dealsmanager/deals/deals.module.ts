import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TUCDealsComponent } from './deals.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CountdownModule } from 'ngx-countdown';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxSliderModule } from '@angular-slider/ngx-slider';

const routes: Routes = [
    { path: '', component: TUCDealsComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUDealsRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TUDealsRoutingModule,
        InfiniteScrollModule,
        CountdownModule,
        SharedModule,
        NgxSliderModule,
        NgxSkeletonLoaderModule.forRoot({theme: {'margin-bottom': '0'}}),
    ],
    declarations: [TUCDealsComponent]
})
export class TUCDealsModule {
}
