import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { ICategory, OResponse, IDeal } from 'src/app/service/interface.service';
import { SwiperOptions } from 'swiper';
// import Swiper core and required modules
import SwiperCore, { EffectCards } from "swiper";
import { Router,ActivatedRoute } from '@angular/router';
import { __param } from 'tslib';
declare let document:any;
// install Swiper modules
SwiperCore.use([EffectCards]);
@Component({
    selector: 'tuc-home',
    templateUrl: './home.component.html',
    styleUrls:['./home.component.css']
})
export class TUCHomeComponent implements OnInit {

    flashCountDown;
    banners:any = [
        'assets/images/banners/slider-banner-1.webp',
        'assets/images/banners/slider-banner-2.webp',
        'assets/images/banners/slider-banner-3.webp',
    ]
    public Images: any =
        [
        ]
    public _SLDod: OwlOptions = {
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        dots: false,
        dotsEach: false,
        navSpeed: 700,
        navText: ['<i class="icofont-thin-left"></i>', '<i class="icofont-thin-right"></i>'],
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1
            },
            740: {
                items: 1
            },
            940: {
                items: 1
            }
        },
        nav: false
    }
    public _SLMain: OwlOptions = {
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        dots: false,
        dotsEach: false,
        navSpeed: 700,
        navText: ['<i class="icofont-thin-left"></i>', '<i class="icofont-thin-right"></i>'],
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1
            },
            740: {
                items: 1
            },
            940: {
                items: 1
            }
        },
        nav: false
    }
    public _SLCat: OwlOptions = {
        loop: false,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        dots: true,
        dotsEach: false,
        navSpeed: 700,
        navText: ['<i class="icofont-thin-left"></i>', '<i class="icofont-thin-right"></i>'],
        responsive: {
            0: {
                items: 3
            },
            400: {
                items: 3
            },
            740: {
                items: 7
            },
            940: {
                items: 7
            }
        },
        nav: false
    }
    public _SLStore: OwlOptions = {
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        dots: false,
        margin: 16,
        dotsEach: false,
        navSpeed: 700,
        navText: ['<i class="icofont-thin-left"></i>', '<i class="icofont-thin-right"></i>'],
        responsive: {
            0: {
                items: 2
            },
            400: {
                items: 2
            },
            740: {
                items: 4
            },
            940: {
                items: 4
            }
        },
        nav: false
    }
    isDragging: boolean = false;

    public _SLGen: OwlOptions = {
        loop: false,
        mouseDrag: true,
        touchDrag: true,
        autoWidth: true,
        autoHeight: false,
        pullDrag: true,
        dots: false,
        dotsEach: false,
        navSpeed: 700,
        navText: ['<i class="icofont-thin-left"></i>', '<i class="icofont-thin-right"></i>'],
        responsive: {
            0: {
                items: 1,
                autoWidth:true 
            },
            400: {
                items: 1
            },
            740: {
                items: 3
            },
            940: {
                items: 4
            }
        },
        nav: true
    }
    sum = 100;
    throttle = 300;
    scrollDistance = 1;
    scrollUpDistance = 2;
    direction = "";
    subCategories: any;
    selectedCategory: any;
    selectedSubCatImg: any;
    innerWidth: number = window.innerWidth;
    constructor(
        public _HelperService: HelperService,
        public _DataService: DataService,
        private router: Router,
        private route:ActivatedRoute
    ) {
    }
    config: SwiperOptions = {
        slidesPerView: 3,
        spaceBetween: 50,
        navigation: true,
        pagination: { clickable: true },
        scrollbar: { draggable: true },
    };
    showCategorySection:boolean = false;
    _SelectedCategoryForSearch:any;
    onSwiper(swiper) {
    }
    onSlideChange() {
    }
    accesskey:any
    ngOnInit() {
        this.route.queryParams.subscribe(params=>{
            this.accesskey= params
        })
        if(this.accesskey.accesskey != undefined || this.accesskey.accessKey != null){
            this.userLoginWithAccessKey()
        }
        
        this._DataService.$showCategorySectionScource.subscribe(data=> this.showCategorySection = data)
        this._DataService._GetDealsReset();
        this._DataService._GetDeals();
        this._DataService._GetDeals_Promo_New();
        this._DataService._GetDeals_Promo_Discount();
        this._DataService._GetDeals_Promo_Ending();
        this._DataService._GetDeals_ProductDeals();
        this._DataService._GetDeals_Promo_FeaturedDeals();
        this._GetBannerImages();
        // this._DataService._GetDeals_Promo_Top();
        this._GetSliderImages();
        if (this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.CustomerDealHistory) == undefined) {
            this._DataService.CustomerDealHistory = [];
        } else {
            this._DataService.CustomerDealHistory = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.CustomerDealHistory)
        }
        this.getTimeDifference()
    }
    userLoginWithAccessKey(){
        this._HelperService.IsProcessing = true;
                var pData = {
                  Task:"impersonate",
                  AccessKey : this.accesskey.accesskey
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Impersonate.Impersonate, pData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            var _Res = _Response.Result;
                            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Acc, _Response.Result);
                            this._DataService.Profile = _Response.Result;
                            this._HelperService.RefreshHelper();
                            this._DataService._GetBalannce();
                            this._DataService._GetBookmarks();
                            let isResetFlow = localStorage.getItem('resetflow');
                            if(isResetFlow == 'true'){
                                localStorage.removeItem('resetflow');
                                this.router.navigate(['/home']);
                            }else{
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.HandleException(_Error);
                    });
            }
    

    getTimeDifference(){
        var d = new Date();
        var h = d.getHours();
        var m = d.getMinutes();
        var s = d.getSeconds();
        this.flashCountDown = (24*60*60) - (h*60*60) - (m*60) - s;
    }

    on_carouselDrag(dragging: boolean) {
        setTimeout(() => {
            this.isDragging = dragging;
        }, 100)
    }
    navigateToRoute(route, TItem) {
        if (!this.isDragging && !TItem.IsSoldout) { this.router.navigate(route); }
    }

    _HomeSliderimg: any = [];
    _GetSliderImages() {
        var Cache = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.HomeSl1);
        if (Cache != null) {
            this._HomeSliderimg = Cache;
        }
        var pData = {
            Task: 'getsliderimages',
            Location: 'home1'
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HomeSliderimg = _Response.Result.Items;
                    //this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.HomeSl1, this._HomeSliderimg);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    HomeSl1Click(Item) {
        if (Item.TypeCode == 'url') {
            if (Item.Url != undefined && Item.Url != null && Item.Url != '') {
                window.open(Item.Url, "_blank");
            }
        }
        else {
            if (Item.DealKey != undefined && Item.DealKey != null && Item.DealKey != "") {
                window.location.href = 'buydeal/' + Item.DealKey;
            }
        }
    }

    _HomeBannerimg: any = [];
   
    _GetBannerImages() {
        var pData = {
            Task: 'getbannerimages',
            Location: 'home1'
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HomeBannerimg = _Response.Result.Items;
                    //This line feeds false cache data to the slider at the top
                   // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.HomeSl1, this._HomeBannerimg);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    BannerImagesClick(image){
        if (image.TypeCode == 'url') {
            if (image.Url != undefined && image.Url != null && image.Url != '') {
                window.open(image.Url, "_blank");
            }
        }
        else {
            if (image.ReferenceKey != undefined && image.ReferenceKey != null && image.ReferenceKey != "") {
                window.location.href = 'buydeal/' + image.ReferenceKey;
            }
        }
    }

    GetDeals(Item) {
        window.location.href = 'category/' + Item.ReferenceId;
    }

    GetMerchants(Item) {
        window.location.href = 'stores/' + Item.ReferenceKey;
    }

    convertRouteName(Name: string): string {
        return Name.toLowerCase().split("-").join("").split(" ").join("");
    }

    cutString(data:string){
        if(data)
        {
            if(data.length >30)
            {
                return `${data.substring(0,30)}...`
            }
         }
        return data
    }

    openSubcatSection(selectedCategory: any){
        document.getElementById('cust_cat_overlay').style.display = 'block';
        document.getElementById('show-subcat').style.display = 'flex';        
        document.querySelector('.banner_category-list-sub').style.display = 'block';
        document.querySelector('.banner-block').style.zIndex = 'auto';
        this._SelectedCategoryForSearch = selectedCategory;
        // call api for subcat
        this.selectedCategory = selectedCategory;        
        this.selectedSubCatImg = selectedCategory?.SubCategories?.length ? selectedCategory?.SubCategories[0] : null;
    }

    calculatePercentageOff(originalValue:any, discountedValue:any) {
        // Calculate the difference between the original value and discounted value
        let difference = originalValue - discountedValue;      
        // Calculate the percentage off
        let percentageOff = (difference / originalValue) * 100;
        // Return the percentage off as a positive number
        return Math.ceil(percentageOff);
      }

}
