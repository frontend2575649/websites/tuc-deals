import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { OResponse } from 'src/app/service/interface.service';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['../profile/profile.component.css']
})
export class WishlistComponent implements OnInit {
  _DealsBookmark: any[] = [];
  _DealsBookmarkLoading: boolean = false;
  public GlobalSearchParameter = "";
  constructor(
    public _HelperService: HelperService,
    public _DataService: DataService) { }

  ngOnInit(): void {
  }
  @Input('fetchWishlist') set fetchWishlist(value) {
    value && this._GetBookmarks();
  }
  Totalcount: any;
  _GetBookmarks() {
    // let cacheBookmarkedDeals = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.bookmarkedDeals);
    // if (cacheBookmarkedDeals) {
    //   this._DealsBookmark = cacheBookmarkedDeals;
    // }
    this._DealsBookmarkLoading = true;
    var pData = {
      Task: 'getbookmarks',
      Offset: 0,
      Limit: 10,
      SearchCondition: this.GlobalSearchParameter,
      ReferenceId: 0,
      ReferenceKey: null
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
    _OResponse.subscribe(
      _Response => {
        this._DealsBookmarkLoading = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._DealsBookmark = [];
          this._DataService.TotalBookmarkCount = _Response.Result.TotalRecords;
          _Response.Result.Data.forEach(element => {
            element.CountdownConfig = this._DataService.TimerCode(element.DealEndDate)
            this._DealsBookmark.push(element);
          });
          this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.bookmarkedDeals, this._DealsBookmark);
        }
      },
      _Error => {
        this._DealsBookmarkLoading = false;
        this._HelperService.HandleException(_Error);
      });
  }

  DeleteWishList(TItem) {
    var _LoginCheck = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Acc);
    if (!_LoginCheck) {
      this._HelperService.NotifyError(`Please <a href="/auth/login" style="color: #fff; text-decoration: underline;"><u>login</u></a> to wishlist deal`, 5);
    }
    else {
      var pData = {
        Task: 'savebookmark',
        DealKey: TItem.DealKey,
      }
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
      _OResponse.subscribe(
        _Response => {
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess(_Response.Message);
            this._GetBookmarks();
          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.HandleException(_Error);
        }
      );
    }
  }

}
