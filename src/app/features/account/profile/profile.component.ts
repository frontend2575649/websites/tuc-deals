import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { IDeal, OResponse } from 'src/app/service/interface.service';
import * as moment from 'moment';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

declare var $:any;

@Component({
    selector: 'tuc-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css'],
})


export class TUCProfileComponent implements OnInit {
    
    shippingAddressForm: FormGroup;
    fethingAddressLoading: boolean = false;
    memberSince: any = "";
    _Profile: any = {
        FirstName: null,
        MiddleName: null,
        LastName: null,
        EmailAddress: null,
        Gender: null,
        Day: null,
        Month: null,
        Year: null,
        Address: null,
        DateOfBirth: null
    };
    
    _DealCode: any;
   
    shippingAddresses: any = [];
    profileAvatar: { Name: string, Type: string, Size: number, Extension?: string, Content: any, IsDefault?: any };
    profileAvatarSpinner: boolean = false;
    YearList: number[] = [];
    citiesAgainstState: any = [];
    PuchasedDeals: any[] = [];
    fetchWishlist: boolean = false;
    editForm: string;
    selectedAddress: any = null;
    LocationTypeList: { LocationTypeId: number, Name: string, LocationTypeCode: string }[] = [
        {
            LocationTypeId: 791,
            Name: "Home",
            LocationTypeCode: "locationtype.residential"
        },
        {
            LocationTypeId: 792,
            Name: "Work",
            LocationTypeCode: "locationtype.work"
        },
        {
            LocationTypeId: 800,
            Name: "Other",
            LocationTypeCode: "locationtype.other"
        }
    ]
    offsetPOS:number;
    fragment:any;
    maxDate :any = new Date();
    initShippingAddressForm(): void {
        this.shippingAddressForm = this.fb.group({
            FirstName: ['', Validators.required],
            LastName: ['', Validators.required],
            City: ['', Validators.required],
            State: ['', Validators.required],
            EmailAddress: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
            AddressLine1: ['', Validators.required],
            AddressLine2: [''],
            PhoneNumber: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/), Validators.minLength(10)]],
            // AdditionalNumber: this.fb.group({
            //     Code: ['234'],
            //     Number: ['', [Validators.pattern(/^[0-9]\d*$/), Validators.minLength(10), Validators.maxLength(11)]]
            // }),
            LocationType: [791]
        })
    }

    @ViewChild('confirmDeleteAddressModal') _modal: any;
    constructor(
        public _HelperService: HelperService,
        public _DataService: DataService,
        private _NgbModal: NgbModal,
        private fb: FormBuilder,
        private router: Router,
        private route: ActivatedRoute) {
        for (let i = 1940; i <= new Date().getFullYear(); i++) {
            this.YearList.push(i);
        }
        this.route.fragment.subscribe(fragment => { this.fragment = fragment; });
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }

    _formAction: any = undefined;

    // 
    get form() {
        return this.shippingAddressForm.controls;
    }
    changeSelectFormControl(control, event?) {
        if (control == 'State') {
            if (event != undefined) {
                this.shippingAddressForm.patchValue({
                    City: ''
                });
            }
            let pData = {
                Task: 'getcities',
                Offset: 0,
                Limit: 1000,
                ReferenceId: this.form['State'].value,			// State Id
                ReferenceKey: this._DataService._AllStates.find(e => e.ReferenceId == this.form['State'].value)?.ReferenceKey,	// State Key
            }
            this._HelperService.IsProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.State, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this.citiesAgainstState = _Response.Result.Data;
                        if (this._formAction) {
                            this.shippingAddressForm.patchValue({
                                City: this._formAction.CityId
                            });
                        }
                    }
                    else {
                        this.citiesAgainstState = [];
                    }
                },
                _Error => {
                    this.citiesAgainstState = [];
                    this._HelperService.HandleException(_Error);
                });
        }

    }
    isSubmittedForm: boolean = false;


    saveAddressForm() {
        this.isSubmittedForm = true;
        if (this.shippingAddressForm.valid) {
            let f = this.shippingAddressForm.value;
            let pData: any = {
                CountryId: this._DataService._CountrySelcted.ReferenceId,
                CountryKey: this._DataService._CountrySelcted.ReferenceKey,
                CityId: f.City,
                EmailAddress: this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Acc)?.EmailAddress,//"omkar@thankucash.com",
                FirstName: f.FirstName,
                LastName: f.LastName,
                Name: `${f.FirstName} ${f.LastName}`,
                ContactNumber: f.PhoneNumber,
                AddressLine1: f.AddressLine1,
                // AdditionalNumber: f.AdditionalNumber.Number ? `${f.AdditionalNumber.Code}${f.AdditionalNumber.Number}` : '',
                StateId: f.State,
                StateKey: this._DataService._AllStates.find(e => e.ReferenceId == f.State)?.ReferenceKey,
                ZipCode: f.Zip ? f.Zip : null,

                "IsPrimary": false,
                "LocationTypeId": f.LocationType,
                "LocationTypeCode": this.LocationTypeList.find(ele => ele.LocationTypeId == f.LocationType)?.LocationTypeCode,
                "DisplayName": null,
                "Landmark": null,
                "CityAreaId": 0,
                "Latitude": 0.0,
                "Longitude": 0.0,
                "MapAddress": null,
                "Instructions": null,
            }
            // api 
            if (this._formAction == undefined) { // new create flow
                pData.Task = "saveaddress";
                this.shipmentAddressApi(pData);

            } else { // update  
                pData.Task = 'updateaddress';
                pData.ReferenceId = this._formAction.ReferenceId;
                pData.ReferenceKey = this._formAction.ReferenceKey;
                this.shipmentAddressApi(pData);
            }
        }
    }
    shipmentAddressApi(pData) {
        this._HelperService.IsProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
        _OResponse.subscribe(
            _Response => {

                this._HelperService.IsProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }



    openAccordian(event) {
        let index = event?.panelId?.split('_')[1];
        let selectedOrder = this._DataService._DealOrderHistory[index];
        this._DataService.DealCode = selectedOrder.DealKey;
        var pData = {
            Task: 'getdealcode',
            Host: this._HelperService.AppConfig.HostType,
            DealCodeKey: selectedOrder.DealKey,
            ReferenceId: selectedOrder.ReferenceId,
            ReferenceKey: selectedOrder.ReferenceKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._DealCode = _Response.Result;
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    ngOnInit() {
        this.initShippingAddressForm();
        this.getProfile();
        this.fetchAddress(true);
        this._DataService.CustomerCart = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.CustomerCart);
        this.changPanelRoute(this.fragment);
        let accDate = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Acc).CreatedAt;
        this.memberSince = `${new Date(accDate).toLocaleString('default', {month: 'long'})}, ${new Date(accDate).getFullYear()}`
    }
 

    showPanel: any = {
        "wishList": false,
        "purchasedDeal": false,
        "orderHistory": false,
        "changePin": false,
        "pointbalance": false,
        "referral": false,
        "myaccount": true,
    };
    showAccountPannel: boolean = true;
    changPanelRoute(route: string) {
        if(!route)
        {
            route = 'myaccount'
        }
        for (let pl in this.showPanel) {
            if (pl == route) {
                this.showPanel[route] = true;
            } else {
                this.showPanel[pl] = false
            }
        }
        this.router.navigate( ['/profile' ], {fragment: route, replaceUrl: true});
        switch (route) {
            case 'orderHistory': {
                this._DataService._GetPuchasedDeals();
                break;
            }

            case 'purchasedDeal': {
                this._GetPuchasedDeals();
                break;
            }
            case 'wishList': {
                this.fetchWishlist = true;
                break;
            }
            case 'changePin': {
                break;
            }
            case 'pointbalance': {
                this._DataService.GetPointPurchaseHistory();
                break;
            }
            case 'myaccount': {
                break;
            }

        }
    }
    getProfile() {
        this._DataService.Profile = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Profile);
        var pData = {
            Task: 'getaccount',
            RequestType: "web"
        };
        this._HelperService.IsProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._DataService.Profile = _Response.Result;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Profile, _Response.Result);
                    let date = new Date(_Response.Result.DateOfBirth);
                    this._DataService.shareReferralUrl = this._DataService.Profile.ReferralCode ? `${window.location.origin}/auth/signup?referral-code=${this._DataService.Profile.ReferralCode}` : '';
                    this._DataService.shareReferralCode = this._DataService.Profile.ReferralCode ? this._DataService.Profile.ReferralCode : '';
                    this._Profile = {
                        ..._Response.Result,
                        Day: date.getDate() || null,
                        Month: date.getMonth() == undefined || date.getMonth() == null ? null : date.getMonth(),
                        Year: date.getFullYear() || null,
                        DateOfBirth: _Response.Result.DateOfBirth ? moment(_Response.Result.DateOfBirth).format('YYYY-MM-DD') : null,
                        Gender: _Response.Result.Gender || null
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    logout(): void {
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageItem.Acc);
        this._HelperService.DeleteStorage('accbal');
        this._HelperService.DeleteStorage('auth');
        this._HelperService.RefreshHelper();
        window.location.href = "home";
    }

    resetProfileForm() {
        let date = new Date(this._DataService.Profile.DateOfBirth);
        this._Profile = {
            ...this._DataService.Profile,
            Day: date.getDate() || null,
            Month: date.getMonth() == undefined || date.getMonth() == null ? null : date.getMonth(),
            Year: date.getFullYear() || null,
            Gender: this._DataService.Profile.Gender || null
        };
    }

    isEmailValidate: boolean = false;
    EmailValid(event: any) {
        if (event.target.value) {
            if (event.target.value.match(this._HelperService.AppConfig.EmailPattern)) {
                this.isEmailValidate = true;
            }
            else {
                this.isEmailValidate = false;
                this._Profile.EmailAddress = event.target.value;
            }
        }
    }
    validateEmail = (email) => {
        return email.match(
          /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
     };

    SetProfile() {
        this.EmailValid({target:{value: this._Profile.EmailAddress}});
        if (this._Profile.FirstName==null || this._Profile.FirstName==undefined || this._Profile.FirstName =='') {
            this._HelperService.NotifyError("Enter first name");
        }
        else if (this._Profile.LastName==null || this._Profile.LastName==undefined || this._Profile.LastName =='') {
            this._HelperService.NotifyError("Enter last name");
        }
        
        else if (this._Profile.EmailAddress && !this.validateEmail(this._Profile.EmailAddress)) {
            this._HelperService.NotifyError("Enter valid email address");
        }
        else if (!this._Profile.Gender) {
            this._HelperService.NotifyError("Select gender");
        }
        else if (!this._Profile.DateOfBirth) {
            this._HelperService.NotifyError("Select date of birth");
        }
        else {
            this._HelperService.IsProcessing = true;
            var pData: any = {
                Task: 'updateaccount',
                Host: this._HelperService.AppConfig.HostType,
                FirstName: this._Profile.FirstName,
                MiddleName: this._Profile.MiddleName,
                LastName: this._Profile.LastName,
                EmailAddress: this._Profile.EmailAddress,
                Address: this._Profile.Address,
                Gender: this._Profile.Gender,
                DateOfBirth: new Date(this._Profile.DateOfBirth),
            };
            if (this.profileAvatar?.Content) {
                pData.ImageContent = { ...this.profileAvatar, Content: this.profileAvatar.Content.toString().split(';base64,')[1] }
            }
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._DataService.Profile = {
                            ...this._DataService.Profile,
                            FirstName: pData.FirstName,
                            MiddleName: pData.MiddleName,
                            LastName: pData.LastName,
                            EmailAddress: pData.EmailAddress,
                            Address: pData.Address,
                            Gender: pData.Gender,
                            Name: `${pData.FirstName} ${pData.LastName}`,
                            DateOfBirth: pData.DateOfBirth
                        };
                        this._Profile.Name = `${pData.FirstName} ${pData.LastName}`,                        
                        this.profileAvatarSpinner = false;
                        if (this.profileAvatar?.Content) {
                            this._DataService.Profile.IconUrl = this.profileAvatar?.Content;
                        }
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Profile, this._DataService.Profile);
                        let accDisplayName = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Acc);
                        accDisplayName.DisplayName = pData.FirstName;
                        accDisplayName.EmailAddress = pData.EmailAddress;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Acc, accDisplayName);
                        this._HelperService.RefreshHelper();
                        // this._HelperService.NotifySuccess("Profile Updated Successfully");
                        this._HelperService.NotifySuccess(_Response.Message);

                    }
                    else {
                        this.profileAvatarSpinner = false;
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
    }

    _GetPuchasedDeals() {
        this.PuchasedDeals = [];
        var pData = {
            Task: 'getpurchasehistory',
            Offset: 0,
            Limit: 10
        };
        this._HelperService.IsProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.PuchasedDeals = _Response.Result.Data;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    confirmLogout(content) {
        this._NgbModal.open(content, { centered: true, modalDialogClass: 'tuc-confirm-logout-body' });
    }
    onSelectFile(event) {
        if (event.target.files && event.target.files[0]) {
            let { type } = event.target.files[0];
            if (type?.split('/')[0] == 'image') {
                this.profileAvatarSpinner = true;
                var reader = new FileReader();
                reader.readAsDataURL(event.target.files[0]);
                reader.onload = (rederEvt) => {
                    this.profileAvatar = {
                        Name: event.target.files[0].name,
                        Size: event.target.files[0].size,
                        Extension: event.target.files[0].name.split(".")[1],
                        Type: event.target.files[0].type,
                        Content: rederEvt.target?.result,
                        IsDefault: 1
                    };
                    this.SetProfile();
                }
            } else {
                this._HelperService.NotifyError('Please upload image only')
            }
        }
    }
    fetchAddress(event?) {
        this.fethingAddressLoading = true;
        if (event == true) {
            this._HelperService.IsProcessing = true;
            var pData = {
                Task: "getaddresslist",
                "TotalRecords": 0,
                "Offset": 0,
                "Limit": 100,
                "RefreshCount": true,
                "SearchCondition": "",
                "SortExpression": "CreateDate desc"
            };
            let _OResponse: Observable<OResponse>;

            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
            _OResponse.subscribe(
                _Response => {
                    this.fethingAddressLoading = false;
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this.shippingAddresses = [];
                        this.shippingAddresses = _Response.Result.Data;
                        if (_Response.Result.Data.length > 0) {
                            this._formAction = _Response.Result.Data[0];
                            this.setFormData();
                        }
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this.fethingAddressLoading = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    setFormData() {
        let f: any = this._formAction;
        this.shippingAddressForm.patchValue({
            FirstName: f.Name.split(' ')[0],
            LastName: f.Name.split(' ')[1],
            EmailAddress: f.EmailAddress,
            State: f.StateId,
            City: f.CityId,
            AddressLine1: f.AddressLine1,
            // AddressLine2: f.AddressLine2,
            PhoneNumber: f.ContactNumber,
            // AdditionalNumber: {
            //     Code: f.AdditionalNumber ? f.AdditionalNumber?.slice(0, 3) : '234',
            //     Number: f.AdditionalNumber?.slice(3)
            // },
            LocationType: f.LocationTypeId
        });
        this.changeSelectFormControl('State');
    }
    openShippingAddressModal(content, action?, type?: string) {
        this._NgbModal.open(content, { size: 'md', centered: type == 'delete' ? true : false, modalDialogClass: 'tuc-shipping-address-modal' });
        this.editForm = action;
        if (type == 'delete') {
            this.selectedAddress = action;
        }
    }

    deleteShippingAddress() {
        if (this.selectedAddress != undefined && this.selectedAddress !== null) {
            // call delete api 
            this._HelperService.IsProcessing = true;
            var pData = {
                Task: "deleteaddress",
                ReferenceId: this.selectedAddress.ReferenceId,
                ReferenceKey: this.selectedAddress.ReferenceKey
            };
            let _OResponse: Observable<OResponse>;

            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this.fetchAddress(true);
                        this._NgbModal.dismissAll();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
    }


    GoToDeals() {
        this.router.navigate[('home')];
    }


    // new my account
    AccountData: boolean = true;
    AccountEdit: boolean = false;
    AddressData: boolean = true;
    AddressEdit: boolean = false;

    AccountToggle() {
        this.AccountData = false;
        this.AddressData = false;
        this.AccountEdit = true;
    }
    AddressToggle() {
        this.AccountData = false;
        this.AddressData = false;
        this.AddressEdit = true;
    }

    BackAccount() {
        this.AccountData = true;
        this.AddressData = true;
        this.AccountEdit = false;
    }
    BackAddress() {
        this.AccountData = true;
        this.AddressData = true;
        this.AddressEdit = false;
    }


    // change pin
    ShowPassword1: boolean = true;
    ShowPassword2: boolean = true;
    ShowPassword3: boolean = true;

    ToogleShowHidePassword(val: string): void {
        if (val == "old") {
            this.ShowPassword1 = !this.ShowPassword1;
        }
        else if (val == "new") {
            this.ShowPassword2 = !this.ShowPassword2;
        }
        else {
            this.ShowPassword3 = !this.ShowPassword3;
        }
    }
}