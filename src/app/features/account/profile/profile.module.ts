import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TUCProfileComponent } from './profile.component';
import { CountdownModule } from 'ngx-countdown';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgxStarRatingModule } from 'ngx-star-rating';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ChangePinComponent } from '../change-pin/change-pin.component';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import { WishlistComponent } from '../wishlist/wishlist.component';
import { OrderHistoryComponent } from '../order-history/order-history.component';
import { PointBalanceComponent } from '../point-balance/point-balance.component';
import { FlutterwaveModule } from 'flutterwave-angular-v3';
import { Angular4PaystackModule } from 'angular4-paystack';
import { ReferralComponent } from '../referral/referral.component';



const routes: Routes = [
    { path: '', component: TUCProfileComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})



export class TUProfileRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TUProfileRoutingModule,
        CountdownModule,
        NgbModule,
        ShareButtonsModule,
        ShareIconsModule,
        SharedModule,NgxStarRatingModule,
        NgxSkeletonLoaderModule.forRoot({theme: {'margin-bottom': '0'}}),
        FlutterwaveModule,
        Angular4PaystackModule.forRoot('pk_test_xxxxxxxxxxxxxxxxxxxxxxxx'),
    ],
    declarations: [
        TUCProfileComponent,
        ChangePinComponent,
        WishlistComponent,
        OrderHistoryComponent,
        PointBalanceComponent,
        ReferralComponent
    ]
})
export class TUCProfileModule {
}
