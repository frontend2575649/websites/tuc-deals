import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { OResponse } from 'src/app/service/interface.service';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['../profile/profile.component.css']
})

export class OrderHistoryComponent implements OnInit {
  showDetails: boolean = false;
  _DealCode: any;
  Showaddress: boolean = true;
  _DealCodeInProgress: boolean = false;
  _DealRatingNumber: number | null = 0;
  _Cdata = {
    Comment: ""
  }
  RatingGiven: boolean = true;
  constructor(
    public _DataService: DataService,
    public _HelperService: HelperService,
  ) {
  }
  
  ngOnInit(): void { 
    this._DealRatingNumber = 0;
  }

  Detailsclicked(TItem: any) {
    this.showDetails = true;
    let selectedOrder = TItem;
    this._DataService.DealCode = TItem.DealKey;
    this._DealCodeInProgress = true;
    var pData = {
      Task: 'getdealcode',
      Host: this._HelperService.AppConfig.HostType,
      DealCodeKey: selectedOrder.DealKey,
      ReferenceId: selectedOrder.ReferenceId,
      ReferenceKey: selectedOrder.ReferenceKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._DealCode = _Response.Result;
          if (this._DealCode.DeliveryDetails.Name == undefined) {
            this.Showaddress = false
          }
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      },
      () => this._DealCodeInProgress = false);
  }

  SaveRating(Rate) {
    this._HelperService.IsProcessing = true;
    var pData = {
      Task: 'savedealreview',
      Host: this._HelperService.AppConfig.HostType,
      DealId: Rate.DealReferenceId,
      DealKey: Rate.DealReferenceKey,
      Rating: this._DealRatingNumber,
      Review: this._Cdata.Comment,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Rating, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this._Cdata.Comment = "";
          this._DealRatingNumber = 0;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          this._Cdata.Comment = "";
          this._DealRatingNumber = 0;
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      });
  }
  checkReviewCommentValidation() {
    return (this._Cdata.Comment.length && this._Cdata.Comment.split(' ').length > 100) ? false : true;
  }
}
