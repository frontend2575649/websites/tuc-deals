import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Flutterwave } from 'flutterwave-angular-v3';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { OResponse } from 'src/app/service/interface.service';
declare var document: any;
@Component({
  selector: 'app-point-balance',
  templateUrl: './point-balance.component.html',
  styleUrls: ['../profile/profile.component.css']
})
export class PointBalanceComponent implements OnInit {
  pointbalanceInput: any;
  paymentGateway: any;
  PaymentMode = 'paystack';
  public _TransactionReference: any = null;
  constructor(
    public _HelperService: HelperService,
    public _DataService: DataService,
    public _NgbModal: NgbModal,
    public flutterwave: Flutterwave,
  ) { }

  ngOnInit(): void {
  }

  //  point balance
  confirmWallet(content) {
    this.createTransaction();
    console.log('_Ref', this._TransactionReference);
    this._NgbModal.open(content, { centered: true, size: 'sm', modalDialogClass: 'tuc-wallet-topup-body' });
    this.pointbalanceInput = "";
    document.querySelector('#pointbalanceInput').focus();
  }
  changePointbalanceInput(event: any) {
    if (/[0-9]/i.test(event.key)) {

    }
    return /[0-9]/i.test(event.key);
  }

  createTransaction() {
    var Ref = Math.floor(100000 + Math.random() * (999999 + 1 - 100000));
    this._TransactionReference = 'Tp' + '_' + Ref;
  }

  toggle(event: any) {
    this.paymentGateway = event.target.value;
    console.log(this.paymentGateway, "paymentGateway of eval")
  }
  openPaymentGateway() {
    if (!this.pointbalanceInput) {
      this._HelperService.NotifyError('Please enter topup amount');
    } else if (!this.paymentGateway) {
      this._HelperService.NotifyError('Please select payment method');
    } else {
      this._HelperService.IsProcessing = true;
      if (this.paymentGateway == 'paystack') {
        document.getElementById('paymentbutton')?.click();
      } else {
        setTimeout(() => {
          let paymentData = {
            public_key: this._HelperService.AppConfig.FlutterwaveKey,
            tx_ref: this._TransactionReference,
            amount: +this.pointbalanceInput,
            currency: this._DataService._CountrySelcted.CurrencyNotation,
            payment_options: "card, banktransfer, ussd",
            redirect_url: "",
            callback: this.ProcessOnline_Confirm,
            onclose: this.CancelPayment,
            callbackContext: this,
            customer: {
              email: this._HelperService._Account.EmailAddress,
              phone_number: this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Profile).MobileNumber,
              name: this._HelperService._Account.Name,
            },
            customizations: {},
          }
          console.log(paymentData);

          this.flutterwave.inlinePay(paymentData);
        }, 200);
      }
    }
  }
  CancelPayment() {
    this._NgbModal.dismissAll();
    this.paymentGateway = undefined;
    this._HelperService.IsProcessing = false;
  }
  ProcessOnline_Confirm(PaymentResponse: any) {
    console.log(PaymentResponse);
    let pData: any = {
      "Task": "fundwallet",
      "sourceCode": "payments",
      "accountId": this._HelperService._Account.AccountId,
      "accountKey": this._HelperService._Account.AccountKey,
      "amount": this.pointbalanceInput,
      "paymentReference": this._TransactionReference,
      "transactionReference": this._TransactionReference,
      "comment": "",
      "paymentType": "WalletFunding"
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.CancelPayment();
          this._HelperService.NotifySuccess(_Response.Message)
          this._DataService.GetPointPurchaseHistory();
          this._DataService._GetBalannce();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      },
      () => this._HelperService.IsProcessing = false
    );
  }
}
