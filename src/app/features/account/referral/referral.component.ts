import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable } from "rxjs";
import { DataService } from "src/app/service/data.service";
import { HelperService } from "src/app/service/helper.service";
import { OResponse } from "src/app/service/interface.service";
declare var document: any;

@Component({
    selector: 'app-referral',
    templateUrl: './referral.component.html',
    styleUrls: ['../profile/profile.component.css']
})

export class ReferralComponent implements OnInit {
    editReferralCode: string = '';
    @Output('refreshProfile') refreshProfile = new EventEmitter<boolean>();
    constructor(
        public _HelperService: HelperService,
        public _DataService: DataService,
        private _NgbModal: NgbModal,
    ) { }

    ngOnInit(): void { }
    copytoClipboard(referralcode) {
        navigator.clipboard.writeText(referralcode);
    }
    OpenEditModal(content) {
        this.editReferralCode = this._DataService.shareReferralCode;
        this._NgbModal.open(content, { centered: true, size: 'sm', modalDialogClass: 'tuc-edit-referral-modal-body', backdrop: 'static', keyboard: false });
        document.querySelector('#shareReferralCode').focus();
    }
    EditReferralCode() {
        if (!this.editReferralCode || this.editReferralCode.length == 0) {
            this._HelperService.NotifyError('Enter referral Id');
        } else if (this.editReferralCode && this.editReferralCode.length < 6) {
            this._HelperService.NotifyError("Referral ID should be at least 6 charaters in length");
        } else {
            this._HelperService.IsProcessing = true;
            let pData: any = {
                "Task": "updaterefid",
                "emailAddress": this._HelperService._Account.EmailAddress,
                "ReferralCode": this.editReferralCode
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._DataService.shareReferralCode = this.editReferralCode;
                        this.refreshProfile.emit(true);
                        this._HelperService.NotifySuccess(_Response.Message);
                        this._NgbModal.dismissAll();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                },
                () => this._HelperService.IsProcessing = false
            );
        }
    }
}