import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { Observable } from "rxjs";
import { DataService } from "src/app/service/data.service";
import { HelperService } from "src/app/service/helper.service";
import { OResponse } from "src/app/service/interface.service";

@Component({
    selector: 'app-change-pin',
    templateUrl: './change-pin.component.html',
    styleUrls: ['../profile/profile.component.css']
})

export class ChangePinComponent implements OnInit {
    // change pin
    ShowPassword1: boolean = true;
    ShowPassword2: boolean = true;
    ShowPassword3: boolean = true;
    password: { oldPin: any, newPin: any, confirmPin: any } = { oldPin: null, newPin: null, confirmPin: null }
    constructor(
        public _HelperService: HelperService,
        private _DataService: DataService
    ) { }
    @Output() changPanelRoute = new EventEmitter<string>();
    ngOnInit(): void {

    }

    backToMyAccount(route) {
        this.changPanelRoute.emit(route);
    }

    ToogleShowHidePassword(val: string): void {
        if (val == "old") {
            this.ShowPassword1 = !this.ShowPassword1;
        }
        else if (val == "new") {
            this.ShowPassword2 = !this.ShowPassword2;
        }
        else {
            this.ShowPassword3 = !this.ShowPassword3;
        }
    }
    submitForm() {
        if (this.password.oldPin == undefined || this.password.oldPin == null || this.password.oldPin == '') {
            this._HelperService.NotifyError("Enter old pin");
        }
        else if (this.password.newPin == undefined || this.password.newPin == null || this.password.newPin == '') {
            this._HelperService.NotifyError("Enter new pin");
        }
        else if (this.password.confirmPin == undefined || this.password.confirmPin == null || this.password.confirmPin == '') {
            this._HelperService.NotifyError("Enter confirm pin");
        }
        else if (this.password.newPin !== this.password.confirmPin) {
            this._HelperService.NotifyError("Pin mismatch, new pin and confirm pin should be same");
        }
        else {
            // API
            this._HelperService.IsProcessing = true;
            var pData: any = {
                Task: 'changepin',
                "AuthAccountKey": this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Acc)?.AccountKey,
                "OldPin": this.password.oldPin,
                "NewPin": this.password.newPin
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Register, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess(_Response.Message);
                        this.password = { oldPin: null, newPin: null, confirmPin: null };
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
    }

}