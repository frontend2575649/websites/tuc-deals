import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HelperService } from 'src/app/service/helper.service';
import { OResponse } from 'src/app/service/interface.service';


@Component({
    selector: 'tuc-stats',
    templateUrl: './stats.component.html',
})

export class TUCStatsComponent implements OnInit {
    constructor(
        public _HelperService: HelperService
    ) {
    }

    ngOnInit() {
        this.GetStats();
    }

    public _ResponseI: any =
        {
            ActiveDeals: 0,
            ActiveCities: 0,
            ActiveMerchant: 0,
            ActiveStores: 0,
            Cities: [],
            Deals: [],
            Merchants: [],
            Stores: [],

        };

    GetStats() {
        var pData = {
            Task: 'getstats',
            Location: 'home1'
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    // console.log(_Response);
                    this._ResponseI = _Response.Result.Data;
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
}