import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TUCStatsComponent } from './stats.component';

const routes: Routes = [
    { path: '', component: TUCStatsComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUCStatsRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TUCStatsRoutingModule,
    ],
    declarations: [TUCStatsComponent]
})
export class TUCStatsModule {
}
