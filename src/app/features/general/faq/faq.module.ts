import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TUCFaqComponent } from './faq.component';

const routes: Routes = [
    { path: '', component: TUCFaqComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUFaqRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TUFaqRoutingModule,
    ],
    declarations: [TUCFaqComponent]
})
export class TUCFaqModule {
}
