import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TUCTermsComponent } from './terms.component';

const routes: Routes = [
    { path: '', component: TUCTermsComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})



export class TUTermsRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TUTermsRoutingModule,
    ],
    declarations: [TUCTermsComponent]
})
export class TUCTermsModule {
}
