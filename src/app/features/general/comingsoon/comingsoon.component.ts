import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';


@Component({
    selector: 'tuc-comingsoon',
    templateUrl: './comingsoon.component.html',
})


export class TUCComingSoonComponent implements OnInit {
    constructor(private _ActivatedRoute: ActivatedRoute,
        public _DataService: DataService,
        private router: Router) {
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }
    countryName: string = "";
    ngOnInit() {
        this.countryName = this._ActivatedRoute.snapshot.params['country'].toLowerCase();
    }
}