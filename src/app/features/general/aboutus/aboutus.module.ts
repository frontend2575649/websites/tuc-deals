import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TUCAboutusComponent } from './aboutus.component';

const routes: Routes = [
    { path: '', component: TUCAboutusComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUAboutusRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TUAboutusRoutingModule,
    ],
    declarations: [TUCAboutusComponent]
})
export class TUCAboutusModule {
}
