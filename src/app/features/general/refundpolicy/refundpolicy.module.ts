import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TUCRefundPolicyComponent } from './refundpolicy.component';

const routes: Routes = [
    { path: '', component: TUCRefundPolicyComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})



export class TURefundPolicyRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TURefundPolicyRoutingModule,
    ],
    declarations: [TUCRefundPolicyComponent]
})
export class TUCRefundPolicyModule {
}
