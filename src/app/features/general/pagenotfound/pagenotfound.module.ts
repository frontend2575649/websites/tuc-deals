import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TUCPagenotfoundComponent } from './pagenotfound.component';

const routes: Routes = [
    { path: '', component: TUCPagenotfoundComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUPagenotfoundRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TUPagenotfoundRoutingModule,
    ],
    declarations: [TUCPagenotfoundComponent]
})
export class TUCPagenotfoundModule {
}
