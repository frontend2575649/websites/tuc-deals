import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HelperService } from 'src/app/service/helper.service';
import { MixpanelService } from 'src/app/service/mixpanel.service';


@Component({
    selector: 'tuc-pagenotfound',
    templateUrl: './pagenotfound.component.html',
})


export class TUCPagenotfoundComponent implements OnInit {
    constructor(
        private _MixpanelService: MixpanelService,
        private router: Router,
        private _HelperService: HelperService,
    ) {
    }

    ngOnInit() {
        this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.Page404, { 
            url: this.router.url
         })
    }
}