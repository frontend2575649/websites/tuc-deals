import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';

@Component({
  selector: 'tuc-help-center',
  templateUrl: './help-center.component.html',
  styleUrls: ['./help-center.component.css', '../../../auth/authNew/auth.component.css']
})
export class HelpCenterComponent implements OnInit {
  public activeTab = 'contact';

  isDragging: boolean = false;
  _SelectedCountryDetails =
  {
      Isd: '234',
      IconUrl: 'assets/images/flag/ng.png',
      CountryCode: '+234',
      Name: 'Nigeria'
  }

  _VInfo =
  {
      Email: "",
      MobileNumber: "",
      Message: ""
  }

  public _SLGen: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    autoWidth: true,
    autoHeight: false,
    pullDrag: true,
    dots: false,
    dotsEach: false,
    navSpeed: 700,
    navText: ['<i class="icofont-thin-left"></i>', '<i class="icofont-thin-right"></i>'],
    responsive: {
        0: {
            items: 1,
            autoWidth:true 
        },
        400: {
            items: 1
        },
        740: {
            items: 3
        },
        940: {
            items: 4
        }
    },
    nav: true
}
innerWidth: number = window.innerWidth;

  constructor(public _HelperService: HelperService,
    public _DataService: DataService, private router: Router ) { }


  tabChange(activeTab) {
    this.activeTab = activeTab;
}
  ngOnInit() {
    this._DataService._GetDeals_Promo_TopDeals()
  }

  navigateToRoute(route, TItem) {
    if (!this.isDragging && !TItem.IsSoldout) { this.router.navigate(route); }
  }

  on_carouselDrag(dragging: boolean) {
    setTimeout(() => {
        this.isDragging = dragging;
    }, 100)
  }

  CountrySelect(id) {
    if (id != 1) {
        this._SelectedCountryDetails =
        {
            Isd: '233',
            IconUrl: 'assets/images/flag/gh.png',
            CountryCode: '+233',
            Name: 'Ghana'
        }
    }
    else {
        this._SelectedCountryDetails =
        {
            Isd: '234',
            IconUrl: 'assets/images/flag/ng.png',
            CountryCode: '+234',
            Name: 'Nigeria'
        }
    }
  }

}
