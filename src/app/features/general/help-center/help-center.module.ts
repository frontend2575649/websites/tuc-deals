import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HelpCenterComponent } from './help-center.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { CarouselModule } from 'ngx-owl-carousel-o';

const routes: Routes = [
    { path: '', component: HelpCenterComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})



export class TUCHelpCenterModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TUCHelpCenterModule,
        CarouselModule,
        NgxSkeletonLoaderModule
    ],
    declarations: [HelpCenterComponent]
})
export class TUCRefundPolicyModule {
}
