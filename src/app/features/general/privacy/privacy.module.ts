import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TUCPrivacyComponent } from './privacy.component';

const routes: Routes = [
    { path: '', component: TUCPrivacyComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUPrivacyRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TUPrivacyRoutingModule,
    ],
    declarations: [TUCPrivacyComponent]
})
export class TUCPrivacyModule {
}
