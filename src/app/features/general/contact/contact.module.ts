import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TUCContactComponent } from './contact.component';

const routes: Routes = [
    { path: '', component: TUCContactComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUContactRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TUContactRoutingModule,
    ],
    declarations: [TUCContactComponent]
})
export class TUCContactModule {
}
