import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { CountdownModule } from 'ngx-countdown';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { SharedModule } from 'src/app/shared/shared.module';
import { TUCStoresComponent } from './stores.component';

const routes: Routes = [
    { path: '', component: TUCStoresComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUStoresRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TUStoresRoutingModule,
        ReactiveFormsModule,
        SharedModule,
        CountdownModule,
        NgxSliderModule,
        NgxSkeletonLoaderModule.forRoot({ theme: { 'margin-bottom': '0' } }),
    ],
    declarations: [TUCStoresComponent]
})

export class TUCStoresModule {
}
