import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { Options } from "@angular-slider/ngx-slider";


@Component({
    selector: 'tuc-stores',
    templateUrl: './stores.component.html',
    styleUrls: ['./stores.component.css']
})

export class TUCStoresComponent implements OnInit {
  
    constructor(
        public _HelperService: HelperService,
        public _DataService: DataService,
        public _Router: Router,
        private _activatedRoute: ActivatedRoute
    ) {

        
    }
    ngOnInit() { }

    cutString(data:string){
        if(data)
        {
            if(data.length >30)
            {
                return `${data.substring(0,30)}...`
            }
         }
        return data
    }
}