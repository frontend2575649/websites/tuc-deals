import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { Options } from "@angular-slider/ngx-slider";
import { ICategory, OResponse } from 'src/app/service/interface.service';
import { Observable } from 'rxjs';
import { PaginationService } from 'src/app/service/pagination.service';


@Component({
    selector: 'tuc-store',
    templateUrl: './store.component.html',
    styleUrls: ['./store.component.css']
})

export class TUCStoreComponent implements OnInit {
    showRating: boolean = false;
    minValue: number = 0;
    maxValue: number = 10000000;
    options: Options = {
        floor: 0,
        ceil: 10000000,
        hideLimitLabels: true
    };
    _fn: string;

    AllMasterCategory: any[] = [];
    _MerchantKey: string;
    _MerchantDetails: any;
    _SelectedMerchant: any;
    _SelectedCategory: any;
    _SubcategorySorted: any = [];
    loadingMerchantDetails: boolean = false;
    StoreReviews: any[] = [];
    TotalReviews: number = 0;
    AverageRatings: number = 0;
    public TotalRecords = 0;
    public Offset = 0;
    public activePage = 1;
    public pager: any = {};
    public Limit = 18;
    public _SubCategoriesSelected: any[] = [];
    constructor(
        public _HelperService: HelperService,
        public _DataService: DataService,
        public _Router: Router,
        private _activatedRoute: ActivatedRoute,
        private paged: PaginationService,
    ) {


    }
    ngOnInit() {
        this._DataService._GetDealsReset();
        this._HelperService.IsProcessing = true;
        this.loadingMerchantDetails = true;
        this._MerchantKey = this._activatedRoute.snapshot.params['ReferenceKey'];
        this._SelectedMerchant = this._DataService._MerchantsSorted.find((e: any) => e.ReferenceKey == this._MerchantKey);

        this._SelectedCategory = [{ ReferenceKey: this._SelectedMerchant.CategoryDistribution[0].CategoryKey }];
        this.GetMerchantDetails();
        this.GetStoreRatings();
        // get master categories
        var pData = {
            Task: 'getcategories',
            Host: this._HelperService.AppConfig.HostType,
            Offset: 0,
            Limit: 24,
            Type: this._DataService.ActiveCity,
            CountryId: this._DataService._CountrySelcted.ReferenceId,
            CountryKey: this._DataService._CountrySelcted.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.AllMasterCategory = _Response.Result.Data;
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }, () => this.generateSubCategory());
    }
    GetMerchantDetails() {
        this.loadingMerchantDetails = true;
        var pData = {
            Task: 'getmerchant',
            "ReferenceKey": this._SelectedMerchant.ReferenceKey,
            "ReferenceId": this._SelectedMerchant.ReferenceId,
            CountryId: this._DataService._CountrySelcted.ReferenceId,
            CountryKey: this._DataService._CountrySelcted.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._MerchantDetails = _Response.Result;
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }, () => this.loadingMerchantDetails = false);
    }
    GetStoreRatings() {
        this._HelperService.IsProcessing = true;
        var pData = {
            Task: 'getstoreratings',
            Host: 0,
            MerchantId: this._SelectedMerchant.ReferenceId,
            CountryId: this._DataService._CountrySelcted.ReferenceId,
            CountryKey: this._DataService._CountrySelcted.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.TotalReviews = _Response.Result?.TotalReviews;
                    this.AverageRatings = _Response.Result?.AverageRatings;
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    generateSubCategory() {
        this.resetCategoryChecked(this._SelectedCategory, this._SelectedMerchant);
        this._DataService._GetDealsReset();
        this._DataService._GetDeals();
        this._SelectedMerchant.SubCategoryDistribution.forEach((storeEle: any) => {
            let item = this.AllMasterCategory.find((catEle: any) => catEle.ReferenceKey == storeEle.CategoryKey)
            item && this._SubcategorySorted.push(item)
        });
    }
    _SubCategorySelected(Item, makeFalse?: boolean) {
        var SubCat = this._SubcategorySorted.find(x => x.ReferenceKey == Item.ReferenceKey);
        if (SubCat != undefined) {
            if (!makeFalse) {
                if (SubCat.IsChecked) {
                    SubCat.IsChecked = false;
                    var Items = this._DataService._SubCategoriesSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                    if (Items.length > 0) {
                        var ItemIndex = this._DataService._SubCategoriesSelected.indexOf(Items[0]);
                        this._DataService._SubCategoriesSelected.splice(ItemIndex, 1);
                    }
                }
                else {
                    var Items = this._DataService._SubCategoriesSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                    if (Items.length == 0) {
                        this._DataService._SubCategoriesSelected.push(Item);
                    }
                    SubCat.IsChecked = true;
                }
                this._DataService._GetDealsReset();
                this._DataService._GetDeals();
            } else {
                SubCat.IsChecked = false;
                var Items = this._DataService._SubCategoriesSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
                if (Items.length > 0) {
                    var ItemIndex = this._DataService._SubCategoriesSelected.indexOf(Items[0]);
                    this._DataService._SubCategoriesSelected.splice(ItemIndex, 1);
                }
            }
        }
    }

    ResetFilters() {
        this.resetCategoryChecked(this._SelectedCategory, this._SelectedMerchant);
        this._SearchByPriceClear();
        this._DataService._GetDeals();
    }

    navigateToRoute(route, TItem, event) {
        event.stopPropagation();
        if (!TItem.IsSoldout) { this._Router.navigate(route); }
    }


    _SortBy(Item) {
        this._DataService.DealsSortOrder = Item;
        this._DataService._GetDealsReset();
        this._DataService._GetDeals();
    }

    _Pricing =
        {
            MinPrice: 0,
            MaxPrice: 0,
        };

    _SearchByPrice() {
        this._DataService._GetDealsReset();
        this._DataService.MinPrice = this.minValue;
        this._DataService.MaxPrice = this.maxValue;
        this._DataService._GetDeals();
    }
    _SearchByPriceClear() {
        this._DataService._GetDealsReset();
        this._Pricing =
        {
            MinPrice: 0,
            MaxPrice: 0,
        };
        this._DataService.MinPrice = 0;
        this._DataService.MaxPrice = 0;
        this._DataService._GetDeals();
    }
    _PricingClick(Item) {
        this._DataService._GetDealsReset();
        this._Pricing.MinPrice = Item.MinPrice;
        this._Pricing.MaxPrice = Item.MaxPrice;
        this._DataService.MinPrice = Item.MinPrice;
        this._DataService.MaxPrice = Item.MaxPrice;
        this._DataService._GetDeals();
    }
    _Pricings = [
        {
            Title: '0 - 1000',
            MinPrice: 0,
            MaxPrice: 1000
        },
        {
            Title: '1000 - 5000',
            MinPrice: 1000,
            MaxPrice: 5000
        },
        {
            Title: '5000 - 10000',
            MinPrice: 5000,
            MaxPrice: 10000
        },
        {
            Title: '10000 - 20000',
            MinPrice: 10000,
            MaxPrice: 20000
        },
        {
            Title: '20000 - 50000',
            MinPrice: 20000,
            MaxPrice: 50000
        },
        {
            Title: '50000 + ',
            MinPrice: 50000,
            MaxPrice: 0
        }

    ]
    resetCategoryChecked(_CategoriesSelected?, _MerchantSelected?) {
        this._DataService._GetDealsReset();
        // this._DataService._CategoriesSelected = _CategoriesSelected;
        this._DataService._SubCategoriesSelected = [];
        // this._DataService._MerchantsSorted = this._DataService._OriginalMerchants;
        // this._DataService._Merchants = this._DataService._OriginalMerchants;
        // this._DataService?._CategoriesSorted.forEach(ele => delete ele.IsChecked);
        this._SubcategorySorted.forEach(ele => delete ele.IsChecked);
        // this._DataService._MerchantsSorted.forEach(ele => delete ele.IsChecked);
        this._DataService._MerchantSelected = [_MerchantSelected];
        // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Categories, this._DataService._CategoriesSorted);
    }


    userChange(data) {
        this.minValue = data.value
        this.maxValue = data.highValue
    }

    getStoreReviews() {
        this.showRating = true;
        this._HelperService.IsProcessing = true;
        var pData = {
            Task: 'getstoredata',
            Host: this._HelperService.AppConfig.HostType,
            Offset: (this.activePage - 1) * this.Limit,
            Limit: this.Limit,
            MerchantKey: this._SelectedMerchant.ReferenceId,
            CountryId: this._DataService._CountrySelcted.ReferenceId,
            CountryKey: this._DataService._CountrySelcted.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.StoreReviews = _Response.Result.Data;
                    this.Offset = this.Offset - 1;
                    this.TotalRecords = _Response.Result.TotalRecords;
                    this.pager = this.paged.getPager(this.TotalRecords, this.activePage);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    setPage(page: number) {
        this.activePage = page;
        if(page < 1 || page > this.pager.totalPages){
            return
        }
        this.pager = this.paged.getPager(this.TotalRecords, page)
        this.getStoreReviews();
    }

    calculatePercentageOff(originalValue, discountedValue) {
        // Calculate the difference between the original value and discounted value
        let difference = originalValue - discountedValue;      
        // Calculate the percentage off
        let percentageOff = (difference / originalValue) * 100;
        // Return the percentage off as a positive number
        return Math.ceil(percentageOff);
      }
      
}