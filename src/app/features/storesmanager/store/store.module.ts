import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { CountdownModule } from 'ngx-countdown';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { SharedModule } from 'src/app/shared/shared.module';
import { TUCStoreComponent } from './store.component';
import { ShareButtonModule } from 'ngx-sharebuttons/button';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
const routes: Routes = [
    { path: '', component: TUCStoreComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUStoreRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TUStoreRoutingModule,
        ReactiveFormsModule,
        SharedModule,
        CountdownModule,
        NgxSliderModule,
        NgxSkeletonLoaderModule.forRoot({ theme: { 'margin-bottom': '0' } }),
        ShareButtonModule,
        ShareIconsModule,
    ],
    declarations: [TUCStoreComponent]
})

export class TUCStoreModule {
}
