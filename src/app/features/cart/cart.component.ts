import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { ICategory, OResponse } from 'src/app/service/interface.service';
import { SwiperOptions } from 'swiper';
import SwiperCore, { EffectCards } from "swiper";
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'tuc-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.css']
})
export class TUCCartComponent implements OnInit {
    CartTotal: number = 0;
    SubTotal: number = 0;
    Discount: number = 0;
    shippingAddressForm: FormGroup;
    isSubmittedForm: boolean = false;
    constructor(
        public _HelperService: HelperService,
        public _DataService: DataService,
        private _NgbModal: NgbModal,
        private fb: FormBuilder,
        private router: Router
    ) {
    }

    ngOnInit(): void {
        this._DataService.CustomerCart = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.CustomerCart);
        console.log(JSON.stringify(this._DataService.CustomerCart));
        if (this._DataService.CustomerCart) {
            this.CartTotal = this._DataService.CustomerCart.reduce((a, ele: any) => {
                return a + ele.ActualPrice
            }, 0)
            this.SubTotal = this._DataService.CustomerCart.reduce((a, ele: any) => {
                return a + ele.SellingPrice
            }, 0)
            this.Discount = this._DataService.CustomerCart.reduce((a, ele: any) => {
                return a + (ele.ActualPrice - ele.SellingPrice)
            }, 0)

        }
        this.initShippingAddressForm();
    }
    decQuantityCount(Item, index) {
        if (this._DataService.CustomerCart[index].Quantity > 1) {
            this._DataService.CustomerCart[index].Quantity = this._DataService.CustomerCart[index].Quantity - 1;
            this.updateCart();
        }
    }
    incQuantityCount(Item, index) {
        this._DataService.CustomerCart[index].Quantity = this._DataService.CustomerCart[index].Quantity + 1;
        this.updateCart();
    }

    updateCart() {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.CustomerCart, this._DataService.CustomerCart);
    }

    removeItem(TItem, index) {
        let _cart = this._DataService.CustomerCart.findIndex(ele => ele.ReferenceKey == TItem.ReferenceKey);
        if (_cart > -1) {
            this._DataService.CustomerCart.splice(_cart, 1)
        }
        this.updateCart();
    }

    shippingAddress(content) {
        this._NgbModal.open(content, { size: 'md', modalDialogClass: 'tuc-shipping-address-modal-body' });
    }
    changeSelectFormControl(control, event) {
        console.log(`[${control}]`, event)
    }
    initShippingAddressForm(): void {
        this.shippingAddressForm = this.fb.group({
            FirstName: ['', Validators.required],
            LastName: ['', Validators.required],
            City: ['', Validators.required],
            State: ['', Validators.required],
            Address: ['', Validators.required],
            PhoneNumber: this.fb.group({
                Code: ['+234', Validators.required],
                Number: ['', Validators.required]
            }),
            AdditionalNumber: this.fb.group({
                Code: ['+234'],
                Number: ['']
            }),
            // saveAddress: [false, Validators.required]
        })
    }

    saveForm() {
        this.isSubmittedForm = true;
        console.log(this.shippingAddressForm)
    }
    get f(): { [key: string]: AbstractControl } {
        return this.shippingAddressForm.controls;
    }

}