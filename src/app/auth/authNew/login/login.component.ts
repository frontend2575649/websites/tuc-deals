import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common'
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { OResponse } from 'src/app/service/interface.service';
import { MixpanelService } from 'src/app/service/mixpanel.service';

declare var $:any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../auth.component.css']
})
export class LoginComponent implements OnInit {

  _SelectedCountryDetails =
  {
      Isd: '234',
      IconUrl: 'assets/images/flag/ng.png',
      CountryCode: '+234',
      Name: 'Nigeria'
  }
  _VInfo =
  {
      MobileNumber: "",
      PIN: ""
  }
  constructor(
    public _HelperService: HelperService,
    public _DataService: DataService,
    private _MixpanelService: MixpanelService,
    private _NgbModal: NgbModal,
    private location: Location,
    private router:Router
  ) { }
  
  ngOnInit(): void {
   this.isLoggedIn() ? this.router.navigate(['/home']) : null; 
  }

  isLoggedIn():boolean
  {
    return this._HelperService.IsActiveUser
  }

  CountrySelect(id) {
    if (id != 1) {
        this._SelectedCountryDetails =
        {
            Isd: '233',
            IconUrl: 'assets/images/flag/gh.png',
            CountryCode: '+233',
            Name: 'Ghana'
        }
    }
    else {
        this._SelectedCountryDetails =
        {
            Isd: '234',
            IconUrl: 'assets/images/flag/ng.png',
            CountryCode: '+234',
            Name: 'Nigeria'
        }
    }
  }
    _vCode = "";
    onOtpChange(event) {
        this._vCode = event;
    }

    Login() {
        this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.LoginInitiated, {
            MobileNumber:  this._VInfo.MobileNumber.substring(0, 1) !== "0" ? "0".concat(this._VInfo.MobileNumber) : this._VInfo.MobileNumber,
        });
        if (this._VInfo.MobileNumber == undefined || this._VInfo.MobileNumber == null || this._VInfo.MobileNumber == "") {
            this._HelperService.NotifyError("Enter mobile number");
        }
        else if (this._VInfo.MobileNumber.length < 10) {
             this._HelperService.NotifyError("Enter valid mobile number");
        }
        else if (this._VInfo.MobileNumber.length > 13) {
            this._HelperService.NotifyError("Enter valid mobile number");
        }
        else if (this._VInfo.PIN.length > 4) {
            this._HelperService.NotifyError("Enter valid PIN");
        }
        else {
            this._HelperService.IsProcessing = true;
            var pData = {
                Task: 'userlogin',
                Host: this._HelperService.AppConfig.HostType,
                CountryIsd: this._SelectedCountryDetails.Isd,
                MobileNumber:  this._VInfo.MobileNumber.substring(0, 1) !== "0" ? "0".concat(this._VInfo.MobileNumber) : this._VInfo.MobileNumber,
                AccessPin: this._VInfo.PIN
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.UserActivity, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.LoginSuccessful, {
                            MobileNumber:  this._VInfo.MobileNumber.substring(0, 1) !== "0" ? "0".concat(this._VInfo.MobileNumber) : this._VInfo.MobileNumber,
                        });
                        var _Res = _Response.Result;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Acc, _Response.Result);
                        this._DataService.Profile = _Response.Result;
                        this._HelperService.RefreshHelper();
                        this._MixpanelService.identify(pData.MobileNumber);
                        this._NgbModal.dismissAll();
                        this._DataService._GetBalannce();
                        this._DataService._GetBookmarks();
                        let isResetFlow = localStorage.getItem('resetflow');
                        if(isResetFlow == 'true'){
                            localStorage.removeItem('resetflow');
                            this.router.navigate(['/home']);
                        }else{
                            this._HelperService.NotifySuccess(_Response.Message);
                            this.location.back()
                        }
                    }
                    else {
                        this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.LoginFailed, {
                            MobileNumber:  this._VInfo.MobileNumber.substring(0, 1) !== "0" ? "0".concat(this._VInfo.MobileNumber) : this._VInfo.MobileNumber,
                        });
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.LoginFailed, {
                        MobileNumber:  this._VInfo.MobileNumber.substring(0, 1) !== "0" ? "0".concat(this._VInfo.MobileNumber) : this._VInfo.MobileNumber,
                    });
                    this._HelperService.HandleException(_Error);
                });
        }
    }

    togglePin(data:string)
    {
        $('#eye-icon').toggleClass('fa-eye-slash')
        let word = $('#'+data).attr("type")
        if(word == "password")
        {
          $("#"+data).attr("type","text");
        }
        else{
          $("#"+data).attr("type","password");
        }
    }
}
