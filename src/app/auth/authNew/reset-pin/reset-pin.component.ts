import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common'
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { OResponse } from 'src/app/service/interface.service';
import { MixpanelService } from 'src/app/service/mixpanel.service';

declare var $: any;

@Component({
    selector: 'app-reset-pin',
    templateUrl: './reset-pin.component.html',
    styleUrls: ['../auth.component.css']
})
export class ResetPinComponent implements OnInit {

    steps: number = 1;
    _SelectedCountryDetails =
        {
            Isd: '234',
            IconUrl: 'assets/images/flag/ng.png',
            CountryCode: '+234',
            Name: 'Nigeria'
        }
    _VInfo =
        {
            MobileNumber: "",
            PIN: ""
        }
    // change pin
    ShowPassword1: boolean = true;
    ShowPassword2: boolean = true;
    ShowPassword3: boolean = true;
    password: any = {
        temporaryPin: { value: null, label: 'temporary' },
        newPin: { value: null, label: 'new' },
        confirmPin: { value: null, label: 'confirm' }
    }
    constructor(
        public _HelperService: HelperService,
        public _DataService: DataService,
        private _MixpanelService: MixpanelService,
        private _NgbModal: NgbModal,
        private location: Location,
        private router: Router
    ) { }

    ngOnInit(): void {
    }


    isLoggedIn(): boolean {
        return this._HelperService.IsActiveUser
    }

    CountrySelect(id) {
        if (id != 1) {
            this._SelectedCountryDetails =
            {
                Isd: '233',
                IconUrl: 'assets/images/flag/gh.png',
                CountryCode: '+233',
                Name: 'Ghana'
            }
        }
        else {
            this._SelectedCountryDetails =
            {
                Isd: '234',
                IconUrl: 'assets/images/flag/ng.png',
                CountryCode: '+234',
                Name: 'Nigeria'
            }
        }
    }
    _vCode = "";
    onOtpChange(event) {
        this._vCode = event;
    }
    RequestPin() {
        if (this._VInfo.MobileNumber == undefined || this._VInfo.MobileNumber == null || this._VInfo.MobileNumber == "") {
            this._HelperService.NotifyError("Enter mobile number");
        } else if (this._VInfo.MobileNumber.length < 11) {
            this._HelperService.NotifyError("Enter 11 digit mobile number");
        } else {
            this._HelperService.IsProcessing = true;
            var pData = {
                Task: 'resetpin',
                CountryIsd: this._SelectedCountryDetails.Isd,
                MobileNumber: this._VInfo.MobileNumber,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.UserActivity, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        var _Res = _Response.Result;
                        this.steps = 2;
                        setTimeout(() => {
                            this.steps = 3;
                        }, 5000);
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
    }

    ResetPin() {
        for (let control in this.password) {
            if (!this.password[control].value) {
                return this._HelperService.NotifyError(`Enter ${this.password[control].label} pin`);
            } else if (this.password[control].value.length < 4) {
                let cntr = this.password[control].label.charAt(0).toUpperCase() + this.password[control].label.slice(1);
                return this._HelperService.NotifyError(`${cntr} pin should be 4 digits`);
            }
        }
        if (this.password.newPin.value !== this.password.confirmPin.value) {
            this._HelperService.NotifyError("Pin mismatch, new pin and confirm pin should be same");
        }
        else {
            this._HelperService.IsProcessing = true;
            var pData = {
                Task: 'updatepin',
                CountryIsd: this._SelectedCountryDetails.Isd,
                MobileNumber: this._VInfo.MobileNumber,
                TempAccessPin: this.password.temporaryPin.value,
                NewAccessPin: this.password.newPin.value
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.UserActivity, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        var _Res = _Response.Result;
                        this._HelperService.NotifySuccess(_Response.Message, 5);
                        window.localStorage.setItem('resetflow', 'true');
                        this.router.navigate(['/auth/login']);
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
    }

    togglePin(data: string) {
        $(`#${data}`).parent().find('#eye-icon').toggleClass('fa-eye-slash');
        let word = $('#' + data).attr("type");
        if (word == "password") {
            $("#" + data).attr("type", "text");
        }
        else {
            $("#" + data).attr("type", "password");
        }
    }
}
