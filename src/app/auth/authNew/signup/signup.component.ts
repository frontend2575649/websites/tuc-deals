import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common'
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { IDeal, OResponse } from 'src/app/service/interface.service';
import { MixpanelService } from 'src/app/service/mixpanel.service';
import * as _ from 'underscore';
declare var $:any;

@Component({
    selector: 'tuc-signup',
    templateUrl: './signup.component.html',
    styleUrls:['../auth.component.css']
})
export class SignUpComponent implements OnInit {

    _Profile =
        {
            FirstName: null,
            LastName: null,
            EmailAddress: null,
            Gender: null,
            Day: null,
            Month: null,
            Year: null
        }
countDown:number = 30;
  _vStage =
  {
      level: 1,
  }
  _SelectedCountryDetails =
  {
      Isd: '234',
      IconUrl: 'assets/images/flag/ng.png',
      CountryCode: '+234',
      Name: 'Nigeria'
  }
  _VInfo =
  {
      MobileNumber: "",
      EmailAddress:"",
      AccountPin:"",
      Name:"",
      ReferredBy:""
  }
  _VerificationD =
  {
      ReferenceId: null,
      MobileNumber: null,
      CountryIsd: null,
      RequestToken: null,
      IsNewAccount: null,
  }

  constructor(
    public _HelperService: HelperService,
    public _DataService: DataService,
    private _MixpanelService: MixpanelService,
    private _NgbModal: NgbModal,
    private location: Location,
    private router:Router,
    private activateRoute:ActivatedRoute,
  ) { }
  
  ngOnInit(): void {
    this.isLoggedIn() ? this.router.navigate(['/home']) : null; 
    this.activateRoute.queryParams.subscribe((params:any) => {
        this._VInfo.ReferredBy = params['referral-code'];
    })
  }

  CountrySelect(id) {
    if (id != 1) {
        this._SelectedCountryDetails =
        {
            Isd: '233',
            IconUrl: 'assets/images/flag/gh.png',
            CountryCode: '+233',
            Name: 'Ghana'
        }
    }
    else {
        this._SelectedCountryDetails =
        {
            Isd: '234',
            IconUrl: 'assets/images/flag/ng.png',
            CountryCode: '+234',
            Name: 'Nigeria'
        }
    }
  }
    SetGender(gender) {
      this._Profile.Gender = gender;
    }
    ChangeNumber() {
      this._vStage.level = 1;
      this._VInfo.MobileNumber = "";
    }

    ValidateOtp() {
        if (this._vCode == undefined || this._vCode == null || this._vCode == "") {
            this._HelperService.NotifyError("Enter verification code");
        }
        else if (this._vCode.length != 4) {
            this._HelperService.NotifyError("Enter valid verification code");
        }
        else {
            this._HelperService.IsProcessing = true;
            var pData = {
                Task: 'userverification',
                Host: this._HelperService.AppConfig.HostType,
                CountryIsd: this._SelectedCountryDetails.Isd,
                MobileNumber: this._VInfo.MobileNumber,
                RequestToken: this._VerificationD.RequestToken,
                AccessCode: this._vCode
            };
            this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.PinVerficationInitiated, _.pick(pData, 'CountryIsd', 'MobileNumber'));
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.UserActivity, pData);
            _OResponse.subscribe(
                async(_Response) => {
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.PinVerficationPassed, _.pick(pData, 'CountryIsd', 'MobileNumber'));
                        var _Res = _Response.Result;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Acc, _Response.Result);
                        this._DataService.Profile = _Response.Result;
                        this._HelperService.RefreshHelper();
                        await this._MixpanelService.alias(this._VInfo.MobileNumber);
                        await this._MixpanelService.setProfile({
                            $host: this._HelperService.AppConfig.HostType,
                            $first_name: this._VInfo.Name.split(' ')[0],
                            $last_name: this._VInfo.Name.split(' ').length > 1 ? this._VInfo.Name.split(' ')[1]: '',
                            $email: this._VInfo.EmailAddress,
                            $mobileNumber: this._VInfo.MobileNumber,
                            $countryIsd: this._SelectedCountryDetails.Isd,
                        });
                        await this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.Registration, {
                            $mobileNumber: this._VInfo.MobileNumber,
                        })
                        await this._MixpanelService.identify(this._VInfo.MobileNumber)
                        this._NgbModal.dismissAll();
                        this.router.navigate(['/home'])
                    }
                    else {
                        this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.PinVerficationFailed, _.pick(pData, 'CountryIsd', 'MobileNumber'));
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.PinVerficationFailed, _.pick(pData, 'CountryIsd', 'MobileNumber'));
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    _vCode = "";
    onOtpChange(event) {
        this._vCode = event;
    }

    SignUp() {
        if (this._VInfo.MobileNumber == undefined || this._VInfo.MobileNumber == null || this._VInfo.MobileNumber == "") {
            this._HelperService.NotifyError("Enter mobile number");
        }
        else if (this._VInfo.MobileNumber.length < 11) {
            this._HelperService.NotifyError("Enter valid mobile number");
        }
        else if (this._VInfo.MobileNumber.length > 13) {
            this._HelperService.NotifyError("Enter valid mobile number");
        }else if(!(this.validateEmail(this._VInfo.EmailAddress)))
        {
            this._HelperService.NotifyError("Enter valid email address");
        }else if(this._VInfo.AccountPin.length < 4){
            this._HelperService.NotifyError("Enter 4 digit account pin");
        }
        else if (this._VInfo.ReferredBy && this._VInfo.ReferredBy.length < 6) {
            this._HelperService.NotifyError("Referral ID should be at least 6 charaters in length");
        }
        else {
            this._HelperService.IsProcessing = true;
            var pData = {
                Task: 'userregistration',
                Host: this._HelperService.AppConfig.HostType,
                CountryIsd: this._SelectedCountryDetails.Isd,
                MobileNumber: this._VInfo.MobileNumber,
                AccountPin:this._VInfo.AccountPin,
                EmailAddress:this._VInfo.EmailAddress.toLowerCase(),
                Name:this._VInfo.Name,
                ReferredBy: this._VInfo.ReferredBy
            };
            this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.RegistrationInitiated, _.omit(pData, 'AccountPin', 'Task'));
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.UserActivity, pData);
            _OResponse.subscribe(
                _Response => {
                    this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.RegisteredSuccessfully, _.omit(pData, 'AccountPin', 'Task'));
                    this._HelperService.IsProcessing = false;
                    if ((_Response.Status == this._HelperService.StatusSuccess) ||
                        (_Response.Message.includes('User already exists') && !_Response.Result.IsMobileNumberVerified)) 
                        {
                            this._HelperService.NotifyInfo(_Response.Message)
                            this._vStage.level = 2;
                            this._VerificationD = _Response.Result;
                            this._HelperService.SaveStorage("auth", _Response.Result);
                            this.startCountDown()
                        }
                    else  if(_Response.Message.includes('User already exists') && _Response.Result.IsMobileNumberVerified){
                            this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.RegistrationUserAlreadyExist, _.omit(pData, 'AccountPin', 'Task'));
                            this.router.navigate(['/auth/login'])
                            this._HelperService.NotifyInfo(_Response.Message)
                    }else{
                        this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.RegistrationFailed, _.omit(pData, 'AccountPin', 'Task'));
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.RegistrationFailed, _.omit(pData, 'AccountPin', 'Task'));
                    this._HelperService.HandleException(_Error);
                });
        }
    }

    togglePin(data:string)
    {
        $('#eye-icon').toggleClass('fa-eye-slash')
        let word = $('#'+data).attr("type")
        if(word == "password")
        {
          $("#"+data).attr("type","text");
        }
        else{
          $("#"+data).attr("type","password");
        }
    }

    startCountDown()
    {
        setInterval(
        () =>{
            if(this.countDown > 0){
                this.countDown = this.countDown-1 
            }
           },1000)
    }

    resendOtp()
    {
        this._HelperService.IsProcessing = true;
            var pData = {
                Task: 'resendpin',
                Host: this._HelperService.AppConfig.HostType,
                CountryIsd: this._SelectedCountryDetails.Isd,
                MobileNumber: this._VInfo.MobileNumber,
                RequestToken:this._VInfo.Name
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.UserActivity, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess(_Response.Message)
                        this.countDown = 30;
                        this.startCountDown()
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
    }

     validateEmail = (email) => {
        return email.match(
          /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
     };

  isLoggedIn():boolean
  {
    return this._HelperService.IsActiveUser
  }
}
