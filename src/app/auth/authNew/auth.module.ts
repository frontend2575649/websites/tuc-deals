import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { CountdownModule } from 'ngx-countdown';
import { SharedModule } from 'src/app/shared/shared.module';
import { SignUpComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { NgOtpInputModule } from 'ng-otp-input';
import { ResetPinComponent } from './reset-pin/reset-pin.component';

const routes: Routes = [
    { path: '', redirectTo:'login', pathMatch:'full' },
    {path: 'login', component:LoginComponent},
    { path: 'signup', component:SignUpComponent},
    { path:'reset-pin', component: ResetPinComponent}
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        CountdownModule,
        NgbModule,
        SharedModule,
        NgOtpInputModule
    ],
    declarations: [LoginComponent,SignUpComponent, ResetPinComponent]
})
export class AuthModule {
}

