import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TUCAccessComponent } from "./access.component";



const routes: Routes = [
    {
        path: "",
        component: TUCAccessComponent,
        children: [
            { path: '', loadChildren: () => import('../../features/home/home.module').then(m => m.TUCHomeModule) },
            { path: 'home', loadChildren: () => import('../../features/home/home.module').then(m => m.TUCHomeModule) },
            { path: 'category/:name', loadChildren: () => import('../../features/dealsmanager/categeory/categeory.module').then(m => m.TUCcategeoryModule) },
            { path: 'category/:name/subcategory/:subcategory', loadChildren: () => import('../../features/dealsmanager/categeory/categeory.module').then(m => m.TUCcategeoryModule) },
            { path: 'buydeal/:slug', loadChildren: () => import('../../features/dealsmanager/dealpayment/dealpayment.module').then(m => m.TUCDealPaymentModule) },
            { path: 'dealcode/:referenceid/:referencekey', loadChildren: () => import('../../features/dealsmanager/dealcode/dealcode.module').then(m => m.TUCDealCodeModule) },
            { path: 'deals', loadChildren: () => import('../../features/dealsmanager/deals/deals.module').then(m => m.TUCDealsModule) },
            { path: 'profile', loadChildren: () => import('../../features/account/profile/profile.module').then(m => m.TUCProfileModule) },
            { path: 'track-order', loadComponent: () => import('../../features/dealsmanager/order-tracking/order-tracking.component').then(m => m.TUCOrderTrackingComponent) },
            { path: 'stores', loadChildren: () => import('../../features/storesmanager/stores/stores.module').then(m => m.TUCStoresModule) },
            { path: 'stores/:ReferenceKey', loadChildren: () => import('../../features/storesmanager/store/store.module').then(m => m.TUCStoreModule) },
            { path: 'terms', loadChildren: () => import('../../features/general/terms/terms.module').then(m => m.TUCTermsModule) },
            { path: 'refundpolicy', loadChildren: () => import('../../features/general/refundpolicy/refundpolicy.module').then(m => m.TUCRefundPolicyModule) },
            { path: 'help-center', loadChildren: () => import('../../features/general/help-center/help-center.module').then(m => m.TUCHelpCenterModule) },


            { path: "faq", loadChildren: () => import('../../features/general/faq/faq.module').then(m => m.TUCFaqModule) },
            { path: "privacy", loadChildren: () => import('../../features/general/privacy/privacy.module').then(m => m.TUCPrivacyModule) },
            { path: "aboutus", loadChildren: () => import('../../features/general/aboutus/aboutus.module').then(m => m.TUCAboutusModule) },
            { path: "stats", loadChildren: () => import('../../features/general/stats/stats.module').then(m => m.TUCStatsModule) },
            { path: "contact", loadChildren: () => import('../../features/general/contact/contact.module').then(m => m.TUCContactModule) },
            { path: "notfound", loadChildren: () => import('../../features/general/pagenotfound/pagenotfound.module').then(m => m.TUCPagenotfoundModule) },
            { path: "comingsoon/:country", loadChildren: () => import('../../features/general/comingsoon/comingsoon.module').then(m => m.TUCComingSoonModule) },
            { path: "**", loadChildren: () => import('../../features/general/pagenotfound/pagenotfound.module').then(m => m.TUCPagenotfoundModule) },


        ]
    }
];



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class TUCAccessRoutingModule { }
