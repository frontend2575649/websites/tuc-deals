import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TUCAccessComponent } from './access.component';
import { TUCAccessRoutingModule } from './access.routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TUCPageFooter } from '../../component/footer/footer.component';
import { TUCHeaderHeader } from '../../component/header/header.component';
import { NgOtpInputModule } from 'ng-otp-input';
import { FilterCityComponent } from 'src/app/component/filter-city/filter-city.component';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import { TopNavSearchComponent } from 'src/app/component/top-nav-search/top-nav-search.component';
import { RouterModule } from '@angular/router';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@NgModule({
    declarations: [
        TUCAccessComponent,
        TUCPageFooter,
        TUCHeaderHeader,
        FilterCityComponent,
        TopNavSearchComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TUCAccessRoutingModule,
        NgOtpInputModule,
        ShareButtonsModule,
        ShareIconsModule,
        RouterModule,
        NgxSkeletonLoaderModule
    ],
    providers: [],
    bootstrap: [TUCAccessComponent]
})
export class TUCAccessModule { }
