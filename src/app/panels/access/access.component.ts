import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { NavigationStart, Router } from '@angular/router';


declare var $:any;
@Component({
    selector: 'tuc-access',
    templateUrl: './access.component.html',
    styleUrls:['./access.component.css']
})

export class TUCAccessComponent implements OnInit {
    showCategorySection:boolean = false;
    isDragging: boolean = false;
    routeName:string='';
    showCategorySec: boolean;
    constructor(
        public _DataService: DataService,
        private router: Router
    ) {
        this.routeName = this.router.url
    }

    ngOnInit() {
        this._DataService.$showCategorySectionScource.subscribe(data=> this.showCategorySection = data)
       this.checkPage()
    }
    isHome(data):boolean
    {
        return data.includes('home') ? true: false;
    }

    checkPage()
    {   
        this.router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                this.routeName = event.url
            }
        })
    }

    navigateToRoute(route, TItem) {
        this.showCategorySection = false
        if (!this.isDragging && !TItem.IsSoldout) { this.router.navigate(route); }
    }
    convertRouteName(Name: string): string {
        return Name.toLowerCase().split("-").join("").split(" ").join("");
    }
}