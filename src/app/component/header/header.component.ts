import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { IDeal, OResponse } from 'src/app/service/interface.service';
import { MixpanelService } from 'src/app/service/mixpanel.service';
import { cities } from '../filter-city/city.mock';

declare var $:any;
declare var document:any;
@Component({
    selector: 'tuc-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class TUCHeaderHeader implements OnInit {
    
    showCategorySec:boolean = false;
    public isMenuCollapsed = true;
    _Profile =
        {
            FirstName: null,
            LastName: null,
            EmailAddress: null,
            Gender: null,
            Day: null,
            Month: null,
            Year: null
        }

    _vStage =
        {
            level: 1,
        };

    _VInfo =
        {
            MobileNumber: "",
        }
    _SelectedCountryDetails =
        {
            Isd: '234',
            IconUrl: 'assets/images/flag/ng.png',
            CountryCode: '+234',
            Name: 'Nigeria'
        }
    dealDayLogoImgUrl: string = '';
    dealDayLogoImgUrlLoading: boolean = false;
    constructor(private _NgbModal: NgbModal,
        public _DataService: DataService,
        public _HelperService: HelperService,
        private _MixpanelService: MixpanelService,
        public router: Router, private activatedRoute: ActivatedRoute) { 
        }
    @HostListener('window:scroll', ['$event']) onWindowScroll(e) {
        let scrollPosition = e.target['scrollingElement'].scrollTop;
        this._DataService.enableCatDropdown = ((this.router.url == '/home' || this.router.url == '/') && scrollPosition < 500) ? false : true;
    }

    ngOnInit() {
        this.dealDayLogoImgUrlLoading = true;
        var pData: any = {
            Task: 'getapplogo',
            Panel: 'WebConnect',
            "ImageContent": {
                "Reference": "",
                "Name": "Logo",
                "Extension": "png",
                "Tags": ""
            }
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
        _OResponse.subscribe(
            _Response => {
                this.dealDayLogoImgUrlLoading = false;
                if(_Response.Status == this._HelperService.StatusSuccess){
                    this.dealDayLogoImgUrl = _Response.Result.ImageUrl
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }, () => this.dealDayLogoImgUrlLoading = false);
    }
    navigateToRoute(route) {
        window.location.href = route;
    }
    convertRouteName(Name: string): string {
        return Name.toLowerCase().split("-").join("").split(" ").join("");
    }
    isHome(data:string): boolean {
        return data.includes('home') ? true: false;
    }

    RefreshBalance() {
        if (this._HelperService.IsActiveUser) {
            this._DataService._GetBalannce();
            // this.router.navigate(['profile']);
        }
    }
    confirmLogout(content) {
        this._NgbModal.open(content, { centered: true , modalDialogClass: 'tuc-confirm-logout-body' });
    }
    OpenLogin(content) {
        this.router.navigate(['/auth/login'])
       // this._NgbModal.open(content, { size: 'lg', modalDialogClass: 'tuc-pp-login' });
    }
    CountrySelect(id) {
        if (id != 1) {
            this._SelectedCountryDetails =
            {
                Isd: '233',
                IconUrl: 'assets/images/flag/gh.png',
                CountryCode: '+233',
                Name: 'Ghana'
            }
        }
        else {
            this._SelectedCountryDetails =
            {
                Isd: '234',
                IconUrl: 'assets/images/flag/ng.png',
                CountryCode: '+234',
                Name: 'Nigeria'
            }
        }
    }
    SetGender(gender) {
        this._Profile.Gender = gender;
    }
    ChangeNumber() {
        this._vStage.level = 1;
        this._VInfo.MobileNumber = "";
    }

    Logout() {
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageItem.Acc);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageItem.Profile);
        this._HelperService.DeleteStorage('accbal');
        this._HelperService.DeleteStorage('auth');
        this._HelperService.RefreshHelper();
        window.location.href = "home";
    }
    _VerificationD =
        {
            ReferenceId: null,
            MobileNumber: null,
            CountryIsd: null,
            RequestToken: null,
            IsNewAccount: null,
        }
    ValidateMobile() {
        if (this._VInfo.MobileNumber == undefined || this._VInfo.MobileNumber == null || this._VInfo.MobileNumber == "") {
            this._HelperService.NotifyError("Enter mobile number");
        }
        else if (this._VInfo.MobileNumber.length < 9) {
            this._HelperService.NotifyError("Enter valid mobile number");
        }
        else if (this._VInfo.MobileNumber.length > 13) {
            this._HelperService.NotifyError("Enter valid mobile number");
        }
        else {
            this._HelperService.IsProcessing = true;
            var pData = {
                Task: 'requestotp',
                Host: this._HelperService.AppConfig.HostType,
                CountryIsd: this._SelectedCountryDetails.Isd,
                MobileNumber: this._VInfo.MobileNumber,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._vStage.level = 2;
                        this._VerificationD = _Response.Result;
                        this._HelperService.SaveStorage("auth", _Response.Result);
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    ValidateOtp() {
        if (this._vCode == undefined || this._vCode == null || this._vCode == "") {
            this._HelperService.NotifyError("Enter verification code");
        }
        else if (this._vCode.length != 4) {
            this._HelperService.NotifyError("Enter valid verification code");
        }
        else {
            this._HelperService.IsProcessing = true;
            var pData = {
                Task: 'verifyotp',
                Host: this._HelperService.AppConfig.HostType,
                CountryIsd: this._SelectedCountryDetails.Isd,
                MobileNumber: this._VInfo.MobileNumber,
                RequestToken: this._VerificationD.RequestToken,
                AccessCode: this._vCode
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        var _Res = _Response.Result;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Acc, _Response.Result);
                        this._DataService.Profile = _Response.Result;
                        this._HelperService.RefreshHelper();
                        if (_Res.IsNewAccount == true) {
                            this._vStage.level = 3;
                        }
                        else {
                            this._HelperService.RefreshHelper();
                            this._MixpanelService.identify(pData.MobileNumber);
                            this._NgbModal.dismissAll();
                        }
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    SetProfile() {
        if (this._Profile.FirstName == undefined || this._Profile.FirstName == null || this._Profile.FirstName == "") {
            this._HelperService.NotifyError("Enter first name");
        }
        else if (this._Profile.LastName == undefined || this._Profile.LastName == null || this._Profile.LastName == "") {
            this._HelperService.NotifyError("Enter last name");
        }
        else if (this._Profile.EmailAddress == undefined || this._Profile.EmailAddress == null || this._Profile.EmailAddress == "") {
            this._HelperService.NotifyError("Enter email address");
        }
        else if (this._Profile.Gender == undefined || this._Profile.Gender == null || this._Profile.Gender == "") {
            this._HelperService.NotifyError("Select gender");
        }
        else if (this._Profile.Day == undefined || this._Profile.Day == null || this._Profile.Day == "") {
            this._HelperService.NotifyError("Select birth day");
        }
        else if (this._Profile.Month == undefined || this._Profile.Month == null || this._Profile.Month == "") {
            this._HelperService.NotifyError("Select birth month");
        }
        else if (this._Profile.Year == undefined || this._Profile.Year == null || this._Profile.Year == "") {
            this._HelperService.NotifyError("Select birth year");
        }
        else {
            this._HelperService.IsProcessing = true;
            var pData = {
                Task: 'updateaccount',
                Host: this._HelperService.AppConfig.HostType,
                FirstName: this._Profile.FirstName,
                LastName: this._Profile.LastName,
                EmailAddress: this._Profile.EmailAddress,
                Gender: this._Profile.Gender,
                DateOfBirth: new Date(this._Profile.Year, this._Profile.Month, (+this._Profile.Day+1)),
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
            _OResponse.subscribe(
                async (_Response) => {
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        // var _Res = _Response.Result;
                        // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Acc, _Response.Result);
                        let accProfile: any = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Acc);
                        accProfile.DisplayName = this._Profile.FirstName;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.Acc, accProfile);
                        this._HelperService.RefreshHelper();
                        await this._MixpanelService.alias(this._VInfo.MobileNumber);
                        await this._MixpanelService.setProfile({
                            $host: this._HelperService.AppConfig.HostType,
                            $first_name: this._Profile.FirstName,
                            $last_name: this._Profile.LastName,
                            $email: this._Profile.EmailAddress,
                            $mobileNumber: this._VInfo.MobileNumber,
                            $countryIsd: this._SelectedCountryDetails.Isd,
                        });
                        await this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.Registration, {
                            $mobileNumber: this._VInfo.MobileNumber,
                        })
                        await this._MixpanelService.identify(this._VInfo.MobileNumber)
                        window.location.reload();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    _vCode = "";
    onOtpChange(event) {
        this._vCode = event;
    }
    OpenSearch(content) {
        this._DealsSearch = this._DataService._DealsPromotionNew;
        this._NgbModal.open(content, { size: 'xl', modalDialogClass: 'tuc-pp-search', centered: true });
    }
    OpenLocation(event, content) {
        if(event){
            this._DataService._SearchedCities = this._DataService._Cities; 
            this._NgbModal.open(content, { size: 'lg', centered: true, scrollable: true });
        }
    }
    OpenWishlist(content) {
        this._NgbModal.open(content, { size: 'lg', modalDialogClass: 'tuc-pp-wishlist' });
        this._GetBookmarks();
    }

    public GlobalSearchParameter = "";
    GlobalSearch() {
        let wordSearch = this.GlobalSearchParameter;
        setTimeout(() => {
            if (wordSearch == this.GlobalSearchParameter) {
                if (this.GlobalSearchParameter) {
                    //função que irá retornar sua lista de objetos
                    this._GetDeals();
                } else {
                    //code here
                }
            }
        }, 800);
    }
    _DealsSearch: IDeal[] = []
    _DealsBookmark: [] = []

    _GetDeals() {
        // var CacheDeal = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Deals);
        // if (CacheDeal != null) {
        //     this._Deals = CacheDeal;
        // }
        var pData = {
            Task: 'getdeals',
            Offset: 0,
            Limit: 6,
            SearchCondition: this.GlobalSearchParameter,
            City: this._DataService.ActiveCity,
            Categories: [],
            Merchants: [],
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._DealsSearch = [];
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    _Response.Result.Data.forEach(element => {
                        this._DealsSearch.push(element);
                    });
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    _LoadDeal(Item) {
        this._MixpanelService.track(this._HelperService.AppConfig.MinPanelEvents.DealSearch, {
            $searchDeal: this.GlobalSearchParameter,
            $dealName: Item.Title
        });
        this._NgbModal.dismissAll();
    }


    _GetBookmarks() {
        // var CacheDeal = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Deals);
        // if (CacheDeal != null) {
        //     this._Deals = CacheDeal;
        // }
        var pData = {
            Task: 'getbookmarks',
            Offset: 0,
            Limit: 6,
            SearchCondition: this.GlobalSearchParameter,
            // City: this._DataService.ActiveCity,
            ReferenceId: 0,
            ReferenceKey: null
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._DealsBookmark = [];
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._DealsBookmark = _Response.Result;
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    _SearchContent(event) {
        let value = event.target.value;
        if (!value) {
            this._DataService._SearchedCities = this._DataService._Cities;
        }else{
            this._DataService._SearchedCities = [];
            this._DataService._Cities.forEach(ele => {
                if(ele.Name.toLocaleLowerCase().indexOf(value) > -1){
                    this._DataService._SearchedCities.push(ele)
                }
            })
        }
    }

    GetCatDeals(Item) {
        window.location.href = 'category/' + Item.ReferenceId;
    }

    toggleCategorySection(){
        this.showCategorySec = !this.showCategorySec;
        this._DataService.showCategorySectionScource.next(this.showCategorySec);
        if(this.showCategorySec == false){
            document.getElementById('cust_cat_overlay').style.display = 'none';
            document.getElementById('show-subcat').style.display = 'none';
            document.querySelector('.banner_category-list-sub').style.display = 'none';
            document.querySelector('.banner-block').style.zIndex = '99';
        } else {
            document.querySelector('.banner-block').style.zIndex = '100';
        }
    }

    openBar()
    {
        $("#mobileMenu").css("opacity", "0");
         $("#mobilePanel").addClass("enter");
    }

     closeBar()
     {
        $("#mobilePanel").removeClass("enter");
        $("#mobileMenu").css("opacity", "1");
     }

     viewBalance()
    {
        this.router.navigate( ['/profile' ], {fragment: 'pointbalance', replaceUrl: true});
    }
}