import { Component } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
@Component({
    selector: 'tuc-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.css']
})
export class TUCPageFooter {
    constructor(public _DataService: DataService, public _HelperService: HelperService) { }
    subscribeEmail:string = '';
    navigateToRoute(route) {
        window.location.href = route;
    }
}