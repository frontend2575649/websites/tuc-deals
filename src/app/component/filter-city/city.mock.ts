export const cities: any = [
    {
        "Name": "Aba",
        "Deals": 1
    },
    {
        "Name": "Abagana",
        "Deals": 99
    },
    {
        "Name": "Abaji",
        "Deals": 61
    },
    {
        "Name": "Abak",
        "Deals": 83
    },
    {
        "Name": "Abakaliki",
        "Deals": 13
    },
    {
        "Name": "Abat",
        "Deals": 82
    },
    {
        "Name": "Baap",
        "Deals": 65
    },
    {
        "Name": "Babban Gida",
        "Deals": 4
    },
    {
        "Name": "Babura",
        "Deals": 17
    },
    {
        "Name": "Calabar",
        "Deals": 19
    },
    {
        "Name": "Charanchi",
        "Deals": 79
    },
    {
        "Name": "Chibok",
        "Deals": 59
    },
    {
        "Name": "Dakingari",
        "Deals": 9
    },
    {
        "Name": "Damagum",
        "Deals": 55
    },
    {
        "Name": "Damasak",
        "Deals": 30
    },
    {
        "Name": "Damaturu",
        "Deals": 19
    },
    {
        "Name": "Dambam",
        "Deals": 24
    },
    {
        "Name": "Dambatta",
        "Deals": 46
    },
    {
        "Name": "Damboa",
        "Deals": 38
    },
    {
        "Name": "Dan Musa",
        "Deals": 20
    },
    {
        "Name": "Ebem Ohafia",
        "Deals": 48
    },
    {
        "Name": "Eberi",
        "Deals": 76
    },
    {
        "Name": "Ebute-Metta",
        "Deals": 63
    },
    {
        "Name": "Ede",
        "Deals": 68
    },
    {
        "Name": "Effraya",
        "Deals": 84
    },
    {
        "Name": "Faskari",
        "Deals": 5
    },
    {
        "Name": "Festac Town",
        "Deals": 98
    },
    {
        "Name": "Fika",
        "Deals": 28
    },
    {
        "Name": "Hadejia",
        "Deals": 78
    },
    {
        "Name": "Hong",
        "Deals": 54
    },
    {
        "Name": "Hunkuyi",
        "Deals": 54
    }
]