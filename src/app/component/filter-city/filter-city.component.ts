import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ICity } from 'src/app/service/interface.service';
import { cities } from './city.mock';

@Component({
  selector: 'app-filter-city',
  templateUrl: './filter-city.component.html',
  styleUrls: ['./filter-city.component.css']
})
export class FilterCityComponent implements OnInit {
  cities = cities;
  _GroupedCities: any = [];
  constructor() { }
  @Input('TCities') set TCities(value) {
    this._GroupedCities = this.groupByAlpha(value);
  }
  @Output() CityChange = new EventEmitter<ICity>();
  ngOnInit(): void { }

  groupByAlpha(value) {
    let data = value.reduce((r, e: any) => {
      let group = e.Name[0];
      if (!r[group]) r[group] = { group, children: [e] }
      else r[group].children.push(e);
      return r;
    }, {});

    return Object.values(data)
  }

}
