import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { debounceTime, distinctUntilChanged, filter, forkJoin, fromEvent, Observable, switchMap, tap } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { OResponse } from 'src/app/service/interface.service';
declare var $: any;
declare var document: any;
@Component({
  selector: 'app-top-nav-search',
  templateUrl: './top-nav-search.component.html',
  styleUrls: ['./top-nav-search.component.css'],
  host: {'class': 'widthSize'}
})
export class TopNavSearchComponent implements OnInit {
  searchKeyworkList: any = [];
  searchContent: string = '';
  styleWidth: any;
  loading: boolean = false;
  innerWidth: number = window.innerWidth;
  _Theme!: string;
  @Input('styles') set styles(value) {
    this.styleWidth = value;
  }
  @Input('theme') set theme(value) {
    this._Theme = value;
  }
  @Output() openLocationModal = new EventEmitter<boolean>();
  @ViewChild('input', { static: true }) input: ElementRef;
  constructor(
    public _DataService: DataService,
    public _HelperService: HelperService,
    private _Router: Router
  ) { }
  @HostListener('blur', ['$event']) onBlur(event) {
    setTimeout(() => $('.search_menu').hide(), 400);
  }
  @HostListener('focus', ['$event']) onFocus(event) {
    this.showMenu()
  }
  navigate(TItem) {
    $('.search_menu').hide();
    this.searchContent = TItem.SearchText;
    this._Router.navigate(['/deals'], { queryParams: { search: TItem.SearchText } });
  }
  ngOnInit(): void {
    fromEvent(this.input?.nativeElement, 'keyup')
      .pipe(
        filter(Boolean),
        tap((text)=> {
          if(this.input?.nativeElement?.value?.trim().length == 0){
            $('.search_menu').hide();
          }
        }),
        debounceTime(800),
        distinctUntilChanged(),
        tap((text:any) => {
          console.log(this.input?.nativeElement.value, text);
          const _SearchContent = this.input?.nativeElement.value.trim();
          if (_SearchContent) {
            this.showMenu();
            const savesearch = this._SaveSearchText(_SearchContent);
            const getsearch = this._GetSearchResult(_SearchContent);
            return forkJoin([savesearch, getsearch])
          } 
          else {
            $('.search_menu').hide();
            return this.searchKeyworkList = [];
          }
        }),
      ).subscribe();
  }
  _SaveSearchText(term){
    let acc = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Acc);
    console.log(acc);
    
    let pData = {
      SearchText: term,
      Task: 'savesearch'
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Search, pData);
    _OResponse.subscribe(_Response => {},
      _Error => {
        this._HelperService.HandleException(_Error);
      });
  }
  _GetSearchResult(term){
    this.loading = true;
    let acc = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Acc);
    console.log(acc);
    let SearchCondition = (acc && acc.AccountId) ? `AccountId = \"${acc.AccountId}\" AND SearchText.Contains(\"${term}\")` : `SearchText.Contains(\"${term}\")`;
    let pData = {
      TotalRecords: 0,
      Offset: 0,
      Limit: 10,
      RefreshCount: true,
      SearchCondition,
      SortExpression: "",
      Task: 'getsearch'
    }
    let _SearchData: any = [];
    this.searchKeyworkList = [];
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Search, pData);
    _OResponse.subscribe(
      _Response => {
        this.searchKeyworkList = _Response.Result.Data;
        this.showMenu();
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }, () => {
        this.loading = false;
        return _SearchData; 
      });
  }

  _GetDeals(term: any) {
    let _SearchData: any = [];
    var pData = {
      Task: 'getdeals',
      Offset: 0,
      Limit: 30,
      SearchCondition: term,
      City: this._DataService.ActiveCity,
      Categories: [],
      Merchants: [],
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.Deals, pData);
    _OResponse.subscribe(
      _Response => {
        this.searchKeyworkList = _Response.Result.Data;
        if (this.searchKeyworkList.length) this.showMenu();
        else setTimeout(() => $('.search_menu').hide(), 300);
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }, () => _SearchData);
  }
  showMenu() {
    if (this.input.nativeElement.value) {
      $('.search_menu').show();
    }
  }
  changeContent(event) {
    console.log(event, event.keyCode);
    if (event.keyCode == 13 && event.target.value.trim().length > 1) {
      document.querySelector('#searchInput').blur();
      this.navigate({ SearchText: event.target.value.trim() })
    }
  }
  clearAllData() {
    setTimeout(() => {
      $('.search_menu').hide();
      this.searchContent = '';
      this.searchKeyworkList = [];
    }, 300);
  }
}
