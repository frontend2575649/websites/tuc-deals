import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TUCCountdownTimer } from "./component/countdown-timer/countdown-timer.component";
import { TUCGuestRegistrationFormComponent } from "./component/guest-registration-form/guest-registration-form.component";
import { TUCShippingAddressFormComponent } from "./component/shipping-address-form/shipping-address-form.component";
import { QuantityInputComponent } from './component/quantity-input/quantity-input.component';

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule],
    declarations: [TUCCountdownTimer, TUCShippingAddressFormComponent, TUCGuestRegistrationFormComponent, QuantityInputComponent],
    exports: [TUCCountdownTimer, TUCShippingAddressFormComponent, TUCGuestRegistrationFormComponent, QuantityInputComponent],
})

export class SharedModule {

}