import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Observable, tap } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { IBalance, OResponse } from 'src/app/service/interface.service';
declare var $: any;
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpEventType } from '@angular/common/http';

@Component({
    selector: 'tuc-guest-registration-form',
    templateUrl: './guest-registration-form.component.html',
    // styleUrls: ['./guest-registration-form.component.css']
})
export class TUCGuestRegistrationFormComponent implements OnInit {
    isActiveUser: boolean = !!this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Acc);
    isSubmittedForm: boolean = false;
    guestUserRegistrationFormForInstore: FormGroup;
    guestUserRegistrationFormForDelivery: FormGroup;
    citiesAgainstState: any = [];
    _DealDetails: any = undefined;
    _registrationType = 'Delivery';
    _Quantity: number = 1;
    LocationTypeList: { LocationTypeId: number, Name: string, LocationTypeCode: string }[] = [
        {
            LocationTypeId: 791,
            Name: "Home",
            LocationTypeCode: "locationtype.residential"
        },
        {
            LocationTypeId: 792,
            Name: "Work",
            LocationTypeCode: "locationtype.work"
        },
        {
            LocationTypeId: 800,
            Name: "Other",
            LocationTypeCode: "locationtype.other"
        }
    ]
    constructor(private _NgbModal: NgbModal, private _ActivatedRoute: ActivatedRoute, public _DataService: DataService, public _HelperService: HelperService, private fb: FormBuilder) {
    }
    _formAction: any = undefined;
    _modal;

    @Input('modal') set modal(value) {
        this.initRegisterForm();
        this._modal = value;

    }
    @Input('registrationType') set registrationType(value) {
        this._registrationType = value;
        this.setFormData();
    }
    @Input('Quantity') set Quantity(value) {
        this._Quantity = value;
    }
    @Output() registeredUser = new EventEmitter<any>();
    @Input('DealDetails') set DealDetails(value) {
        if (value != undefined) {
            this._DealDetails = value;
            // this.initRegisterForm();
            // if (this._DealDetails.DealTypeCode == this._HelperService.AppConfig.DealTypeCode.ServiceDeal || this._DealDetails?.DeliveryTypeCode == this._HelperService.AppConfig.DeliveryTypeCode.InStorePickUp) {

        }
    }
    ngOnInit(): void {
    }

    initRegisterForm() {
        this.guestUserRegistrationFormForInstore = this.fb.group({
            FullName: ['', Validators.required],
            // LastName: ['', Validators.required],
            PhoneNumber: this.fb.group({
                Code: ['234', Validators.required],
                Number: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/), Validators.minLength(10), Validators.maxLength(11)]]
            }),
            AdditionalNumber: this.fb.group({
                Code: ['234'],
                Number: ['']
            }),
            referralCode: ['', Validators.minLength(6)],
            EmailAddress: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]]
        })

        this.guestUserRegistrationFormForDelivery = this.fb.group({
            FullName: ['', Validators.required],
            // LastName: ['', Validators.required],
            City: ['', Validators.required],
            State: ['', Validators.required],
            AddressLine1: ['', Validators.required],
            AddressLine2: [''],
            PhoneNumber: this.fb.group({
                Code: ['234', Validators.required],
                Number: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/), Validators.minLength(10), Validators.maxLength(11)]]
            }),
            AdditionalNumber: null,
            LocationType: [791],
            referralCode: ['', Validators.minLength(6)],
            EmailAddress: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]]
        });

    }
    shippingAddress(content) {
        this._NgbModal.open(content, { size: 'md', modalDialogClass: 'tuc-shipping-address-modal' });
    }
    changeSelectFormControl(control, event?) {
        if (control == 'State') {
            if (event != undefined) {
                this.guestUserRegistrationFormForDelivery.patchValue({
                    City: ''
                });
            }
            let pData = {
                Task: 'getcities',
                Offset: 0,
                Limit: 1000,
                ReferenceId: this.deliveryform['State'].value,			// State Id
                ReferenceKey: this._DataService._AllStates.find(e => e.ReferenceId == this.deliveryform['State'].value)?.ReferenceKey,	// State Key
            }
            this._HelperService.IsProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.State, pData);
            _OResponse.subscribe(
                _Response => {
                    // this._modal.close('Yes click');
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this.citiesAgainstState = _Response.Result.Data;
                        let f = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.GuestUser);
                        if (f !== null && f !== undefined) {
                            this.guestUserRegistrationFormForDelivery.patchValue({
                                City: f.CityId
                            });
                        }
                    }
                    else {
                        this.citiesAgainstState = [];
                    }
                },
                _Error => {
                    this.citiesAgainstState = [];
                    this._HelperService.HandleException(_Error);
                });
        }

    }

    setFormData() {
        let f: any = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.GuestUser);
        
        if (f !== null && f !== undefined) {
            if (this._registrationType == 'Pickup') {
                this.guestUserRegistrationFormForInstore.patchValue({
                    FullName: `${f.FirstName} ${f.LastName}`,
                    // LastName: f.LastName,
                    PhoneNumber: {
                        Code: f.MobileNumber?.slice(0, 3),
                        Number: f.MobileNumber?.slice(3)
                    },
                    EmailAddress: f.EmailAddress
                })
            } else {
                this.guestUserRegistrationFormForDelivery.patchValue({
                    FullName: `${f.FirstName} ${f.LastName}`,
                    // LastName: f.LastName,
                    PhoneNumber: {
                        Code: f.MobileNumber?.slice(0, 3),
                        Number: f.MobileNumber?.slice(3)
                    },
                    EmailAddress: f.EmailAddress,
                    State: f.StateId == 0 ? '' : f.StateId,
                    City: f.CityId == 0 ? '' : f.CityId,

                    AddressLine1: f.AddressLine1,
                    AddressLine2: f.AddressLine2,
                    AdditionalNumber: {
                        Code: f.AdditionalNumber ? f.AdditionalNumber?.slice(0, 3) : '234',
                        Number: f.AdditionalNumber?.slice(3)
                    },
                    LocationType: f.LocationTypeId
                });
                this.changeSelectFormControl('State');
            }
        }
    }

    // saveForm() {
    //     this.isSubmittedForm = true;
    //     console.log(this.guestUserRegistrationForm);
    //     if (this.guestUserRegistrationForm.valid) {
    //     let f = this.guestUserRegistrationForm.value;
    //     let pData: any = {
    //         CountryId: this._DataService._CountrySelcted.ReferenceId,
    //         CountryKey: this._DataService._CountrySelcted.ReferenceKey,
    //         CityId: f.City,
    //         EmailAddress: this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Acc)?.EmailAddress,//"omkar@thankucash.com",
    //         FirstName: f.FirstName,
    //         LastName: f.LastName,
    //         AddressLine1: f.AddressLine1,
    //         AddressLine2: f.AddressLine2,
    //         Name: `${f.FirstName} ${f.LastName}`,
    //         ContactNumber: f.PhoneNumber.Code + f.PhoneNumber.Number,
    //         StateId: f.State,
    //         StateKey: this._DataService._AllStates.find(e => e.ReferenceId == f.State)?.ReferenceKey,
    //         ZipCode: f.Zip ? f.Zip : null,

    //         "IsPrimary": false,
    //         "LocationTypeId": f.LocationType,
    //         "LocationTypeCode": this.LocationTypeList.find(ele => ele.LocationTypeId == f.LocationType)?.LocationTypeCode,
    //         "DisplayName": null,
    //         "Landmark": null,
    //         "CityAreaId": 0,
    //         "Latitude": 0.0,
    //         "Longitude": 0.0,
    //         "MapAddress": null,
    //         "Instructions": null,
    //     }
    //     // api 
    //     if (this._formAction == undefined) { // new create flow
    //         pData.Task = "saveaddress";
    //         this.shipmentAddressApi(pData);

    //     } else { // update  
    //         pData.Task = 'updateaddress';
    //         pData.ReferenceId = this._formAction.ReferenceId;
    //         pData.ReferenceKey = this._formAction.ReferenceKey;
    //         this.shipmentAddressApi(pData);
    //     }
    //     }
    // }

    guestUserRegistrationApi(pData) {
        this._HelperService.IsProcessing = true;
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageItem.GuestUser, pData);
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    // console.log(_Response);
                    this.registeredUser.emit({ ..._Response.Result, user: pData });
                    this._modal.close('Yes click');
                    this._HelperService.NotifySuccess(_Response.Message);
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
                // this._modal.close('Yes click');
                // this.registeredUser.emit({ key: 'something', AccountId: '1', AddressId: '1', user: pData });
            });
    }
    get instoreform() {
        return this.guestUserRegistrationFormForInstore.controls;
    }
    get deliveryform() {
        return this.guestUserRegistrationFormForDelivery.controls;
    }
    get instorePhoneNumberControl() {
        return (this.guestUserRegistrationFormForInstore.controls['PhoneNumber'] as FormGroup).controls;
    }
    get deliveryPhoneNumberControl() {
        return (this.guestUserRegistrationFormForDelivery.controls['PhoneNumber'] as FormGroup).controls;
    }
    get deliveryAlternateNumberControl() {
        return (this.guestUserRegistrationFormForDelivery.controls['AdditionalNumber'] as FormGroup).controls;
    }


    Checkout() {
        this.isSubmittedForm = true;
        let pData: any = {};
        if (this._registrationType == 'Pickup') {
            if (this.guestUserRegistrationFormForInstore.valid) {
                let f = this.guestUserRegistrationFormForInstore.value;
                pData = {
                    CountryId: this._DataService._CountrySelcted.ReferenceId,
                    CountryKey: this._DataService._CountrySelcted.ReferenceKey,
                    CityId: 0,
                    EmailAddress: f.EmailAddress,
                    FirstName: f.FullName.split(' ')[0],
                    LastName: f.FullName.split(' ').slice(1).join(' '),
                    AddressLine1: '',
                    AddressLine2: '',
                    Name: f.FullName,
                    MobileNumber: f.PhoneNumber.Code + f.PhoneNumber.Number,
                    ContactNumber: f.AdditionalNumber.Code + f.AdditionalNumber.Number,
                    StateId: 0,
                    StateKey: 0,
                    ZipCode: null,

                    // "IsPrimary": false,
                    "LocationTypeId": 0,
                    "LocationTypeCode": 0,
                    "DisplayName": null,
                    "Landmark": null,
                    "CityAreaId": 0,
                    "Latitude": 0.0,
                    "Longitude": 0.0,
                    "MapAddress": null,
                    "Instructions": null,
                    Task: 'guestcheckout',
                    "CountryIsd": "234",
                }
                this.guestUserRegistrationApi(pData);
            }

        } else {
            if (this.guestUserRegistrationFormForDelivery.valid) {
                let f = this.guestUserRegistrationFormForDelivery.value;
                pData = {
                    CountryId: this._DataService._CountrySelcted.ReferenceId,
                    CountryKey: this._DataService._CountrySelcted.ReferenceKey,
                    CityId: f.City,
                    EmailAddress: f.EmailAddress,//"omkar@thankucash.com",
                    FirstName: f.FullName.split(' ')[0],
                    LastName: f.FullName.split(' ').slice(1).join(' '),
                    AddressLine1: f.AddressLine1,
                    AddressLine2: f.AddressLine2,
                    Name: f.FullName,
                    MobileNumber: f.PhoneNumber.Code + f.PhoneNumber.Number,
                    ContactNumber: null,
                    StateId: f.State,
                    StateKey: this._DataService._AllStates.find(e => e.ReferenceId == f.State)?.ReferenceKey,
                    ZipCode: null,

                    // "IsPrimary": false,
                    "LocationTypeId": 0,
                    "LocationTypeCode": this.LocationTypeList.find(ele => ele.LocationTypeId == f.LocationType)?.LocationTypeCode,
                    "DisplayName": null,
                    "Landmark": null,
                    "CityAreaId": 0,
                    "Latitude": 0.0,
                    "Longitude": 0.0,
                    "MapAddress": null,
                    "Instructions": null,
                    Task: 'guestcheckout',
                    "CountryIsd": "234",
                }
                this.guestUserRegistrationApi(pData);
            }
        }


        // this.registeredUser.emit({ key: 'something', user: this.guestUserRegistrationFormForInstore.value });

    }
}
