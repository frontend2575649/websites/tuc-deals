import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Observable, tap } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { HelperService } from 'src/app/service/helper.service';
import { IBalance, OResponse } from 'src/app/service/interface.service';
declare var $: any;
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpEventType } from '@angular/common/http';

@Component({
    selector: 'tuc-shipping-address-form',
    templateUrl: './shipping-address-form.component.html',
    // styleUrls: ['./shipping-address-form.component.css']
})
export class TUCShippingAddressFormComponent implements OnInit {

    DeliveryType: string = '';
    SelectedDeliveryAddress: string = '';
    shippingAddresses: any[] = [];
    isSubmittedForm: boolean = false;
    shippingAddressForm: FormGroup;
    shippingAddressChangeMode: boolean = false;
    selectedAddress: any = null;
    citiesAgainstState: any = [];
    LocationTypeList: { LocationTypeId: number, Name: string, LocationTypeCode: string }[] = [
        {
            LocationTypeId: 791,
            Name: "Home",
            LocationTypeCode: "locationtype.residential"
        },
        {
            LocationTypeId: 792,
            Name: "Work",
            LocationTypeCode: "locationtype.work"
        },
        {
            LocationTypeId: 800,
            Name: "Other",
            LocationTypeCode: "locationtype.other"
        }
    ]
    constructor(private _NgbModal: NgbModal, private _ActivatedRoute: ActivatedRoute, public _DataService: DataService, public _HelperService: HelperService, private fb: FormBuilder) {
    }
    _formAction: any = undefined;
    _modal;
    @Output() fetchAddress = new EventEmitter<boolean>();
    @Input('formAction') set formAction(value) {
        this.initShippingAddressForm();
        if (value != undefined) {
            this._formAction = value;
            this.setFormData();
        }
    }
    @Input('modal') set modal(value) {
        this._modal = value;
        console.log(this._modal)
    }
    ngOnInit(): void {
        // this.initShippingAddressForm();
    }
    shippingAddress(content) {
        this._NgbModal.open(content, { size: 'md', modalDialogClass: 'tuc-shipping-address-modal' });
    }
    changeSelectFormControl(control, event?) {
        if (control == 'State') {
            if (event != undefined) {
                this.shippingAddressForm.patchValue({
                    City: ''
                });
            }
            let pData = {
                Task: 'getcities',
                Offset: 0,
                Limit: 1000,
                ReferenceId: this.form['State'].value,			// State Id
                ReferenceKey: this._DataService._AllStates.find(e => e.ReferenceId == this.form['State'].value)?.ReferenceKey,	// State Key
            }
            this._HelperService.IsProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.MadDeals.State, pData);
            _OResponse.subscribe(
                _Response => {
                    // this._modal.close('Yes click');
                    this._HelperService.IsProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        // console.log(_Response);
                        this.citiesAgainstState = _Response.Result.Data;
                        if (this._formAction) {
                            this.shippingAddressForm.patchValue({
                                City: this._formAction.CityId
                            });
                            console.log(this.shippingAddressForm.value)
                        }
                    }
                    else {
                        this.citiesAgainstState = [];
                    }
                },
                _Error => {
                    this.citiesAgainstState = [];
                    this._HelperService.HandleException(_Error);
                });
        }
        console.log(`['${control}']`, event, this.shippingAddressForm.value)

    }
    initShippingAddressForm(): void {
        let _profile = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Profile);
        this.shippingAddressForm = this.fb.group({
            FirstName: [_profile ? _profile?.FirstName : '', Validators.required],
            LastName: [_profile ? _profile?.LastName : '', Validators.required],
            City: ['', Validators.required],
            State: ['', Validators.required],
            AddressLine1: ['', Validators.required],
            AddressLine2: [''],
            PhoneNumber: this.fb.group({
                Code: ['234', Validators.required],
                Number: [_profile ? _profile?.MobileNumber.slice(3) : '', [Validators.required, Validators.pattern(/^[0-9]\d*$/), Validators.minLength(10), Validators.maxLength(11)]]
            }),
            AdditionalNumber: this.fb.group({
                Code: ['234'],
                Number: ['', [Validators.pattern(/^[0-9]\d*$/), Validators.minLength(10), Validators.maxLength(11)]]
            }),
            LocationType: [791]
        })
    }
    setFormData() {
        // console.log(this._formAction);
        // if(this._formAction.ContactNumber.indexOf('+') == -1){
        //     this._formAction.ContactNumber = `+${this._formAction.ContactNumber}`;
        // }
        let f: any = this._formAction;
        this.shippingAddressForm.patchValue({
            FirstName: f.Name.split(' ')[0],
            LastName: f.Name.split(' ')[1],
            State: f.StateId,
            City: f.CityId,
            AddressLine1: f.AddressLine1,
            AddressLine2: f.AddressLine2,
            PhoneNumber: {
                Code: f.ContactNumber?.slice(0, 3),
                Number: f.ContactNumber?.slice(3)
            },
            AdditionalNumber: {
                Code: f.AdditionalMobileNumber ? f.AdditionalMobileNumber?.slice(0, 3) : '234',
                Number: f.AdditionalMobileNumber?.slice(3)
            },
            LocationType: f.LocationTypeId
        });
        this.changeSelectFormControl('State');
    }

    saveForm() {
        this.isSubmittedForm = true;
        console.log(this.shippingAddressForm);
        if (this.shippingAddressForm.valid) {
            let f = this.shippingAddressForm.value;
            let pData: any = {
                CountryId: this._DataService._CountrySelcted.ReferenceId,
                CountryKey: this._DataService._CountrySelcted.ReferenceKey,
                CityId: f.City,
                EmailAddress: this._HelperService.GetStorage(this._HelperService.AppConfig.StorageItem.Acc)?.EmailAddress,//"omkar@thankucash.com",
                FirstName: f.FirstName,
                LastName: f.LastName,
                AddressLine1: f.AddressLine1,
                AddressLine2: f.AddressLine2,
                Name: `${f.FirstName} ${f.LastName}`,
                ContactNumber: f.PhoneNumber.Code + f.PhoneNumber.Number,
                AdditionalMobileNumber: f.AdditionalNumber.Number ? `${f.AdditionalNumber.Code}${f.AdditionalNumber.Number}` : '',
                StateId: f.State,
                StateKey: this._DataService._AllStates.find(e => e.ReferenceId == f.State)?.ReferenceKey,
                ZipCode: f.Zip ? f.Zip : null,

                "IsPrimary": false,
                "LocationTypeId": f.LocationType,
                "LocationTypeCode": this.LocationTypeList.find(ele => ele.LocationTypeId == f.LocationType)?.LocationTypeCode,
                "DisplayName": null,
                "Landmark": null,
                "CityAreaId": 0,
                "Latitude": 0.0,
                "Longitude": 0.0,
                "MapAddress": null,
                "Instructions": null,
            }
            // api 
            if (this._formAction == undefined) { // new create flow
                pData.Task = "saveaddress";
                this.shipmentAddressApi(pData);

            } else { // update  
                pData.Task = 'updateaddress';
                pData.ReferenceId = this._formAction.ReferenceId;
                pData.ReferenceKey = this._formAction.ReferenceKey;
                this.shipmentAddressApi(pData);
            }
        }
    }

    shipmentAddressApi(pData) {
        this._HelperService.IsProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acc.Acc, pData);
        _OResponse.subscribe(
            _Response => {
                this._modal.close('Yes click');
                this._HelperService.IsProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    // console.log(_Response);
                    this.fetchAddress.emit(true);
                    this._HelperService.NotifySuccess(_Response.Message);
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
                // this._modal.close('Yes click');
                this.fetchAddress.emit(true);
            });
    }
    get form() {
        return this.shippingAddressForm.controls;
    }
    get phoneNumberControl() {
        return (this.shippingAddressForm.controls['PhoneNumber'] as FormGroup).controls;
    }
    get additionalNumberControl() {
        return (this.shippingAddressForm.controls['AdditionalNumber'] as FormGroup).controls;
    }
}