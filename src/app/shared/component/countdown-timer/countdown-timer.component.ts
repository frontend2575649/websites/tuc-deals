import { Component, Input, OnChanges, OnDestroy } from '@angular/core';
@Component({
    selector: 'tuc-countdown-timer',
    template: `<span>{{countdown}}</span>`,
})
export class TUCCountdownTimer implements OnChanges, OnDestroy {
    private _config;
    countdown: any = "00:00:00:00";
    @Input() set config(value: any) {
        this._config = value;
    }

    days: any = 0;
    hours: any = 0;
    minutes: any = 0;
    seconds: any = 0;
    private counterInterval: any;
    constructor() { }

    ngOnChanges() {
        if (this._config) {
            let countDownDate = new Date(this._config).getTime();
            // clearInterval(this.counterInterval);
            let self = this;
            this.counterInterval = setInterval(function () {
                let now = new Date().getTime();
                let distance = countDownDate - now;

                self.days = Math.floor(distance / (1000 * 60 * 60 * 24));
                self.hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                self.minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                self.seconds = Math.floor((distance % (1000 * 60)) / 1000);

                self.countdown = `${self.days < 10 ? '0' + self.days : self.days
                    }:${self.hours < 10 ? '0' + self.hours : self.hours}:${self.minutes < 10 ? '0' + self.minutes : self.minutes
                    }:${self.seconds < 10 ? '0' + self.seconds : self.seconds}`;

                if (distance < 0) {
                    clearInterval(self.counterInterval);
                    self.countdown = '00:00:00:00';
                }
            }, 1000);
        }
    }
    ngOnDestroy(): void {
        clearInterval(this.counterInterval);
    }
}