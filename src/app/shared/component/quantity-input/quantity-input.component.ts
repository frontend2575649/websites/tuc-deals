import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-quantity-input',
  templateUrl: './quantity-input.component.html',
  styleUrls: ['./quantity-input.component.css']
})
export class QuantityInputComponent implements OnInit {
  @Input() DealQuantity: number = 1;
  @Input('Theme') set Theme(value){
    if(value){
      this._Theme = value;
    }
  } 
  @Input('MaxLimit') set MaxLimit(value){
    this._MaxLimit = value;
  } 
  @Output() DealQuantityChange = new EventEmitter<number>();
  _Theme: string = 'input';
  _MaxLimit: number = 10;
  constructor() { }

  ngOnInit(): void {
  }

  updateQuantity(value){
    this.DealQuantity =  Math.min(this._MaxLimit, Math.max(1, +this.DealQuantity + value));
    this.DealQuantityChange.emit(this.DealQuantity);
  }
}
